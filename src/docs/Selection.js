import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Selection from '../lib/Selection';

const ImportCode = `import Selection from 'elementar-ui/dist/Selection';`;

const ExampleCode = `state = {
  color: 'blue',
  rims: false,
  spoilers: false,
  lights: false,
  insurance: true
}

handleCheckBox = e => this.setState({ [e.target.name]: e.target.checked });

handleRadio = e => this.setState({ [e.target.name]: e.target.value });

...

<Selection
  label="Azul"
  name="color"
  type="radio"
  value="blue"
  checked={this.state.color === 'blue'}
  onChange={this.handleRadio}
/>
<Selection
  label="Vermelho"
  name="color"
  type="radio"
  value="red"
  inline
  checked={this.state.color === 'red'}
  onChange={this.handleRadio}
/>
<Selection
  label="Verde"
  name="color"
  type="radio"
  value="green"
  inline
  disabled
  checked={this.state.color === 'green'}
  onChange={this.handleRadio}
  help="Temporariamente indisponível"
/>

<hr/>

<Selection
  label="Rodas"
  name="rims"
  type="checkbox"
  color="primary"
  checked={this.state.rims}
  onChange={this.handleCheckBox}
/>
<Selection
  label="Aerofólios"
  name="spoilers"
  type="checkbox"
  color="success"
  checked={this.state.spoilers}
  onChange={this.handleCheckBox}
/>
<Selection
  label="Faróis"
  name="lights"
  type="checkbox"
  color="warning"
  checked={this.state.lights}
  onChange={this.handleCheckBox}
/>

<hr/>

<Selection
  label="Fazer seguro de novas peças"
  name="insurance"
  color="alert"
  checked={this.state.insurance}
  onChange={this.handleCheckBox}
/>
<Selection
  label="Adicionar itens premium"
  name="insurance"
  color="#0FF"
  checked={this.state.premiumItems}
  onChange={this.handleCheckBox}
  mobile
/>`;

export default class SelectionDocs extends React.Component {
  state = {
    color: 'blue',
    rims: false,
    spoilers: false,
    lights: false,
    insurance: true,
    premiumItems: true
  }

  handleCheckBox = e => this.setState({ [e.target.name]: e.target.checked });

  handleRadio = e => this.setState({ [e.target.name]: e.target.value });

  render() {
    return (
      <Template title="Controles de seleção" id="selection-controls">

        <p>Os controles de seleção permitem dar escolhas rápidas ao usuário em um formulário ou num componente que demanda uma ação de escolha.</p>

        <CodeBox>{ImportCode}</CodeBox>

        <p>Existem três modelos de seleção de controle disponíveis para o usuário: o checkbox e o toggle, para ações de opt-in ou opt-out, ou o radio para decisões com múltiplas escolhas.</p>

        <DemoBox>
          <Selection
            label="Azul"
            name="color"
            type="radio"
            value="blue"
            inline
            checked={this.state.color === 'blue'}
            onChange={this.handleRadio}
          />
          <Selection
            label="Vermelho"
            name="color"
            type="radio"
            value="red"
            inline
            checked={this.state.color === 'red'}
            onChange={this.handleRadio}
          />
          <Selection
            label="Verde"
            name="color"
            type="radio"
            value="green"
            inline
            disabled
            checked={this.state.color === 'green'}
            onChange={this.handleRadio}
            help="Temporariamente indisponível"
          />
          <hr/>
          <Selection
            label="Rodas"
            name="rims"
            type="checkbox"
            color="primary"
            checked={this.state.rims}
            onChange={this.handleCheckBox}
          />
          <Selection
            label="Faróis"
            name="lights"
            type="checkbox"
            color="warning"
            checked={this.state.lights}
            onChange={this.handleCheckBox}
          />
          <hr/>
          <Selection
            label="Fazer seguro de novas peças"
            name="insurance"
            color="danger"
            checked={this.state.insurance}
            onChange={this.handleCheckBox}
          />
          <Selection
            label="Adicionar itens premium"
            name="premiumItems"
            color="#0FF"
            checked={this.state.premiumItems}
            onChange={this.handleCheckBox}
            mobile
            noMargin
            loading
          />
        </DemoBox>

        <CodeBox>{ExampleCode}</CodeBox>

        <p>O componente <code>Selection</code> possui parâmetros que permitem customizar o elemento de acordo com o devido propósito.</p>

        <Table closed striped>
          <TableHead>
            <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
            </TableRow>
          </TableHead>

          <TableBody>
            <TableRow>
              <TableCell bold>label</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>Opção</TableCell>
              <TableCell>Atribui o texto do seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>type</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>Toggle, Checkbox, Radio</TableCell>
              <TableCell>Define o modelo de seleção do componente</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>onChange</TableCell>
              <TableCell italic>function</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Atribui função que controla a ação de mudar estado do seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>containerClassName</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica o valor do atributo <code>class</code> ao elemento ancestral do seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>className</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica o valor do atributo <code>class</code> ao seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>id</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica o valor do atributo <code>id</code> ao seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>name</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica o valor do atributo <code>name</code> ao seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>checked</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Define a condição para que o seletor seja ou não marcado</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>disabled</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Define a condição para que o seletor esteja ou não habilitado para uso</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>color</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>default, primary, success, warning, danger</TableCell>
              <TableCell>Muda a cor do seletor</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>inline</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Dispõe os seletores lado-a-lado, em linha</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>mobile</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Dispõe os seletores com a legenda ao lado esquerdo, ocupando toda a largura</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </Template>
    )
  }
}
