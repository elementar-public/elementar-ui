import React, { useState } from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import FileInput from '../lib/FileInput';
import Button from '../lib/Button';

const ImportCode = `import FileInput from 'elementar-ui/dist/FileInput';`;

const ExampleCode = `state = {
  file: undefined
}

handleFile = e => this.setState({ file: e.target.files });

handleSeeFile = e => {
  e.preventDefault();

  window.alert('O arquivo selecionado se chama "' + file[0].name +'" e tem ' + file[0].size + ' bytes de tamanho.');
}

...

<FileInput name="file" label="Arquivo" help="Selecione um arquivo do tipo PDF" file={this.state.file} onChange={this.handleFile} accept=".pdf" />
<Button label="Ver arquivo" onClick={this.handleSeeFile} />
`;

const FileInputDocs = () => {
  const [file, handleFile] = useState(undefined);

  const handleSubmit = e => {
    e.preventDefault();

    window.alert('O arquivo selecionado se chama "' + file[0].name +'" e tem ' + file[0].size + ' bytes de tamanho.');
  }

  return (
    <Template title="File Input" id="file-input">
      <p>Os <strong>File Inputs</strong> são caixas de seleção de arquivos para manipulação pelo navegador. Apesar de terem o layout parecido com os demais inputs, o <code>File Input</code> possui um ícone próprio para diferenciar-se dos demais e o meio para modificar seu valor é com a caixa de seleção de arquivos do próprio navegador.</p>
  
      <CodeBox>{ImportCode}</CodeBox>

      <p>Com o <code>FileInput</code>, é possível selecionar até um arquivo e especificar tipos de arquivos aceitáveis.</p>
  
      <DemoBox>
        <FileInput name="file" label="Arquivo" help="Selecione um arquivo do tipo PDF" file={file} onChange={({target}) => handleFile(target.files)} accept=".pdf" placeholder="Teste" />
        <Button label="Ver arquivo" onClick={handleSubmit} />
      </DemoBox>
      
      <CodeBox>{ExampleCode}</CodeBox>
  
      <p>Você pode utilizar estes parâmetros para configurar o componente <code>FileInput</code>:</p>
  
      <Table closed striped>
        <TableHead>
          <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>
  
        <TableBody>
          <TableRow>
            <TableCell bold>className</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Controla o nome do atributo class do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>containerClassName</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Controla o nome do atributo class do elemento contenedor do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>label</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Controla o texto exibido no elemento <code>label</code>, logo acima do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>noMargin</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Desabilita a margem inferior que separa o seletor de opções do restante do conteúdo abaixo dele</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>onChange</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica as ações do seletor de opções quando uma nova opção é escolhida</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>file</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o atributo que recebe as informações do arquivo selecionado</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>name</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o valor do atributo <code>name</code> do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>id</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o valor do atributo <code>id</code> do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>inputRef</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o valor do atributo <code>ref</code> do elemento do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>accept</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Define as extensões de arquivos aceitáveis para o componente</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>disabled</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Define a condição para que o componente esteja ou não habilitado para uso</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>loading</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Define a condição para que o componente esteja ou não exibindo um ícone de atividade</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Template>
  );
};

export default FileInputDocs;
