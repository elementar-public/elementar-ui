import React, {useState} from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import TextArea from '../lib/TextArea';
// import Table, {TableHead, TableBody, TableRow, TableHeading, TableCell} from '../lib/Table';

const ImportCode = `import TextArea from 'elementar-ui/dist/TextArea';`;

const ExampleCode = `<TextArea
  label="Nome"
  color="primary"
  resize="vertical"
  height="200px"
  value={comment}
  onChange={({target}) => handleComment(target.value)}
  placeholder="Digite seu comentário"
/>`;

const TextAreaDocs = () => {
  const [comment, handleComment] = useState('');

  return (
    <Template title="TextArea" id="text-area">
      <p>TextArea</p>

      <CodeBox>{ImportCode}</CodeBox>

      <p>Sobre TextArea</p>

      <DemoBox>
        <TextArea
          label="Comentário"
          color="primary"
          resize="vertical"
          height="200px"
          value={comment}
          onChange={({target}) => handleComment(target.value)}
          placeholder="Digite seu comentário"
          data-test="TEST"
        />
      </DemoBox>

      <CodeBox>{ExampleCode}</CodeBox>
    </Template>
  )
};

export default TextAreaDocs;