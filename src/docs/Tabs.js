import React from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Tab, { TabUnit } from '../lib/Tab';

const ImportCode = `import Tab, {TabUnit} from 'elementar-ui/dist/Tab';`;

const ExampleCode = `<Tab maxHeight={200} color="success" align="fill">
  <TabUnit title="Garamond">
    ...
  </TabUnit>
  <TabUnit title="Akzidenz Grotesk" active>
    ...
  </TabUnit>
  <TabUnit title="Futura">
    ...
  </TabUnit>
</Tab>`;

const TabsDocs = () => (
  <Template title="Abas" id="tab">
    <p><strong>Abas</strong> podem ser utilizadas como componentes de interação para classificar e segmentar a porção de conteúdo visível na tela. Ao selecionar uma aba, o componente auxilia a aplicação a mostrar o conteúdo desejado e esconder o conteúdo referenciado pelas demais abas.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Os componentes de abas são separados entre o contênier específico <code>Tab</code> e as abas <code>TabUnit</code>, que aplicam da forma adequada os elementos necessários para a barra de abas funcionar adequadamente. Não é necessário utilizar um <code>state</code> externo para gerenciar a exibição das abas.</p>

    <DemoBox>
      <Tab maxHeight={200} color="success" align="fill">
        <TabUnit title="Garamond">
          <p>Garamond is a group of many old-style serif typefaces, named for sixteenth-century Parisian engraver Claude Garamond (generally spelled as Garamont in his lifetime). Garamond-style typefaces are popular and often used, particularly for printing body text and books.</p>
        </TabUnit>
        <TabUnit title="Akzidenz Grotesk" active>
          <p>Akzidenz-Grotesk is a sans-serif typeface family originally released by the Berthold Type Foundry of Berlin. Akzidenz indicates its intended use as a typeface for commercial, or "occasional" or "jobbing", print runs such as publicity, tickets and forms, as opposed to fine printing, and "grotesque" was a standard name for sans-serif typefaces at the time.</p>
        </TabUnit>
        <TabUnit title="Futura">
          <p>Futura is a geometric sans-serif typeface designed by Paul Renner and released in 1927. It was designed as a contribution on the New Frankfurt-project. It is based on geometric shapes, especially the circle, similar in spirit to the Bauhaus design style of the period. It was developed as a typeface by the Bauer Type Foundry, in competition with Ludwig & Mayer's seminal Erbar typeface of 1926.</p>
          <p>Futura has an appearance of efficiency and forwardness. Although Renner was not associated with the Bauhaus, he shared many of its idioms and believed that a modern typeface should express modern models, rather than be a revival of a previous design. Renner's design rejected the approach of most previous sans-serif designs (now often called grotesques), which were based on the models of signpainting, condensed lettering and nineteenth-century serif typefaces, in favour of simple geometric forms: near-perfect circles, triangles and squares. It is based on strokes of near-even weight, which are low in contrast. The lowercase has tall ascenders, which rise above the cap line, and uses nearly-circular, single-story forms for the "a" and "g", the former previously more common in handwriting than in printed text.[a] The uppercase characters present proportions similar to those of classical Roman capitals. The original metal type showed extensive adaptation of the design to individual sizes, and several divergent digitisations have been released by different companies.</p>
          <p>Futura was extensively marketed by Bauer and its American distribution arm by brochure as capturing the spirit of modernity, using the German slogan "die Schrift unserer Zeit" ("the typeface of our time") and in English "the typeface of today and tomorrow". It has remained popular since.</p>
        </TabUnit>
      </Tab>
    </DemoBox>

    <CodeBox>{ExampleCode}</CodeBox>

    <p>Podem ser utilizados alguns parâmetros para configurar a barra de abas.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>default</TableCell>
          <TableCell>Especifica a cor da aba selecionada na barra</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>height</TableCell>
          <TableCell italic>string, integer</TableCell>
          <TableCell italic>auto</TableCell>
          <TableCell>Especifica a altura fixa do componente.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>maxHeight</TableCell>
          <TableCell italic>string, integer</TableCell>
          <TableCell italic>auto</TableCell>
          <TableCell>Especifica a altura máxima do componente.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>align</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>left, center, right, spaced, fixed, fill</TableCell>
          <TableCell>Especifica a distribuição das abas ao longo da barra.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>mobile</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica o tema para dispositivos móveis na barra.</TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <p>As abas também possuem alguns parâmetros próprios.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>title</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>Aba <strong>n</strong></TableCell>
          <TableCell>Especifica o título da aba na barra superior</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>active</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se a aba será exibida na abertura do componente.</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default TabsDocs;
