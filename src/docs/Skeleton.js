import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Skeleton from '../lib/Skeleton';

const ImportCode = `import Skeleton from 'elementar-ui/dist/Skeleton';`;

const ExampleCode = `<Skeleton type="block" height={200} />
<Skeleton type="line" lines={[100, 98, 100, 45]} />
<Skeleton type="avatar" withName />
<Skeleton type="card" withLabel />
<Skeleton type="table" withAction />
<Skeleton type="menu" />
<Skeleton type="icon" height={300} withAction />`;

const SkeletonDocs = () => (
  <Template title="Skeleton" id="skeleton">
    <p><strong>Skeletons</strong> são réplicas de elementos de interface utilizados para mimetizar um conteúdo específico enquanto este é carregado.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <code>Skeleton</code> possui parâmetros que permitem selecionar o tipo de conteúdo que ele vai mostrar em tela, além de outros aspectos visuais.</p>

    <DemoBox>
      <Skeleton style={{marginBottom: 60}} type="block" height={200} />
      <Skeleton style={{marginBottom: 60}} type="line" lines={[100, 98, 100, 45]} />
      <Skeleton style={{marginBottom: 60}} type="avatar" withName />
      <Skeleton style={{marginBottom: 60}} type="card" height="10rem" withLabel />
      <Skeleton style={{marginBottom: 60}} type="table" withAction />
      <Skeleton style={{marginBottom: 60}} type="menu" />
      <Skeleton type="icon" height={300} withAction />
    </DemoBox>
    
    <CodeBox>{ExampleCode}</CodeBox>

    <p>Além destes comandos, você pode utilizar outros parâmetros para configurar um elemento <code>Skeleton</code>.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>type</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>block, line, avatar, card, table</TableCell>
          <TableCell>Especifica o tipo de conteúdo do componente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>width</TableCell>
          <TableCell italic>integer, string</TableCell>
          <TableCell italic>100%</TableCell>
          <TableCell>Especifica a largura do componente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>height</TableCell>
          <TableCell italic>integer, string</TableCell>
          <TableCell italic>50px</TableCell>
          <TableCell>Especifica a altura do componente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lines</TableCell>
          <TableCell italic>[array]</TableCell>
          <TableCell italic>[90, 100, 60]</TableCell>
          <TableCell>Especifica a quantidade e comprimento de linhas do skeleton de linhas</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>withName</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Determina se o skeleton de avatar terá espaço para nome</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>withLabel</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Determina se o skeleton de cartão terá espaço para rótulo</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>withAction</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Determina se o skeleton de linha de tabela terá espaço para botão de ação</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default SkeletonDocs;
