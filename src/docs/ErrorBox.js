import React, { useState } from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Button from '../lib/Button';
import ErrorBox, { ErrorBoundary } from '../lib/ErrorBox';

const ImportCode = `import ErrorBox, { ErrorBoundary } from 'elementar-ui/dist/ErrorBox';`;

const ExampleCode = [
`<ErrorBox error={new Error('Um erro importante.')} />
<ErrorBox message="Uma advertência." type="warning" />
<ErrorBox message="Um aviso." type="default" />`,
`const ErrorCounter = () => {
  const [counter, handleCounter] = useState(0);

  const errorCounter = () => handleCounter(counter + 1);

  if (counter > 4) throw new Error('Erro na contagem');

  return (
    <>
      <p>O contador acusará erro quando chegar ao número 5. Clique no botão para aumentar a contagem.</p>
      <p>Contador: {counter.toString()}</p>

      <Button onClick={errorCounter} label="Aumentar contador" size="small" />
    </>
  );
}

...

<ErrorBoundary>
  <ErrorCounter />
</ErrorBoundary>`
];

const ErrorCounter = () => {
  const [counter, handleCounter] = useState(0);

  const errorCounter = () => handleCounter(counter + 1);

  if (counter > 4) throw new Error('Erro na contagem');

  return (
    <>
      <p>O contador acusará erro quando chegar ao número 5. Clique no botão para aumentar a contagem.</p>
      <p>Contador: {counter.toString()}</p>

      <Button onClick={errorCounter} label="Aumentar contador" size="small" />
    </>
  );
}

const ErrorDocs = () => (
  <Template title="ErrorBox" id="errorbox">
    <p>O componente de <strong>erro</strong> permite mostrar erros pontuais na interface, decorrentes de algum erro provocado pelo próprio sistema ou pelo usuário.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Utilizando o componente <code>ErrorBox</code>, é criado um espaço que exibe uma mensagem específica. É possível também enviar um objeto <code>Error</code> para o componente, que exibirá o nome do erro gerado pelo interpretador de JavaScript.</p>

    <DemoBox>
      <ErrorBox error={new Error('Um erro importante.')} />
      <ErrorBox message="Uma advertência." type="warning" style={{ marginTop: 10 }} />
      <ErrorBox message="Um aviso." type="default" style={{ marginTop: 10 }} />
    </DemoBox>
    <CodeBox>{ExampleCode[0]}</CodeBox>

    <p>Com o <code>ErrorBoundary</code>, é criado um espaço delimitado em que os erros provocados pela renderização não afetam o estado geral da aplicação. Desta forma, é possível isolar erros e trata-los de forma que não impossibilite o uso da aplicação para outros fins. Em ambiente de desenvolvimento, o <code>ErrorBoundary</code> não previne a exibição de erro em tela cheia, mas mantém a execução normal do restante da aplicação e exibe o <em>stack</em> de erro gerado pelo servidor.</p>

    <DemoBox>
      <ErrorBoundary>
        <ErrorCounter />
      </ErrorBoundary>
    </DemoBox>
    <CodeBox>{ExampleCode[1]}</CodeBox>

    <p>O componente <code>ErrorBox</code> possui alguns parâmetros que permitem controlar a exibição do erro.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>type</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>danger</TableCell>
          <TableCell>Especifica a cor e ícone do quadro de erro</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>message</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>null</TableCell>
          <TableCell>Especifica a mensagem exibida pelo quadro de erro</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>error</TableCell>
          <TableCell italic>object</TableCell>
          <TableCell italic>null</TableCell>
          <TableCell>Atribui um objeto <code>Error</code> para o quadro de erro</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>errorInfo</TableCell>
          <TableCell italic>object</TableCell>
          <TableCell italic>null</TableCell>
          <TableCell>Atribui informações adicionais para o quadro de erro, exibidas apenas em ambiente de desenvolvimento</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default ErrorDocs;
