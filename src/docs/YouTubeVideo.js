import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import YouTubeVideo from '../lib/YouTubeVideo';

const ImportCode = `import YouTubeVideo from 'elementar-ui/dist/YouTubeVideo';`;

const ExampleCode = `<YouTubeVideo videoId="-0Ao4t_fe0I" noControls maxHeight="300px" />`;

const YouTubeVideoDocs = () => (
  <Template title="Player de YouTube" id="youtube-video">
    <p>O componente de <strong>player de vídeo</strong> permite incorporar à página um vídeo armazenado em serviços de streaming de vídeo. Por enquanto, apenas o <strong>YouTube</strong> é suportado.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Utilizando o componente <code>YouTubeVideo</code>, é criado um espaço que ocupa toda a largura do elemento-pai e ajusta-se livremente em caso de redimensionamento da página. O vídeo será carregado e executado apenas quando o usuário clicar no botão ou quando o comando <code>autoPlay</code> estiver presente.</p>

    <DemoBox>
      <YouTubeVideo videoId="-0Ao4t_fe0I" noControls maxHeight="300px" />
    </DemoBox>
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O componente <code>YouTubeVideo</code> possui alguns parâmetros que permitem controlar a execução do vídeo.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>videoId</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>oavMtUWDBTM</TableCell>
          <TableCell>Informa ao componente o identificador do vídeo</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>noControls</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Esconde os controles do vídeo</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>maxHeight</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Define a altura máxima do reprodutor de vídeo</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>autoPlay</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Executa o vídeo assim que a página é carregada</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>cover</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Estende a capa do vídeo para ocupar toda a área do componente quando o vídeo não for executado</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default YouTubeVideoDocs;
