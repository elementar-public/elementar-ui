import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Tooltip from '../lib/Tooltip';

const ImportCode = `import Tooltip from 'elementar-ui/dist/Tooltip';`;

const ExampleCode = `<p><Tooltip light message="Future in french">Avenir</Tooltip> is a <strong><Tooltip message="Sans-serif fonts are typefaces whose terminals don't contain serifs.">geometric sans-serif</Tooltip></strong> typeface designed ... such as <strong><Tooltip bottom position="right" message="Typeface designed by Jakob Erbar and released in 1926">Erbar</Tooltip></strong> and <strong><Tooltip position="left" active message="Typeface designed by Paul Renner and released in 1927">Futura</Tooltip></strong>.</p>`;

const TooltipDocs = () => (
  <Template title="Tooltip" id="tooltip">
    <p>O componente <strong>Tooltip</strong> permite adicionar um balão de ajuda adicional, revelado assim que o usuário posiciona o cursor sobre o conteúdo.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Um <code>Tooltip</code> pode ser formatado para utilizar dois modos de cores (escuro como padrão, claro como opcional), além de dispôr de três modos de posicionamento (central, esquerda e direita).</p>

    <DemoBox>
      <p><Tooltip light message="Future in french">Avenir</Tooltip> is a <strong><Tooltip message="Sans-serif fonts are typefaces whose terminals don't contain serifs.">geometric sans-serif</Tooltip></strong> typeface designed <Tooltip message="Future in french">by</Tooltip> Adrian Frutiger in 1987 and released in 1988 by Linotype GmbH. As the name suggests, the family takes inspiration from the geometric style of sans-serif typeface developed in the 1920s that took the circle as a basis, such as <strong><Tooltip bottom position="right" message="Typeface designed by Jakob Erbar and released in 1926">Erbar</Tooltip></strong> and <strong><Tooltip position="left" active message="Typeface designed by Paul Renner and released in 1927">Futura</Tooltip></strong>.</p>
    </DemoBox>
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O componente <code>Tooltip</code> possui alguns parâmetros que permitem customizar a aparência do item.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>message</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>Título</TableCell>
          <TableCell>Especifica o texto do Tooltip</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>light</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se o Tooltip alternará para o modo claro</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>position</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>center</TableCell>
          <TableCell>Especifica a posição do Tooltip em relação ao conteúdo a que se refere</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>active</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Controla a abertura do Tooltip independente do movimento do cursor</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>bottom</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Determina que o Tooltip aparecerá abaixo do conteúdo</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default TooltipDocs;
