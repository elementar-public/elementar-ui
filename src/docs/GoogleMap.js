import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import GoogleMap from '../lib/GoogleMap';

const ImportCode = `import GoogleMap from 'elementar-ui/dist/GoogleMap';`;

const ExampleCode = `const boxStyle = {
  background: '#FFF',
  border: '1px solid #BBB',
  borderRadius: 4,
  padding: 10
};

...

<GoogleMap
  apiKey="CHAVE_DA_API_DO_GOOGLE_MAPS"
  lat={-8.05}
  lng={-34.9}
  zoom={13}
  hideZoomControl
  hideFullScreenControl
  marker={[
    { lat: -8.0631471, lng: -34.8711184, title: 'Marco Zero do Recife' },
    { lat: -8.0631001, lng: -34.8884961, title: 'Mercado Boa Vista' },
    { lat: -8.055537, lng: -34.959112, title: 'Instituto Ricardo Brennand' },
    { lat: -8.1309239, lng: -34.8991826, title: 'Praia de Boa Viagem' },
    { lat: -8.0304433, lng: -34.9250221, title: 'Museu do Homem do Nordeste' }
  ]}
>
  <div style={boxStyle} lat={-8.0701471} lng={-34.9121184}>Rio Capibaribe</div>
  <div style={boxStyle} lat={-8.0301471} lng={-34.8570184}>Rio Beberibe</div>
</GoogleMap>`;

const boxStyle = {
  background: '#FFF',
  border: '1px solid #BBB',
  borderRadius: 4,
  padding: 10
};

const GoogleMapDocs = () => (
  <Template title="Mapas" id="google-map">
    <p>O componente de <strong>Mapas</strong> permite inserir na página um mapa interativo do Google Maps, com marcadores apontando locais determinados pelo usuário.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <strong>GoogleMap</strong> pode assumir altura de tamanho fixo ou proporcional em relação à largura que ocupa em tela, assumindo caráter responsivo. A largura do mapa, por sua vez, será sempre igual à do componente-pai que acomoda-o.</p>

    <DemoBox>
      <GoogleMap
        apiKey={process.env.REACT_APP_GOOGLE_API_KEY}
        lat={-8.05}
        lng={-34.9}
        zoom={13}
        hideZoomControl
        hideFullScreenControl
        marker={[
          { lat: -8.0631471, lng: -34.8711184, title: 'Marco Zero do Recife' },
          { lat: -8.0631001, lng: -34.8884961, title: 'Mercado Boa Vista' },
          { lat: -8.055537, lng: -34.959112, title: 'Instituto Ricardo Brennand' },
          { lat: -8.1309239, lng: -34.8991826, title: 'Praia de Boa Viagem' },
          { lat: -8.0304433, lng: -34.9250221, title: 'Museu do Homem do Nordeste' }
        ]}
      >
        <div style={boxStyle} lat={-8.0701471} lng={-34.9121184}>Rio Capibaribe</div>
        <div style={boxStyle} lat={-8.0301471} lng={-34.8570184}>Rio Beberibe</div>
      </GoogleMap>
    </DemoBox>
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O componente <code>GoogleMap</code> possui alguns parâmetros que perimtem controlar a exibição e interação do mapa.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>height</TableCell>
          <TableCell italic>number, string</TableCell>
          <TableCell italic>56,25%</TableCell>
          <TableCell>Controla a altura do mapa em relação à largura do mapa</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lat</TableCell>
          <TableCell italic>float</TableCell>
          <TableCell italic>-15.793889</TableCell>
          <TableCell>Indica a latitude da posição central do mapa ao ser exibido</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lng</TableCell>
          <TableCell italic>float</TableCell>
          <TableCell italic>-47.882778</TableCell>
          <TableCell>Indica a longitude da posição central do mapa ao ser exibido</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>zoom</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>11</TableCell>
          <TableCell>Indica a precisão inicial do mapa ao ser exibido</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>marker</TableCell>
          <TableCell italic>object</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Adiciona um ou mais marcadores ao mapa</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>hideZoomControl</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>true</TableCell>
          <TableCell>Exibe ou oculta o controle de precisão do mapa</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>hideFullScreenControl</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Exibe ou oculta o controle de exibição em tela cheia do mapa</TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <p>Ao utilizar múltiplos marcadores, o conteúdo da propriedade <code>marker</code> deve ser um Array com as propriedades abaixo:</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>lat</TableCell>
          <TableCell italic>float</TableCell>
          <TableCell italic>props.lat</TableCell>
          <TableCell>Indica a latitude da posição central do marcador ao ser exibido</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lng</TableCell>
          <TableCell italic>float</TableCell>
          <TableCell italic>props.lng</TableCell>
          <TableCell>Indica a longitude da posição central do marcador ao ser exibido</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>title</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>Marcador</TableCell>
          <TableCell>Adiciona um título ao marcador</TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <p>O componente também permite adicionar elementos-filhos dentro de si como marcadores customizáveis. Cada elemento ou componente deve declarar, em suas propriedades, a latitude e longitude onde ele deve ser posicionado.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>lat</TableCell>
          <TableCell italic>float</TableCell>
          <TableCell italic>props.lat</TableCell>
          <TableCell>Indica a latitude da posição central do marcador customizado ao ser exibido</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lng</TableCell>
          <TableCell italic>float</TableCell>
          <TableCell italic>props.lng</TableCell>
          <TableCell>Indica a longitude da posição central do marcador customizado ao ser exibido</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default GoogleMapDocs;
