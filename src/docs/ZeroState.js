import React, { useState } from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import ZeroState from '../lib/ZeroState';

const ImportCode = `import ZeroState from 'elementar-ui/dist/ZeroState';`;

const ExampleCode = [
`<ZeroState
  message="Esta é uma tela em branco. Clique no botão abaixo para criar seu primeiro item."
  action={{
    onClick: () => window.confirm('Deseja criar um item?'),
    label: 'Criar item'
  }}
/>`,
`<ZeroState
  message="Houve um erro importante no sistema. Deseja tentar novamente ou fazer outra ação?"
  error={new Error('Houve um erro')}
  action={{
    onClick: () => window.confirm('Deseja fazer outra ação?'),
    label: 'Fazer outra ação'
  }}
  callback={{
    onClick: () => window.alert('Tentando novamente'),
    label: 'Tentar novamente',
    pending: true
  }}
/>`
];

const ErrorDocs = () => {
  const [pending, togglePending] = useState(false);
  const [retry, toggleRetry] = useState(false);

  const turnPending = () => {
    const confirm = window.confirm('Deseja criar um item?');

    if (confirm) {
      togglePending(true);

      setTimeout(() => togglePending(false), 3000);
    }
  }

  const turnRetry = () => {
    toggleRetry(true);

    setTimeout(() => toggleRetry(false), 3000);
  }

  return (
    <Template title="ZeroState" id="zerostate">
      <p>O componente <strong>ZeroState</strong> mostra uma interface apropriada para uma tela que não dispõe de informação suficiente para ser exibida ao usuário.</p>

      <CodeBox>{ImportCode}</CodeBox>

      <p>Com o <code>ZeroState</code>, é possível dar mensagens instrutivas ao usuário acerca de uma ação necessária para que ele possa usar plenamente as funções de uma interface específica. Além da instrução, é possível adicionar um botão de ação ao componente.</p>

      <DemoBox>
        <ZeroState
          message="Esta é uma tela em branco. Clique no botão abaixo para criar seu primeiro item."
          action={{
            onClick: turnPending,
            label: 'Criar item',
            pending
          }}
        />
      </DemoBox>
      <CodeBox>{ExampleCode[0]}</CodeBox>

      <p>O <code>ZeroState</code> pode ajudar o usuário em situações de erro crítico no sistema, quando uma falha acaba comprometendo a exibição de um componente ou uma tela inteira. O componente pode exibir uma breve descrição do erro, além de botões para ações de nova tentativa ou desistência.</p>

      <DemoBox>
        <ZeroState
          message="Houve um erro importante no sistema. Deseja tentar novamente ou fazer outra ação?"
          error={new Error('Houve um erro')}
          action={{
            onClick: () => window.confirm('Deseja fazer outra ação?'),
            label: 'Fazer outra ação'
          }}
          callback={{
            onClick: turnRetry,
            label: 'Tentar novamente',
            pending: retry
          }}
        />
      </DemoBox>
      <CodeBox>{ExampleCode[1]}</CodeBox>

      <p>O componente <code>ZeroState</code> possui alguns parâmetros que permitem controlar a exibição do componente.</p>

      <Table closed striped>
        <TableHead>
          <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>

        <TableBody>
          <TableRow>
            <TableCell bold>message</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>null</TableCell>
            <TableCell>Especifica a mensagem exibida para o usuário</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>action</TableCell>
            <TableCell italic>object</TableCell>
            <TableCell italic>null</TableCell>
            <TableCell>Especifica uma ação padrão para a tela de ZeroState</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>callback</TableCell>
            <TableCell italic>object</TableCell>
            <TableCell italic>null</TableCell>
            <TableCell>Especifica uma ação de nova tentativa para ZeroStates decorrentes de um erro</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>error</TableCell>
            <TableCell italic>object</TableCell>
            <TableCell italic>null</TableCell>
            <TableCell>Atribui um objeto <code>Error</code> para a mensagem de erro</TableCell>
          </TableRow>
        </TableBody>
      </Table>

      <p>Os objetos <code>action</code> e <code>callback</code> podem receber os seguintes parâmetros:</p>

      <Table closed striped>
        <TableHead>
          <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>

        <TableBody>
          <TableRow>
            <TableCell bold>label</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>Confirmar, Parar, Cancelar</TableCell>
            <TableCell>Especifica o texto dentro de cada botão</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>disabled</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Desabilita o botão</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>pending</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Coloca o botão em modo de espera</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>onClick</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica as ações de cada botão da caixa de diálogo</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Template>
  );
}

export default ErrorDocs;
