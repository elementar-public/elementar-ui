import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import ProgressBar from '../lib/ProgressBar';

const ImportCode = `import ProgressBar from 'elementar-ui/dist/ProgressBar';`;

const ExampleCode = `<ProgressBar progress={62} color="default" />
      
<ProgressBar progress={{ progress: 47, color: 'success', align: 'right', message: 'Progresso: $$%' }} height="30px" rounded remaining />

<ProgressBar
  height="40px"
  progress={[
    { progress: 35, color: 'primary', align: 'left', message: 'Documentos' },
    { progress: 17, color: 'success', align: 'left', message: 'Vídeos' },
    { progress: 20, color: 'warning', align: 'left', message: 'Fotos' },
    { progress: 4,  color: 'danger', align: 'left', message: 'Outros' },
    { progress: 5,  color: 'default', labelHidden: true }
  ]}
  remaining="Espaço livre: $$%"
/>
`;

const ProgressBarDocs = () => (
  <Template title="Barras de progresso" id="progress-bar">
    <p>O componente de <strong>barras de progresso</strong> permite dar ao usuário uma resposta visual compreensível sobre o andamento de um processo em curso. O componente de barras de progresso pode acomodar vários segmentos que mostram etapas de um processo corrente.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>A barra de progresso pode ter a informação de progresso encaminhada por props separadas no componente, por um objeto ou por um <em>array</em> de objetos. Este último caso permite mostrar várias barras dentro do espaço delimitado.</p>

    <DemoBox>
      <ProgressBar progress={62} color="default" />
      
      <ProgressBar progress={{ progress: 47, color: 'success', align: 'right', message: 'Progresso: $$%' }} height="30px" rounded remaining />
      
      <ProgressBar
        height="40px"
        progress={[
          { progress: 35, color: 'primary', align: 'left', message: 'Documentos' },
          { progress: 17, color: 'success', align: 'left', message: 'Vídeos' },
          { progress: 20, color: 'warning', align: 'left', message: 'Fotos' },
          { progress: 4,  color: 'danger', align: 'left', message: 'Outros' },
          { progress: 5,  color: 'default', labelHidden: true }
        ]}
        remaining="Espaço livre: $$%"
      />
    </DemoBox>

    <CodeBox>{ExampleCode}</CodeBox>

    <p>O componente <code>ProgressBar</code> possui alguns parâmetros que permitem customizar a aparência da barra e dos indicadores.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>default</TableCell>
          <TableCell>Especifica a cor da barra indicadora</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>progress</TableCell>
          <TableCell italic>integer, object, array</TableCell>
          <TableCell italic>0</TableCell>
          <TableCell>Especifica o progresso da barra indicadora, ou um objeto com seus atributos, ou ainda um <em>array</em> de indicadores</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>align</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>center</TableCell>
          <TableCell>Especifica o alinhamento do texto indicador de progresso</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>message</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica a mensagem do texto indicador de progresso</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>labelHidden</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se a mensagem do texto indicador de progresso deve aparecer</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>rounded</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se a barra de progresso deve ter extremidades arredondadas</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>height</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>20px</TableCell>
          <TableCell>Especifica a altura total da barra de progresso</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>remaining</TableCell>
          <TableCell italic>bool, string</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se a barra de progresso deve mostrar algum conteúdo sobre o espaço não preenchido</TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <p>Quando se utiliza um objeto para definir os atributos de uma ou mais barras indicadoras, podem ser utilizados alguns parâmetros semelhantes.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>default</TableCell>
          <TableCell>Especifica a cor da barra indicadora</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>progress</TableCell>
          <TableCell italic>integer, object, array</TableCell>
          <TableCell italic>0</TableCell>
          <TableCell>Especifica o progresso da barra indicadora, ou um objeto com seus atributos, ou ainda um <em>array</em> de indicadores</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>align</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>center</TableCell>
          <TableCell>Especifica o alinhamento do texto indicador de progresso</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>message</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica a mensagem do texto indicador de progresso</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>labelHidden</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se a mensagem do texto indicador de progresso deve aparecer</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default ProgressBarDocs;
