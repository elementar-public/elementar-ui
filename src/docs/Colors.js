import React from 'react';
import styled from 'styled-components';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Button from '../lib/Button';

const ColorGrid = styled.ul`
  display: grid;
  grid-template: auto / repeat(auto-fit, minmax(250px, 1fr));
  grid-gap: 20px;
  margin: 40px 0 100px;
  padding: 0;
  list-style: none;

  > li {
    border-radius: 10px;
    overflow: hidden;
    box-shadow: 0 4px 8px rgba(0,0,0,0.05), inset 0 0 0 1px rgba(0,0,0,0.1);
  }
`;

const ColorChip = styled.dl`
  background: ${({color}) => color};
  color: ${({negative}) => negative ? 'white' : 'inherit'};
  padding: 20px;
  display: flex;
  flex-wrap: wrap;
  font-size: 0.85rem;
  line-height: 1.4rem;

  dt {
    flex: 0 0 40%;
    margin: 0;
    padding: 0;
  }
  
  dd {
    flex: 0 0 60%;
    margin: 0;
    padding: 0;
    font-weight: bold;
  }
`;

const ColorDefinition = styled.dl`
  padding: 10px 20px 20px;
  line-height: 1.3rem;

  dt {
    font-size: 0.75rem;
    text-transform: uppercase;
    letter-spacing: 2px;
    margin: 10px 0 0;
    padding: 0;
    color: #89A;
  }

  dd {
    margin: 0;
    padding: 0;
  }
`;

const ExampleCode = `<Button label="Default"/>
<Button label="Primary" color="primary"/>
<Button label="Success" color="success"/>
<Button label="Warning" color="warning"/>
<Button label="Danger" color="danger"/>
<Button label="Help" color="help"/>
<Button label="Dimmed" color="dimmed"/>`;

const ColorsDocs = () => (
  <Template title="Cores" id="cores">
    <p>O esquema de cores da Elementar é um componente importante para a identidade da marca e a consistência entre interfaces, documentos e materiais multimídia produzidos. Com o uso correto de cores, é possível comunicar com maior clareza informações que levam à precepção de situações e tomada de decisões importantes.</p>

    <p>As cores primárias da paleta são recomendadas para o uso corriqueiro, proporcionando um código cromático simples e efetivo para as principais finalidades.</p>

    <ColorGrid>
      <li>
        <ColorChip color="#0099DD" negative>
          <dt>HEX</dt>
          <dd>#0099DD</dd>
          <dt>RGB</dt>
          <dd>0, 153, 221</dd>
          <dt>CMYK</dt>
          <dd>100, 0, 0, 10</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>

        <ColorDefinition>
          <dt>Nome</dt>
          <dd><strong>Azul Elementar</strong></dd>
          <dt>Uso</dt>
          <dd>Cor principal da marca, utilizado para destacar elementos pontuais</dd>
        </ColorDefinition>
      </li>

      <li>
        <ColorChip color="#0044BB" negative>
          <dt>HEX</dt>
          <dd>#0044BB</dd>
          <dt>RGB</dt>
          <dd>0, 68, 187</dd>
          <dt>CMYK</dt>
          <dd>100, 40, 0, 20</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>

        <ColorDefinition>
          <dt>Nome</dt>
          <dd><strong>Azul Escuro Elementar</strong></dd>
          <dt>Uso</dt>
          <dd>Tonalidade complementar, para conferir destaque a outros elementos quando o Azul principal for usado</dd>
        </ColorDefinition>
      </li>

      <li>
        <ColorChip color="#99DDFF">
          <dt>HEX</dt>
          <dd>#99DDFF</dd>
          <dt>RGB</dt>
          <dd>153, 221, 255</dd>
          <dt>CMYK</dt>
          <dd>40, 0, 0, 0</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>

        <ColorDefinition>
          <dt>Nome</dt>
          <dd><strong>Azul Claro Elementar</strong></dd>
          <dt>Uso</dt>
          <dd>Tonalidade complementar, quando os outros tons de Azul estiverem em uso</dd>
        </ColorDefinition>
      </li>

      <li>
        <ColorChip color="#DDE6EE">
          <dt>HEX</dt>
          <dd>#DDE6EE</dd>
          <dt>RGB</dt>
          <dd>136, 153, 170</dd>
          <dt>CMYK</dt>
          <dd>0, 0, 0, 10</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>

        <ColorDefinition>
          <dt>Nome</dt>
          <dd><strong>Cinza Claro Elementar</strong></dd>
          <dt>Uso</dt>
          <dd>Para conferir destaque sutil a elementos como cor de fundo</dd>
        </ColorDefinition>
      </li>

      <li>
        <ColorChip color="#8899AA" negative>
          <dt>HEX</dt>
          <dd>#8899AA</dd>
          <dt>RGB</dt>
          <dd>136, 153, 170</dd>
          <dt>CMYK</dt>
          <dd>20, 0, 0, 50</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>

        <ColorDefinition>
          <dt>Nome</dt>
          <dd><strong>Cinza Elementar</strong></dd>
          <dt>Uso</dt>
          <dd>Para reduzir o destaque de elementos pouco relevantes</dd>
        </ColorDefinition>
      </li>

      <li>
        <ColorChip color="#111A22" negative>
          <dt>HEX</dt>
          <dd>#111A22</dd>
          <dt>RGB</dt>
          <dd>17, 26, 34</dd>
          <dt>CMYK</dt>
          <dd>0, 0, 0, 100</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>

        <ColorDefinition>
          <dt>Nome</dt>
          <dd><strong>Preto Elementar</strong></dd>
          <dt>Uso</dt>
          <dd>Para cor de texto, ou como fundo para usos excepcionais de contraste negativo</dd>
        </ColorDefinition>
      </li>
    </ColorGrid>

    <p>Em casos excepcionais, que demandam o uso de mais cores para estabelecer distinção entre múltiplas entidades, ou transmitir corretamente uma conotação importante, é possível o uso de cores complementares. O uso dessas cores deve ser pontual, de forma que não descaracterize o material em produção.</p>

    <ColorGrid>
      <li>
        <ColorChip color="#F6F8FB">
          <dt>HEX</dt>
          <dd>#F6F8FB</dd>
          <dt>RGB</dt>
          <dd>102, 255, 187</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#EAF0F6">
          <dt>HEX</dt>
          <dd>#EAF0F6</dd>
          <dt>RGB</dt>
          <dd>0, 221, 136</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#CCD6DD">
          <dt>HEX</dt>
          <dd>#CCD6DD</dd>
          <dt>RGB</dt>
          <dd>0, 170, 102</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
      </li>

      <li>
        <ColorChip color="#66FFBB">
          <dt>HEX</dt>
          <dd>#66FFBB</dd>
          <dt>RGB</dt>
          <dd>102, 255, 187</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#00DD88" negative>
          <dt>HEX</dt>
          <dd>#00DD88</dd>
          <dt>RGB</dt>
          <dd>0, 221, 136</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
        <ColorChip color="#00AA66" negative>
          <dt>HEX</dt>
          <dd>#00AA66</dd>
          <dt>RGB</dt>
          <dd>0, 170, 102</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
      </li>
      
      <li>
        <ColorChip color="#FFEE99">
          <dt>HEX</dt>
          <dd>#FFEE99</dd>
          <dt>RGB</dt>
          <dd>255, 238, 153</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#FFDD00">
          <dt>HEX</dt>
          <dd>#FFDD00</dd>
          <dt>RGB</dt>
          <dd>255, 221, 0</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#DDBB00" negative>
          <dt>HEX</dt>
          <dd>#DDBB00</dd>
          <dt>RGB</dt>
          <dd>221, 187, 0</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
      </li>
      
      <li>
        <ColorChip color="#FFCC66">
          <dt>HEX</dt>
          <dd>#FFCC66</dd>
          <dt>RGB</dt>
          <dd>255, 238, 153</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#FF8800">
          <dt>HEX</dt>
          <dd>#FF8800</dd>
          <dt>RGB</dt>
          <dd>255, 221, 0</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#CC6600" negative>
          <dt>HEX</dt>
          <dd>#CC6600</dd>
          <dt>RGB</dt>
          <dd>221, 187, 0</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
      </li>
      
      <li>
        <ColorChip color="#FFB6BB">
          <dt>HEX</dt>
          <dd>#FFB6BB</dd>
          <dt>RGB</dt>
          <dd>255, 182, 187</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#EE2244" negative>
          <dt>HEX</dt>
          <dd>#EE2244</dd>
          <dt>RGB</dt>
          <dd>238, 34, 68</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
        <ColorChip color="#BB1133" negative>
          <dt>HEX</dt>
          <dd>#BB1133</dd>
          <dt>RGB</dt>
          <dd>187, 17, 51</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
      </li>
      
      <li>
        <ColorChip color="#EEBBFF">
          <dt>HEX</dt>
          <dd>#EEBBFF</dd>
          <dt>RGB</dt>
          <dd>238, 187, 255</dd>
          <dt>Contraste</dt>
          <dd>Positivo</dd>
        </ColorChip>
        <ColorChip color="#BB22DD" negative>
          <dt>HEX</dt>
          <dd>#BB22DD</dd>
          <dt>RGB</dt>
          <dd>187, 34, 221</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
        <ColorChip color="#771199" negative>
          <dt>HEX</dt>
          <dd>#771199</dd>
          <dt>RGB</dt>
          <dd>119, 17, 153</dd>
          <dt>Contraste</dt>
          <dd>Negativo</dd>
        </ColorChip>
      </li>
    </ColorGrid>

    <p>Alguns componentes do Elementar UI podem ser coloridos utilizando nomes para trios de cores. Por padrão, o Elementar UI utiliza as mesmas cores da marca da Elementar para seus componentes; no entanto, essas configurações poder ser customizadas com a criação de um arquivo adicional de tema, em que os sets de cores são definidos como chaves dentro de um objeto com valores pré-definidos.</p>

    <p>A convenção do Elementar UI determina nomes contextuais de cores, como <code>primary</code>, <code>success</code>, <code>warning</code>, <code>danger</code> e <code>dimmed</code>. Outros nomes podem ser adicionados, e os valores desses sets de cores também pode ser customizado de acordo com as necessidades do projeto.</p>

    <DemoBox>
      <Button label="Default"/>&nbsp;
      <Button label="Primary" color="primary"/>&nbsp;
      <Button label="Success" color="success"/>&nbsp;
      <Button label="Warning" color="warning"/>&nbsp;
      <Button label="Danger" color="danger"/>&nbsp;
      <Button label="Help" color="help"/>&nbsp;
      <Button label="Dimmed" color="dimmed"/>
    </DemoBox>
    
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O uso de cores em interfaces é recomendado para os seguintes contextos:</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Uso</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>default</TableCell>
          <TableCell>Cor padrão para elementos secundários na interface</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>primary</TableCell>
          <TableCell>Cor de ação principal, usada para concluir uma tarefa comum</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>success</TableCell>
          <TableCell>Cor de ação usada para concluir um fluxo composto de múltiplas tarefas em sequência</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>warning</TableCell>
          <TableCell>Cor de ação usada para concluir uma tarefa que demanda atenção redobrada do usuário</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>danger</TableCell>
          <TableCell>Cor de ação usada para concluir uma tarefa com efeito irreversível para o usuário</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>help</TableCell>
          <TableCell>Cor de ação usada para conduzir o usuário a uma documentação de ajuda</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>dimmed</TableCell>
          <TableCell>Cor utilizada para elementos de relevância reduzida</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default ColorsDocs;