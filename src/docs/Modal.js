import React, {useEffect, useState} from 'react';
import Template, {CodeBox, DemoBox, TableBox} from '../components/template';
import Table, {TableHead, TableBody, TableRow, TableHeading, TableCell} from '../lib/Table';
import Modal from '../lib/Modal';
import Button from '../lib/Button';

const ImportCode = `import Modal from 'elementar-ui/dist/Modal';`;

const ExampleCode = `state = {
  modal: false
}

toggleModal = () => this.setState({ state: !this.state.modal });

...

<Button
  label="Botão"
  color="primary"
  rounded
  onClick={this.toggleModal}
  width="600px"
/>

<Modal
  cover
  title="Estratégias em um novo paradigma globalizado"
  active={modal}
  close={this.toggleModal}
>
  <p>O empenho em analisar a execução [...] motivação departamental.</p>
  <p>Evidentemente, o comprometimento entre [...] fazemos parte.</p>
</Modal>
`;

const ModalDocs = () => {
  const [modal, toggleModal] = useState(false);
  const [test, toggleTest] = useState(true);

  useEffect(() => {
    if (modal) setTimeout(() => {
      toggleModal(false);
      toggleTest(false);
      setTimeout(() => toggleTest(true), 2000);
    }, 2000);
  });

  return (
    <Template title="Modal" id="modal">
      <p><strong>Modais</strong> são elementos de interação que mostram conteúdo de contexto específico numa camada superior ao conteúdo normal. É utilizado para mostrar conteúdo necessário para a tela, mas que não dispõe de um local apropriado para tal.</p>
  
      <CodeBox>{ImportCode}</CodeBox>
  
      <p>Modais, por padrão, sempre aparecem com visualização desabilitada. É recomendado que se tenha, dentro do componente a ser escrito, uma variável atrelada a uma função que controle adequadamente a visualização do modal. O compontente de modais possui um modo auto-destrutivo de exibição que permite que o conteúdo seja exibido uma vez na tela, mas ao ser fechado, ele é removido da tela.</p>
  
      {test &&
        <DemoBox>
          <Button label="Ativar modal" color="primary" rounded onClick={() => toggleModal(!modal)} />

          {modal &&
            <Modal
              title="Estratégias em um novo paradigma globalizado"
              cover
              active={modal}
              close={() => toggleModal(!modal)}
              height="600px"
              width="600px"
            >
              <p>O empenho em analisar a execução dos pontos do programa assume importantes posições no estabelecimento dos relacionamentos verticais entre as hierarquias. No mundo atual, o fenômeno da Internet talvez venha a ressaltar a relatividade das diversas correntes de pensamento. É importante questionar o quanto o surgimento do comércio virtual facilita a criação dos níveis de motivação departamental.</p>
              <p>Evidentemente, o comprometimento entre as equipes possibilita uma melhor visão global do fluxo de informações. Ainda assim, existem dúvidas a respeito de como a contínua expansão de nossa atividade estende o alcance e a importância do levantamento das variáveis envolvidas. Por outro lado, o julgamento imparcial das eventualidades não pode mais se dissociar da gestão inovadora da qual fazemos parte.</p>
            </Modal>
          }
        </DemoBox>
      }

      <CodeBox>{ExampleCode}</CodeBox>
  
      <p>Quando não houver um parâmetro <code>close</code> declarado, o modal será removido da tela definitivamente ao selecionar-se a opção de fechar. Você pode utilizar outros parâmetros para configurar um elemento de modal.</p>

      <TableBox>
        <Table closed striped>
          <TableHead>
            <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
            </TableRow>
          </TableHead>

          <TableBody>
            <TableRow>
              <TableCell bold>title</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>Modal</TableCell>
              <TableCell>Insere conteúdo de texto dentro do título do modal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>active</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>false</TableCell>
              <TableCell>Controla a exibição do modal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>close</TableCell>
              <TableCell italic>function</TableCell>
              <TableCell italic>removeModal</TableCell>
              <TableCell>Especifica o comportamento da aplicação ao fechar o modal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>width</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>400px</TableCell>
              <TableCell>Especifica a largura máxima do modal</TableCell>
            </TableRow>
            <TableRow>
                <TableCell bold>height</TableCell>
                <TableCell italic>integer</TableCell>
                <TableCell italic>400px</TableCell>
                <TableCell>Especifica a altura máxima do modal</TableCell>
              </TableRow>
              <TableRow>
                <TableCell bold>minHeight</TableCell>
                <TableCell italic>integer</TableCell>
                <TableCell italic>200px</TableCell>
                <TableCell>Especifica a altura mínima do modal</TableCell>
              </TableRow>
            <TableRow>
              <TableCell bold>cover</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Aplica largura e altura máxima ao modal quando está em uma tela de dispositivo móvel</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableBox>
    </Template>
  );  
}

export default ModalDocs;