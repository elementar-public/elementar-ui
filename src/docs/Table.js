import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableFooter, TableRow, TableHeading, TableCell } from '../lib/Table';

const ImportCode = `import Table, { TableHead, TableBody, TableFooter, TableRow, TableHeading, TableCell } from 'elementar-ui/dist/Table';`;

const ExampleCode = `<Table closed striped fixed verticalAlign="top">
  <TableHead>
    <TableRow>
      <TableHeading>Região</TableHeading>
      ...
      <TableHeading>Rosquinhas</TableHeading>
    </TableRow>
  </TableHead>

  <TableBody>
    <TableRow>
      <TableCell bold>Mangatinga</TableCell>
      ...
      <TableCell>123</TableCell>
    </TableRow>
    
    ...

    <TableRow>
      <TableCell bold>Jandira</TableCell>
      ...
      <TableCell>200</TableCell>
    </TableRow>
  </TableBody>

  <TableFooter>
    <TableRow>
      <TableCell>Total</TableCell>
      ...
      <TableCell>853</TableCell>
    </TableRow>
  </TableFooter>
</Table>
`;

const TableDocs = () => (
  <Template title="Tabelas" id="tabela">
    <p>Tabelas são componentes utilizados para mostrar dados tabulares e organiza-los de forma bidimensional. A formatação de tabelas pode ser utilizada ao importar o componente <strong>Table</strong> e seus derivados.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>As tabelas e as células dispõem de opções de formatação pré-configuradas, podendo ser utilizadas com atributos utilizados em seus componentes. Os demais não possuem opções de configuração.</p>

    <DemoBox>
      <Table closed striped fixed verticalAlign="top">
        <TableHead>
          <TableRow>
            <TableHeading>Região</TableHeading>
            <TableHeading>Hot dog</TableHeading>
            <TableHeading>Hamburguer</TableHeading>
            <TableHeading>Sanduíche</TableHeading>
            <TableHeading>Kebab</TableHeading>
            <TableHeading>Fritas</TableHeading>
            <TableHeading>Rosquinhas</TableHeading>
          </TableRow>
        </TableHead>

        <TableBody>
          <TableRow>
            <TableCell bold>Mangatinga</TableCell>
            <TableCell number>184</TableCell>
            <TableCell number>96</TableCell>
            <TableCell number>15</TableCell>
            <TableCell number>97</TableCell>
            <TableCell number>168</TableCell>
            <TableCell number>123</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>Tavares</TableCell>
            <TableCell number>87</TableCell>
            <TableCell number>159</TableCell>
            <TableCell number>5</TableCell>
            <TableCell number>184</TableCell>
            <TableCell number>68</TableCell>
            <TableCell number>32</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>Centro Franco</TableCell>
            <TableCell number>176</TableCell>
            <TableCell number>185</TableCell>
            <TableCell number>100</TableCell>
            <TableCell number>18</TableCell>
            <TableCell number>13</TableCell>
            <TableCell number>193</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>Haidée</TableCell>
            <TableCell number>88</TableCell>
            <TableCell number>104</TableCell>
            <TableCell number>36</TableCell>
            <TableCell number>139</TableCell>
            <TableCell number>195</TableCell>
            <TableCell number>108</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>Penteado</TableCell>
            <TableCell number>127</TableCell>
            <TableCell number>4</TableCell>
            <TableCell number>46</TableCell>
            <TableCell number>180</TableCell>
            <TableCell number>119</TableCell>
            <TableCell number>139</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>Melo Rocha</TableCell>
            <TableCell number>180</TableCell>
            <TableCell number>192</TableCell>
            <TableCell number>110</TableCell>
            <TableCell number>9</TableCell>
            <TableCell number>81</TableCell>
            <TableCell number>58</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>Jandira</TableCell>
            <TableCell number>165</TableCell>
            <TableCell number>144</TableCell>
            <TableCell number>62</TableCell>
            <TableCell number>3</TableCell>
            <TableCell number>100</TableCell>
            <TableCell number>200</TableCell>
          </TableRow>
        </TableBody>
        <TableFooter>
          <TableRow>
            <TableCell>Total</TableCell>
            <TableCell number>1007</TableCell>
            <TableCell number>884</TableCell>
            <TableCell number>374</TableCell>
            <TableCell number>630</TableCell>
            <TableCell number>744</TableCell>
            <TableCell number>853</TableCell>
          </TableRow>
        </TableFooter>
      </Table>
    </DemoBox>
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O componente <code>Table</code> possui alguns parâmetros que permitem customizar a aparência geral da tabela.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>closed</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Delimita a tabela dentro de uma borda e aplica efeito de sombra</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>striped</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Intercala o preenchimento das linhas entre preenchimento claro e escuro</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>compact</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Reduz o espaçamento interno de todas as células</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>fixed</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Fixa a largura das colunas proporcionalmente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>verticalAlign</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>middle</TableCell>
          <TableCell>Alinha o conteúdo das células verticalmente, no topo, base ou centro</TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <p>O componente <code>TableCell</code> possui alguns parâmetros que permitem customizar a aparência individual de células.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>bold</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica peso negrito para o conteúdo dentro da célula</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>italic</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica estilo itálico para o conteúdo dentro da célula</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>number</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Alinha o conteúdo da célula para a direita</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>align</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>left, center, right</TableCell>
          <TableCell>Alinha o conteúdo da célula para a direção desejada</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default TableDocs;
