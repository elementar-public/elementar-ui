import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Button from '../lib/Button';

const ImportCode = `import Button from 'elementar-ui/dist/Button';`;

const ExampleCode = [`<Button label="Default" leftIcon={BackIcon}/>
<Button label="Primary" color="primary"/>
<Button label="Success" color="success"/>
<Button label="Warning" color="warning"/>
<Button label="Danger" color="danger"/>
<Button label="Hollow" color="hollow" rightIcon={ForwardIcon}/>
`,
`<Button label="Default" size="small"/>
<Button label="Default"/>
<Button label="Default" size="large"/>
<Button label="Default" size="xLarge"/>
`,
`<Button label="Outline" leftIcon={BackIcon} outline radius="0"/>
<Button label="Rounded" size="small" color="danger" rounded/>
<Button label="Disabled" color="warning" disabled size="large"/>
<Button label="Loading" color="success" loading={true} rightIcon={ForwardIcon} outline/>
<Button label="Loading" color="primary" loading={true} rightIcon={ForwardIcon} rounded/>
`,
`import Theme from 'elementar-ui/dist/Theme';

const theme = Theme;
theme.buttons.borderRadius = 0;
`];

const BackIcon = () => (
  <svg width="18" height="18" viewBox="0,0,18,18" fill="currentColor">
    <path d="M10,2 l-7,7 l7,7" fill="none" stroke="currentColor" strokeWidth="2px" />
    <rect x="0" y="0" width="18" height="18" fill="none" />
  </svg>
);

const ForwardIcon = () => (
  <svg width="18" height="18" viewBox="0,0,18,18" fill="currentColor">
    <path d="M8,2 l7,7 l-7,7" fill="none" stroke="currentColor" strokeWidth="2px" />
    <rect x="0" y="0" width="18" height="18" fill="none" />
  </svg>
);

const ButtonDocs = () => (
  <Template title="Botões" id="buttons">
    <p>Os <strong>botões</strong> são importantes elementos de interação para o usuário, pois servem como controles para as ações desejadas pelo usuário. Os botões podem levar o usuário para outro lugar ou causar alguma mudança desejada pelo usuário na tela.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Os botões podem utilizar diversas cores de fundo para representar o contexto da ação que desempenham, além de poderem ter vários tamanhos pré-determinados e formato redondo.</p>

    <DemoBox>
      <Button label="Default" leftIcon={BackIcon}/>&nbsp;
      <Button label="Primary" color="primary"/>&nbsp;
      <Button label="Success" color="success"/>&nbsp;
      <Button label="Warning" color="warning"/>&nbsp;
      <Button label="Danger" color="danger"/>&nbsp;
      <Button label="Hollow" rightIcon={ForwardIcon} color="hollow"/>
    </DemoBox>
    
    <CodeBox>{ExampleCode[0]}</CodeBox>

    <DemoBox>
      <Button label="Small" size="small"/>&nbsp;
      <Button label="Default"/>&nbsp;
      <Button label="Large" size="large"/>&nbsp;
      <Button label="xLarge" size="xLarge"/>
    </DemoBox>
    
    <CodeBox>{ExampleCode[1]}</CodeBox>

    <DemoBox>
      <Button label="Outline" leftIcon={BackIcon} outline radius="0"/>&nbsp;
      <Button label="Rounded" size="small" color="danger" rounded/>&nbsp;
      <Button label="Disabled" color="warning" disabled size="large"/>&nbsp;
      <Button label="Loading" color="success" loading={true} rightIcon={ForwardIcon} outline/>&nbsp;
      <Button label="Loading" color="primary" loading={true} rightIcon={ForwardIcon} rounded/>
    </DemoBox>
    
    <CodeBox>{ExampleCode[2]}</CodeBox>

    <p>Os esquemas de cores dos botões seguem as cores definidas no tema da aplicação. Outros parâmetros podem ser alterados também modificando o tema principal da aplicação.</p>

    <CodeBox>{ExampleCode[3]}</CodeBox>

    <p>Além destes comandos básicos, outros parâmetros podem ser utilizados para dar mais versatilidade aos botões.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading head>Parâmetro</TableHeading>
          <TableHeading head>Tipo</TableHeading>
          <TableHeading head>Padrão</TableHeading>
          <TableHeading head>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>label</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>label</TableCell>
          <TableCell>Aplica um texto específico no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>size</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>small, large, xLarge</TableCell>
          <TableCell>Aplica tamanhos específicos no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>outline</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica esquema de cores de fundo branco no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>rounded</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica bordas redondas ao botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>radius</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Arredonda os cantos do botão na medida desejada</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>primary, success, warning, alert, hollow</TableCell>
          <TableCell>Aplica cor de fundo específica no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>fullWidth</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Permite ao boão ocupar toda a largura do elemento contenedor</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>disabled</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Configura o botão como desativado</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>loading</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Desabilita o botão e ativa a animação de carregamento no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>type</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>button, submit, reset</TableCell>
          <TableCell>Aplica o atributo "type" do botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>leftIcon</TableCell>
          <TableCell italic>object</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Posiciona um ícone à esquerda do texto do botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>rightIcon</TableCell>
          <TableCell italic>object</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Posiciona um ícone à direita do texto do botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>to</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Direciona o usuário para outra página interna</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>href</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Direciona o usuário para uma página externa</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default ButtonDocs;
