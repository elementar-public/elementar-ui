import React, {useState} from 'react';
import Template, {CodeBox, DemoBox, TableBox} from '../components/template';
import Table, {TableHead, TableBody, TableRow, TableHeading, TableCell} from '../lib/Table';
import Context from '../lib/Context';
import Button from '../lib/Button';
import Select from '../lib/Select';
import Selection from '../lib/Selection';
import TextInput from '../lib/TextInput';

const ImportCode = `import Context from 'elementar-ui/dist/Context';`;

const ExampleCode = [
`const [context, toggleContext] = useState(false);
const [phone, handlePhone] = useState('');
const [sms, toggleSms] = useState(false);
const [pending, togglePending] = useState(false);

const saveContext = () => {
  {...}
}

...

<Context
  active={context}
  close={() => toggleContext(false)}
  size="small"
  confirm={{
    action: saveContext,
    label: 'Enviar',
    pending: pending,
    disabled: phone === ''
  }}
  dismiss={{
    label: 'Fechar'
  }}
>
  <TextInput
    label="Digite seu telefone"
    value={phone}
    onChange={({target}) => handlePhone(target.value)}
    placeholder="(__) _____-____"
    mask={['(', /\\d/, /\\d/, ')', ' ', /\\d/, /\\d/, /\\d/, /\\d/, /\\d/, '-', /\\d/, /\\d/, /\\d/, /\\d/ ]}
  />

  <Selection
    type="checkbox"
    name="sms"
    value={sms}
    checked={sms}
    onChange={() => toggleSms(!sms)}
    label="Desejo receber SMS neste número"
  />
</Context>`,

`const [context, toggleContext] = useState(false);
const [step, handleStep] = useState(1);
const [name, handleName] = useState('');
const [gender, handleGender] = useState(false);
const [email, handleEmail] = useState('');
const [optIn, toggleOptIn] = useState(0);
const [pending, togglePending] = useState(false);

const handleSubmit = (e) => {
  e.preventDefault();

  {...}
}

const increaseStep = () => handleStep(step + 1);

const decreaseStep = () => handleStep(step - 1);

const checkBlockedStep = () => {
  switch (step) {
    case 1:
      return name === '' ? true : false;
    case 2:
      return gender === 'none' ? true : false;
    case 3:
      return email === '' || optIn === 0 ? true : false;
    default:
      return true;
  }
}

{...}

<Context
  title="Dados para contato"
  active={context}
  close={() => toggleContext(false)}
  onSubmit={handleSubmit}
  size="large"
  progress={{
    step: step,
    steps: 3
  }}
  confirm={{
    action: increaseStep,
    type: step === 3 ? 'submit' : 'button',
    label: step === 3 ? 'Enviar' : 'Avançar',
    color: step === 3 ? 'success' : undefined,
    pending: pending,
    disabled: checkBlockedStep()
  }}
  dismiss={{
    action: step === 1 ? closeContextStep : decreaseStep,
    label: step === 1 ? 'Fechar' : 'Voltar'
  }}
  otherActions={[
    {action: closeContextStep, label: 'Fechar agora', color: 'danger'},
    {action: showHiddenContentStep, label: 'Ver conteúdo escondido'},
  ]}
>
  {step === 1 &&
    <TextInput
      label="Seu nome"
      value={name}
      onChange={({target}) => handleName(target.value)}
      placeholder="Digite seu nome"
    />
  }

  {step === 2 &&
    <Select
      label="Seu gênero"
      value={gender}
      onChange={({target}) => handleGender(target.value)}
      options={[
        {value: 'none', label: 'Selecione uma opção'},
        {value: 'femi', label: 'Feminino'},
        {value: 'masc', label: 'Masculino'},
        {value: 'femt', label: 'Feminino trans'},
        {value: 'mast', label: 'Masculino trans'},
        {value: 'pest', label: 'Pessoa trans'},
        {value: 'indt', label: 'Não sei responder'},
        {value: 'semr', label: 'Prefiro não responder'}
      ]}
    />
  }

  {step === 3 &&
    <>
      <TextInput
        label="Seu e-mail"
        value={email}
        onChange={({target}) => handleEmail(target.value)}
        placeholder="Digite seu e-mail"
        type="email"
      />

      <Selection
        type="radio"
        label="Desejo assinar a newsletter e saber de novidades"
        value="1"
        name="optIn"
        checked={optIn === 1}
        onChange={() => handleOptIn(1)}
      />
      
      <Selection
        type="radio"
        label="Não quero assinar a newsletter"
        value="2"
        name="optIn"
        checked={optIn === 2}
        onChange={() => handleOptIn(2)}
      />
    </>
  }
</Context>`];

const ContextDocs = () => {
  const [context, toggleContext] = useState(false);
  const [pending, togglePending] = useState(false);

  const [phone, handlePhone] = useState('');
  const [sms, toggleSms] = useState(false);
  const [message, handleMessage] = useState('');

  const [contextStep, toggleContextStep] = useState(false);
  const [pendingStep, togglePendingStep] = useState(false);
  const [step, handleStep] = useState(1);

  const [name, handleName] = useState('');
  const [gender, handleGender] = useState('none');
  const [email, handleEmail] = useState('');
  const [optIn, handleOptIn] = useState(0);
  const [messageStep, handleMessageStep] = useState('');

  const assignInitialValues = () => {
    handlePhone('');
    toggleSms(false);
  }
  
  const closeContext = () => {
    toggleContext(false);
    assignInitialValues();
  }

  const openContext = () => {
    assignInitialValues();
    toggleContext(true);
  }
  
  const saveContext = () => {
    if (phone !== '') {
      togglePending(true);

      setTimeout(() => {
        togglePending(false);
        toggleContext(false);
        handleMessage(`${sms ? 'Enviar' : 'Não enviar'} SMS para o telefone "${phone}".`);
        assignInitialValues();
      }, 2000);
    }
  }

  const assignInitialValuesStep = () => {
    handleEmail('');
    handleName('');
    handleGender('none');
    handleOptIn(0);
    handleStep(1);
  }
  
  const saveContextStep = e => {
    e.preventDefault();

    if (name !== '' && email !== '' && gender !== 'none' && optIn !== 0) {
      togglePendingStep(true);

      setTimeout(() => {
        togglePendingStep(false);
        toggleContextStep(false);
        handleMessageStep(`O usuário ${name} (${gender}) ${optIn === 2 ? 'não quer' : 'quer'} receber novidades no e-mail "${email}".`);
        assignInitialValuesStep();
      }, 2000);
    }
  }

  const increaseStep = () => handleStep(step + 1);

  const decreaseStep = () => handleStep(step - 1);

  const checkBlockedStep = () => {
    switch (step) {
      case 1:
        return name === '' ? true : false;
      case 2:
        return gender === 'none' ? true : false;
      case 3:
        return email === '' || optIn === 0 ? true : false;
      default:
        return true;
    }
  }

  const closeContextStep = () => {
    toggleContextStep(false);
    assignInitialValuesStep();
  }

  const openContextStep = () => {
    assignInitialValuesStep();
    toggleContextStep(true);
  }

  const showHiddenContentStep = () => window.alert('Olá! Eu sou um conteúdo escondido!');

  return (
    <Template title="Context (beta)" id="context">
      <p><strong>Contexts</strong> são janelas voltadas para interações complexas, orientadas para o usuário preencher formulários, alterar valores relativos à tela anterior e atribuir parâmetros adicionais a uma tarefa maior.</p>
  
      <CodeBox>{ImportCode}</CodeBox>
  
      <p>Assim como os modais, os componentes <code>Context</code> aparecem com visualização desabilitada. Contudo, como é voltado para o usuário exercer ações complexas, o componente possui controles próprios de abertura e fechamento do conteúdo. Dessa forma, o usuário terá sempre ao menos um controle visível, que o permite fechar o <code>Context</code> sem fazer maiores alterações.</p>

      <DemoBox>
        <Button label="Preencher formulário simples" rounded onClick={openContext} />

        {message !== '' && <p>{message}</p>}

        <Context
          active={context}
          close={closeContext}
          size="small"
          confirm={{
            action: saveContext,
            label: 'Enviar',
            pending: pending,
            disabled: phone === ''
          }}
          dismiss={{
            label: 'Fechar'
          }}
        >
          <TextInput
            label="Digite seu telefone"
            value={phone}
            onChange={({target}) => handlePhone(target.value)}
            placeholder="(__) _____-____"
            mask={['(', /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/ ]}
          />

          <Selection
            type="checkbox"
            name="sms"
            value={sms}
            checked={sms}
            onChange={() => toggleSms(!sms)}
            label="Desejo receber SMS neste número"
          />
        </Context>
      </DemoBox>

      <CodeBox>{ExampleCode[0]}</CodeBox>

      <p>O componente <code>Context</code> também possibilita a criação de janelas com etapas, em que o usuário pode cumprir uma etapa ao longo de múltiplas fases. com esta opção, o usuário pode ver uma barra indicativa do seu progresso ao longo do formulário.</p>
  
      <DemoBox>
        <Button label="Preencher formulário em etapas" color="primary" rounded onClick={openContextStep} />

        {messageStep !== '' && <p>{messageStep}</p>}

        <Context
          title="Dados para contato"
          active={contextStep}
          close={closeContextStep}
          onSubmit={saveContextStep}
          size="large"
          progress={{
            step: step,
            steps: 3
          }}
          confirm={{
            action: increaseStep,
            type: step === 3 ? 'submit' : 'button',
            label: step === 3 ? 'Enviar' : 'Avançar',
            color: step === 3 ? 'success' : undefined,
            pending: pendingStep,
            disabled: checkBlockedStep()
          }}
          dismiss={{
            action: step === 1 ? closeContextStep : decreaseStep,
            label: step === 1 ? 'Fechar' : 'Voltar'
          }}
          otherActions={[
            {action: closeContextStep, label: 'Fechar agora', color: 'danger'},
            {action: showHiddenContentStep, label: 'Ver conteúdo escondido'},
          ]}
        >
          {step === 1 &&
            <TextInput
              label="Seu nome"
              value={name}
              onChange={({target}) => handleName(target.value)}
              placeholder="Digite seu nome"
            />
          }

          {step === 2 &&
            <Select
              label="Seu gênero"
              value={gender}
              onChange={({target}) => handleGender(target.value)}
              options={[
                {value: 'none', label: 'Selecione uma opção'},
                {value: 'femi', label: 'Feminino'},
                {value: 'masc', label: 'Masculino'},
                {value: 'femt', label: 'Feminino trans'},
                {value: 'mast', label: 'Masculino trans'},
                {value: 'pest', label: 'Pessoa trans'},
                {value: 'indt', label: 'Não sei responder'},
                {value: 'semr', label: 'Prefiro não responder'}
              ]}
            />
          }

          {step === 3 &&
            <div>
              <TextInput
                label="Seu e-mail"
                value={email}
                onChange={({target}) => handleEmail(target.value)}
                placeholder="Digite seu e-mail"
                type="email"
              />

              <Selection
                type="radio"
                label="Desejo assinar a newsletter e saber de novidades"
                value="1"
                name="optIn"
                checked={optIn === 1}
                onChange={() => handleOptIn(1)}
              />
              
              <Selection
                type="radio"
                label="Não quero assinar a newsletter"
                value="2"
                name="optIn"
                checked={optIn === 2}
                onChange={() => handleOptIn(2)}
              />
            </div>
          }
        </Context>
      </DemoBox>

      <CodeBox>{ExampleCode[1]}</CodeBox>
  
      <p>Quando não houver um parâmetro <code>close</code> declarado, o <code>Context</code> será removido da tela definitivamente ao selecionar-se a opção de fechar. Você pode utilizar outros parâmetros para configurar um elemento <code>Context</code>.</p>

      <TableBox>
        <Table closed striped>
          <TableHead>
            <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
            </TableRow>
          </TableHead>

          <TableBody>
            <TableRow>
              <TableCell bold>title</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>Modal</TableCell>
              <TableCell>Insere conteúdo de texto dentro do título do modal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>active</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>false</TableCell>
              <TableCell>Controla a exibição do modal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>close</TableCell>
              <TableCell italic>function</TableCell>
              <TableCell italic>removeModal</TableCell>
              <TableCell>Especifica o comportamento da aplicação ao fechar o modal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>width</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>400px</TableCell>
              <TableCell>Especifica a largura máxima do modal</TableCell>
            </TableRow>
            <TableRow>
                <TableCell bold>height</TableCell>
                <TableCell italic>integer</TableCell>
                <TableCell italic>400px</TableCell>
                <TableCell>Especifica a altura máxima do modal</TableCell>
              </TableRow>
              <TableRow>
                <TableCell bold>minHeight</TableCell>
                <TableCell italic>integer</TableCell>
                <TableCell italic>200px</TableCell>
                <TableCell>Especifica a altura mínima do modal</TableCell>
              </TableRow>
            <TableRow>
              <TableCell bold>cover</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Aplica largura e altura máxima ao modal quando está em uma tela de dispositivo móvel</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableBox>
    </Template>
  );  
}

export default ContextDocs;