import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Loading from '../lib/Loading';

const ImportCode = `import Loading from 'elementar-ui/dist/Loading';`;

const ExampleCode = `<Loading />`;

const LoadingDocs = () => (
  <Template title="Loading" id="loading">
    <p>Em alguns processos básicos, o sistema precisa mostrar um estado provisório ao usuário, que indique que os recursos que ele deseja estão sendo carregados. Esse estado de espera sem tempo definido pode ser ocupado por um componente <strong>Loading</strong>.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <strong>Loading</strong> dispõe de algumas animações básicas em SVG para mostrar ao usuário um estado de carregamento do sistema. Logo, não demanda imagens adicionais para serem executadas.</p>

    <DemoBox>
      <Loading />
      <Loading type={1} color="primary"/>
      <Loading type={2} color="success"/>
      <Loading type={3} color="warning"/>
      <Loading type={4} color="danger"/>
      <Loading type={5} color="dark"/>
    </DemoBox>
    
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O <code>Loading</code> aceita alguns parâmetros que permitem configurar a aparência da animação.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>type</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..6</TableCell>
          <TableCell>Especifica a animação que será executada</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>rounded</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica laterais redondas no label</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default LoadingDocs;
