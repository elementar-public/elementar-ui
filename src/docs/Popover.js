import React from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Popover from '../lib/Popover';

const ImportCode = `import Popover from 'elementar-ui/dist/Popover';`;

const ExampleCode = `<p><Popover message="Monotype Bembo" label="Learn more" action={() => alert('Available on Adobe Fonts')}><strong>Bembo</strong></Popover> is a serif typeface created ... or <Popover position="right" bottom message="In Latin script typography, roman is one of the three main kinds of historical type, alongside blackletter and italic."><strong>roman</strong></Popover> style ...poet and cleric <Popover bottom label="See more" message="Pietro Bembo, O.S.I.H. was an Italian scholar, poet, literary theorist, member of the Knights Hospitaller and a cardinal." dark href="https://en.wikipedia.org/wiki/Pietro_Bembo"><strong>Pietro Bembo</strong></Popover>. The italic is based on work by Giovanni Antonio Tagliente, a calligrapher who worked as a printer in the 1520s, after the time of Manutius and Griffo.</p>`;

const PopoverDocs = () => {
  return (
    <Template title="Popover" id="popover">
      <p>O <strong>Popover</strong> é uma caixa temporária que permite mostrar conteúdo adicional interativo para o usuário, revelado assim que o usuário clica sobre um conteúdo de gatilho.</p>
  
      <CodeBox>{ImportCode}</CodeBox>

      <p>Assim como o <code>Tooltip</code>, um componente <code>Popover</code> pode ser formatado para utilizar dois modos de cores (claro como padrão, escuro como opcional), além de dispôr de três modos de posicionamento (central, esquerda e direita).</p>
  
      <DemoBox>
        <p><Popover message="Monotype Bembo" label="Learn more" action={() => window.alert('Available on Adobe Fonts')}><strong>Bembo</strong></Popover> is a serif typeface created by the British branch of the Monotype Corporation in 1928–1929 and most commonly used for body text. It is a member of the "old-style" of serif fonts, with its regular or <Popover position="right" bottom message="In Latin script typography, roman is one of the three main kinds of historical type, alongside blackletter and italic." label="Fechar"><strong>roman</strong></Popover> style based on a design cut around 1495 by Francesco Griffo for Venetian printer Aldus Manutius, sometimes generically called the "Aldine roman". Bembo is named for Manutius's first publication with it, a small 1496 book by the poet and cleric <Popover bottom label="See more" message="Pietro Bembo, O.S.I.H. was an Italian scholar, poet, literary theorist, member of the Knights Hospitaller and a cardinal." dark href="https://en.wikipedia.org/wiki/Pietro_Bembo"><strong>Pietro Bembo</strong></Popover>. The italic is based on work by Giovanni Antonio Tagliente, a calligrapher who worked as a printer in the 1520s, after the time of Manutius and Griffo.</p>
      </DemoBox>
      
      <CodeBox>{ExampleCode}</CodeBox>
  
      <p>Além destes comandos, você pode utilizar outros parâmetros para configurar um elemento <code>Popover</code>.</p>
  
      <Table closed striped>
        <TableHead>
          <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>

        <TableBody>
          <TableRow>
            <TableCell bold>message</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>Mensagem do Popover</TableCell>
            <TableCell>Especifica o texto do Popover</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>label</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>Fechar</TableCell>
            <TableCell>Especifica o texto do botão interno Popover</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>action</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>null</TableCell>
            <TableCell>Controla a ação do botão interno do Popover</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>to</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica um link interno de destino para o botão interno do Popover</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>href</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica um link externo de destino para o botão interno do Popover</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>dark</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica se o Popover alternará para o modo escuro</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>position</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>center</TableCell>
            <TableCell>Especifica a posição do Popover em relação ao conteúdo a que se refere</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>bottom</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Determina que o Popover aparecerá abaixo do conteúdo</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Template>
  );
};

export default PopoverDocs;
