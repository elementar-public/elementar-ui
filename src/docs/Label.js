import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Label from '../lib/Label';

const ImportCode = `import Label from 'elementar-ui/dist/Label';`;

const ExampleCode = [
`<p>Um texto normal com um <Label message="pequeno trecho" /> destacado.</p>`,
`<p>Labels podem ter <Label color="primary" message="várias cores" />, assim como <Label color="success" rounded message="formas diferentes" /> e ser apresentado em <Label color="warning" outline message="contorno" />. Você também pode deixar o conteúdo em letras <Label color="danger" uppercase message="maiúsculas" /> e combinar estes <Label rounded outline uppercase message="parâmetros" /> num só Label.</p>`
];

const LabelDocs = () => (
  <Template title="Labels" id="label">
    <p><strong>Labels</strong> são trechos de texto formatados com fundo colorido, que lembram pequenas etiquetas. Os labels existem para dar destaque a palavras e sentenças curtas.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <code>Label</code> possui parâmetros que permitem customizar aspectos visuais como cor de fundo, fonte e contorno.</p>

    <DemoBox>
      <p>Um texto normal com um <Label message="pequeno trecho" /> destacado.</p>
    </DemoBox>
    
    <CodeBox>{ExampleCode[0]}</CodeBox>

    <DemoBox>
    <p>Labels podem ter <Label bold color="primary" message="várias cores" />, assim como <Label color="success" rounded message="formas diferentes" /> e ser apresentado em <Label color="warning" outline message="contorno" />. Você também pode deixar o conteúdo em letras <Label color="danger" uppercase message="maiúsculas" /> e combinar estes <Label rounded outline uppercase message="parâmetros" /> num só Label.</p>
    </DemoBox>
    
    <CodeBox>{ExampleCode[1]}</CodeBox>

    <p>Além destes comandos, você pode utilizar outros parâmetros para configurar um elemento <code>Label</code>.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>message</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>text</TableCell>
          <TableCell>Aplica um texto específico no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>rounded</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica laterais redondas no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>outline</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica esquema de cores de fundo branco no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>primary, success, warning, alert</TableCell>
          <TableCell>Aplica cor de fundo específica no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>uppercase</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica letras maiúsculas no texto do label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>noWrap</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Impede quebra de linha do conteúdo do label</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default LabelDocs;
