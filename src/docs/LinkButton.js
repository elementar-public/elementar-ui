import React from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import LinkButton from '../lib/LinkButton';

const ImportCode = `import LinkButton from 'elementar-ui/dist/LinkButton';`;

const ExampleCode = [`<LinkButton label="Default" />
<LinkButton label="Primary" color="primary" />
<LinkButton label="Success" color="success" />
<LinkButton label="Warning" color="warning" />
<LinkButton label="Danger" color="danger" />
<LinkButton label="Hollow" color="hollow" />
`,
`<LinkButton label="Default" />
<LinkButton label="Default" small />
`];

const LinkButtonDocs = () => (
  <Template title="Botões de link" id="linkbuttons">
    <p>Os <strong>botões de link</strong> são elementos de interação complementares, compostos apenas de texto clicável para as ações desejadas pelo usuário. Os botões de link podem levar o usuário para outro lugar ou causar alguma mudança desejada pelo usuário na tela.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Os botões podem utilizar diversas cores de texto para representar o contexto da ação que desempenham, além de tamanhos regular e pequeno.</p>

    <DemoBox>
      <LinkButton label="Default" />&nbsp;
      <LinkButton label="Primary" color="primary"/>&nbsp;
      <LinkButton label="Success" color="success"/>&nbsp;
      <LinkButton label="Warning" color="warning"/>&nbsp;
      <LinkButton label="Danger" color="danger"/>&nbsp;
      <LinkButton label="Hollow" color="hollow"/>
    </DemoBox>
    
    <CodeBox>{ExampleCode[0]}</CodeBox>

    <DemoBox>
      <LinkButton label="Default" />&nbsp;
      <LinkButton label="Default" small />&nbsp;
    </DemoBox>
    
    <CodeBox>{ExampleCode[1]}</CodeBox>

    <p>Além destes comandos básicos, outros parâmetros podem ser utilizados para dar mais versatilidade aos botões.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading head>Parâmetro</TableHeading>
          <TableHeading head>Tipo</TableHeading>
          <TableHeading head>Padrão</TableHeading>
          <TableHeading head>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>label</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>label</TableCell>
          <TableCell>Aplica um texto específico no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>small</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Reduz o tamanho do texto do botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>primary, success, warning, alert, hollow</TableCell>
          <TableCell>Aplica cor de fundo específica no botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>disabled</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Configura o botão como desativado</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>type</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>button, submit, reset</TableCell>
          <TableCell>Aplica o atributo "type" do botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>to</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Direciona o usuário para outra página interna</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>href</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Direciona o usuário para uma página externa</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default LinkButtonDocs;
