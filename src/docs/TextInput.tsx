import React, { useState } from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import {
  LocationIcon,
} from '../lib/IconLibrary';
import TextInput from '../lib/TextInput';

const ImportCode = `import TextInput from 'elementar-ui/dist/TextInput';`;

const ExampleCode = `import {
  MutePhoneIcon,
  LocationIcon,
} from 'elementar-ui/dist/IconLibrary';

(...)

<TextInput
  label="Nome"
  color="primary"
  size="xLarge"
  width="300px"
  value={name}
  onChange={({ target }) => handleName(target.value)}
  placeholder="Digite seu nome"
/>

<TextInput
  label="Senha"
  color="warning"
  type="password"
  value={password}
  onChange={({ target }) => handlePassword(target.value)}
  placeholder="Digite sua senha"
/>

<TextInput
  mobile
  label="Localização"
  color="success"
  value={location}
  onChange={({ target }) => handleLocation(target.value)}
  placeholder="Digite sua localização"
  icon={<LocationIcon />}
/>
`;

const TextInputDocs = () => {
  const [name, handleName] = useState('');
  const [password, handlePassword] = useState('');
  const [location, handleLocation] = useState('');

  return (
    <Template title="TextInput" id="text-input">
      <p>TextInput</p>

      <CodeBox>{ImportCode}</CodeBox>

      <p>Sobre TextInput</p>

      <DemoBox>
        <TextInput
          label="Nome"
          color="primary"
          size="xLarge"
          width="300px"
          value={name}
          onChange={({ target }) => handleName(target.value)}
          placeholder="Digite seu nome"
        />

        <TextInput
          label="Senha"
          color="warning"
          type="password"
          value={password}
          onChange={({ target }) => handlePassword(target.value)}
          placeholder="Digite sua senha"
        />

        <TextInput
          mobile
          label="Localização"
          color="success"
          value={location}
          onChange={({ target }) => handleLocation(target.value)}
          placeholder="Digite sua localização"
          icon={<LocationIcon />}
        />
      </DemoBox>

      <CodeBox>{ExampleCode}</CodeBox>

      <p>O suporte à propriedade <code>mask</code> foi retirado do Elementar-UI, cabendo a cada projeto em específico utilizar uma solução própria de mascaramento de texto. É recomendado o <a href="https://github.com/s-yadav/react-number-format">React Number Format</a> para implementações de campos com mascaramento.</p>
    </Template>
  );
};

export default TextInputDocs;
