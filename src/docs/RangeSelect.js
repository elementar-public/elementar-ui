import React, { useState } from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import RangeSelect from '../lib/RangeSelect';

const ImportCode = `import RangeSelect from 'elementar-ui/dist/RangeSelect';`;

const ExampleCode = `state = {
  level: 2,
  value: 76.39,
  range: 500
}

handleRange = e => this.setState({ [e.target.name]: e.target.value });

...

<RangeSelect value={level} min="1" max="5" step="1" onChange={e => handleLevel(e.target.value)} width="40%" label="Nível" />
<RangeSelect value={value} min="10" max="300" step="0.01" onChange={e => handleValue(e.target.value)} color="primary" label="Faixa de escolha" />
<RangeSelect value={range} min="50" max="5000" step="50" onChange={e => handleRange(e.target.value)} color="warning" label="Progresso" help="Apenas valores divisíveis por 50" />

<p>Nível: <strong>{level}</strong></p>
<p>Progresso: <strong>{value}</strong></p>
<p>Faixa de escolha: <strong>{range}</strong></p>
`;

const RangeSelectDocs = () => {
  const [level, handleLevel] = useState(2);
  const [value, handleValue] = useState(76.39);
  const [range, handleRange] = useState(500);

  return (
    <Template title="Range selector" id="range-selector">
      <p>O componente <strong>seletor de nível</strong> permite ao usuário escolher um valor dentro de um intervalo determinado com uma interface que lembra um ajuste deslizante.</p>
  
      <CodeBox>{ImportCode}</CodeBox>
  
      <p>Embora possa ser facilmente substituído por um <code>input</code> comum, o seletor de nível facilita para o usuário visualizar o universo de valores possíveis para o que ele deseja, com uma interface simples de manusear.</p>
  
      <DemoBox>
        <RangeSelect value={level} min="1" max="5" step="1" onChange={e => handleLevel(e.target.value)} width="40%" label="Nível" />
        <RangeSelect value={value} min="10" max="300" step="0.01" onChange={e => handleValue(e.target.value)} color="primary" label="Faixa de escolha" />
        <RangeSelect value={range} min="50" max="5000" step="50" onChange={e => handleRange(e.target.value)} color="warning" label="Progresso" help="Apenas valores divisíveis por 50" />

        <p>Nível: <strong>{level}</strong></p>
        <p>Progresso: <strong>{value}</strong></p>
        <p>Faixa de escolha: <strong>{range}</strong></p>
      </DemoBox>
  
      <CodeBox>{ExampleCode}</CodeBox>
  
      <p>O componente <code>RangeSelect</code> possui alguns parâmetros que permitem customizar a aparência dos indicadores.</p>
  
      <Table closed striped>
        <TableHead>
          <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>
  
        <TableBody>
          <TableRow>
            <TableCell bold>color</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>default</TableCell>
            <TableCell>Especifica a cor do nível indicador</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>label</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica um título para ser exibido acima da barra de seleção</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>containerClassname</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica o atributo <code>class</code> do elemento principal do componente</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>mobile</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica a visualização da barra indicadora adequada para interfaces mobile</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>noMargin</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica se haverá margem inferior no componente</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>width</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>100%</TableCell>
            <TableCell>Especifica a largura total da barra de seleção</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>help</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica um texto auxiliar para ser exibido abaixo da barra de seleção</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Template>
  );
};

export default RangeSelectDocs;
