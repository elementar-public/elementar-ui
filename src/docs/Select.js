import React, { useState } from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Select from '../lib/Select';

const importCode = `import { Select } from 'elementar-ui/dist/Select';`

const exampleCode = `state = {
  value: 'gel',
  options: [
    { value: 'gel', label: 'Geladeira' },
    { value: 'fog', label: 'Fogão' },
    'Microondas'
  ]
}

handleSelect = e => this.setState({ value: e.target.value });

...

<Select
  label="Selecione um eletrodoméstico"
  onChange={this.handleSelect}
  value={this.state.value}
  options={this.state.options}
/>
`;

const SelectDocs = () => {
  const [options, setOptions] = useState({
    value: 'gel',
    options: [
      { value: 'gel', label: 'Geladeira' },
      { value: 'fog', label: 'Fogão' },
      'Microondas'
    ]
  });
  
  const [cities, setCities] = useState({
    value: 'bsb',
    options: [
      { value: 'bsb', label: 'Brasília' },
      { value: 'rec', label: 'Recife' },
      { value: 'cgh', label: 'São Paulo' },
    ]
  });

  return (
    <Template title="Select" id="select">
      <p><strong>Selects</strong> são elementos de interação que permitem ao usuário escolher uma entre várias opções dentro de um formulário ou num espaço dedicado a exibir conteúdo de acordo com uma opção escolhida.</p>
  
      <CodeBox>{importCode}</CodeBox>
  
      <p>Selects recebem, por padrão, os itens disponíveis ao usuário por meio de um objeto com valores value e label. Enquanto o primeiro, opcional, define o conteúdo que será passado pelo formulário, o segundo define o conteúdo a ser exibido na tela.</p>
  
      <DemoBox>
        <Select
          color="primary"
          size="large"
          label="Selecione uma cidade"
          onChange={e => setCities({ value: e.target.value, options: cities.options })}
          value={cities.value}
          options={cities.options}
        />

        <p>{JSON.stringify(cities.value)}</p>

        <button type="button" onClick={() => setCities((e) => ({ value: 'rec', options: e.options }))}>Teste</button>

        <Select
          mobile
          label="Selecione um eletrodoméstico"
          onChange={e => setOptions({ value: e.target.value, options: options.options })}
          value={options.value}
          color="warning"
          width="400px"
          options={options.options}
        />
      </DemoBox>
  
      <CodeBox>{exampleCode}</CodeBox>
  
      <p>O componente <code>Select</code> possui parâmetros que permitem customizar o elemento de acordo com o devido propósito.</p>
  
      <Table closed striped>
        <TableHead>
          <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>
  
        <TableBody>
          <TableRow>
            <TableCell bold>className</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Controla o nome do atributo class do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>containerClassName</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Controla o nome do atributo class do elemento contenedor do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>label</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Controla o texto exibido no elemento <code>label</code>, logo acima do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>noMargin</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Desabilita a margem inferior que separa o seletor de opções do restante do conteúdo abaixo dele</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>onChange</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica as ações do seletor de opções quando uma nova opção é escolhida</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>value</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o atributo que controla a opção escolhida do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>name</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o valor do atributo <code>name</code> do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>id</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o valor do atributo <code>id</code> do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>inputRef</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Especifica o valor do atributo <code>ref</code> do elemento do seletor de opções</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>disabled</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Define a condição para que o componente esteja ou não habilitado para uso</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Template>
  );
};

export default SelectDocs;
