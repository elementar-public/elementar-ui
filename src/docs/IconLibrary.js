import React from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Tooltip from '../lib/Tooltip';
import BenefitIcon, {
  PhotoIcon,
  VideoIcon,
  AreaIcon,
  MapIcon,
  LocationIcon,
  BedroomIcon,
  BathroomIcon,
  GarageIcon,
  ConfirmIcon,
  WaterBoilerIcon,
  CoworkingSpaceIcon,
  BarbecueIcon,
  PoolIcon,
  SportsCourtIcon,
  CinemaRoomIcon,
  PlaygroundIcon,
  GymIcon,
  USBIcon,
  GourmetIcon,
  PartyIcon,
  GrillIcon,
  ClosedGarageIcon,
  AirConditionerIcon,
  PizzaIcon,
  GamesRoomIcon,
  GamerSpaceIcon,
  ToysRoomIcon,
  BicycleIcon,
  PipedGasIcon,
  SpaIcon,
  SolariumIcon,
  SteamRoomIcon,
  GeneratorIcon,
  LaundryIcon,
  BalconyIcon,
  UnitIcon,
  TransportationIcon,
  EducationIcon,
  HealthIcon,
  ServicesIcon,
  ChevronUpIcon,
  ChevronLeftIcon,
  ChevronDownIcon,
  ChevronRightIcon,
  ArrowUpIcon,
  ArrowDownIcon,
  ArrowLeftIcon,
  ArrowRightIcon,
  ArrowUpLeftIcon,
  ArrowUpRightIcon,
  ArrowDownLeftIcon,
  ArrowDownRightIcon,
  ThickArrowUpIcon,
  ThickArrowDownIcon,
  ThickArrowLeftIcon,
  ThickArrowRightIcon,
  ThickArrowUpLeftIcon,
  ThickArrowUpRightIcon,
  ThickArrowDownLeftIcon,
  ThickArrowDownRightIcon,
  EditIcon,
  ClipboardIcon,
  MoreIcon,
  LessIcon,
  ExpandIcon,
  CloseIcon,
  PhoneIcon,
  MutePhoneIcon,
  WhatsappIcon,
  RemoveIcon,
  CleanIcon,
  DeleteIcon,
  AddIcon,
  SearchIcon,
  HomeIcon,
  UserIcon,
  MoreItemsIcon,
  BuildingsIcon,
  PropertiesIcon,
  PropertyIcon,
  DevelopersIcon,
  DeveloperIcon,
  DocumentIcon,
  DocumentsIcon,
  LogoutIcon,
  MenuIcon,
  PersonIcon,
  AddPersonIcon,
  EditPanelIcon,
  CheckIcon,
  WarningIcon,
  DangerIcon,
  DownloadIcon,
  MenuTasksIcon,
  MenuDashboardIcon,
  MenuContactsIcon,
  MenuCampaignsIcon,
  MenuSalesIcon,
  MenuSalesAltIcon,
  MenuVisitsIcon,
  MenuUsersIcon,
  MenuPersonsIcon,
  MenuMarketingIcon,
  MenuChevronUpIcon,
  MenuChevronDownIcon,
  MenuChevronLeftIcon,
  MenuChevronRightIcon,
  MenuHelpIcon,
  MenuOpsIcon,
  HelpIcon,
  LinkIcon,
  MessageIcon,
  GlobeIcon,
  PlayIcon,
  PauseIcon,
  ReverseIcon,
  PlayPauseIcon,
  FastForwardIcon,
  FastReverseIcon,
  NextTrackIcon,
  PrevTrackIcon,
  StopIcon,
  EjectIcon,
  SunIcon,
  MoonIcon,
  SeenMessageIcon,
  ContactIcon,
  OptOutIcon,
  SpinnerIcon,
  LoadingIcon,
  MoneyIcon,
  BookmarkIcon,
  HeartIcon,
  StarIcon,
  ShareIcon,
  PetIcon,
  ElevatorIcon,
  WarmPoolIcon,
  WetDeckIcon,
  RoofIcon,
  MeetingRoomIcon,
  ExclusiveGarageIcon,
  SoundIcon,
  SoundUpIcon,
  SoundDownIcon,
  SoundMutedIcon,
  CalendarIcon,
  BeautyCareIcon,
  OutdoorFitnessIcon,
  BeachCourtIcon,
} from '../lib/IconLibrary';

const ImportCode = `import BenefitIcon, {#iconName#} from 'elementar-ui/dist/IconLibrary';`;

const ExampleCode = `...
<BenefitIcon benefit="toy_room" />
<BenefitIcon benefit="game_room" />
<BenefitIcon benefit="playground" />
<BenefitIcon benefit="pool" />
<BenefitIcon benefit="cinema" />
<BenefitIcon benefit="coworking" />
...`;

const IconLibraryDocs = () => (
  <Template title="Acervo de ícones" id="icon-library">
    <p>O Elementar-UI conta com um acervo de ícones prontos para uso, o <strong>Icon library</strong>. Os ícones em formato SVG são utilizados para compôr interfaces e ajudam a complementar menus, botões e outros elemntos de interação.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Os ícones do <code>IconLibrary</code> podem ter espessura de um ou dois pixels, de acordo com seu uso. Enquanto ícones destinados aos menus principais têm traços de dois pixels, os demais têm traços de um pixel de largura.</p>

    <DemoBox>
      <h3>Ícones de menus:</h3>
      <Tooltip message="MenuHelpIcon">
        <MenuHelpIcon/>
      </Tooltip>
      <Tooltip message="MenuTasksIcon">
        <MenuTasksIcon/>
      </Tooltip>
      <Tooltip message="MenuDashboardIcon">
        <MenuDashboardIcon/>
      </Tooltip>
      <Tooltip message="MenuOpsIcon">
        <MenuOpsIcon/>
      </Tooltip>
      <Tooltip message="MenuContactsIcon">
        <MenuContactsIcon/>
      </Tooltip>
      <Tooltip message="MenuCampaignsIcon">
        <MenuCampaignsIcon />
      </Tooltip>
      <Tooltip message="MenuSalesIcon">
        <MenuSalesIcon/>
      </Tooltip>
      <Tooltip message="MenuSalesAltIcon">
        <MenuSalesAltIcon/>
      </Tooltip>
      <Tooltip message="MenuVisitsIcon">
        <MenuVisitsIcon/>
      </Tooltip>
      <Tooltip message="MenuUsersIcon">
        <MenuUsersIcon/>
      </Tooltip>
      <Tooltip message="MenuPersonsIcon">
        <MenuPersonsIcon/>
      </Tooltip>
      <Tooltip message="MenuMarketingIcon">
        <MenuMarketingIcon/>
      </Tooltip>
      <Tooltip message="DocumentIcon">
        <DocumentIcon/>
      </Tooltip>
      <Tooltip message="PropertiesIcon">
        <PropertiesIcon/>
      </Tooltip>
      <Tooltip message="PropertyIcon">
        <PropertyIcon/>
      </Tooltip>
      <Tooltip message="DevelopersIcon">
        <DevelopersIcon/>
      </Tooltip>
      <Tooltip message="EditIcon">
        <EditIcon/>
      </Tooltip>
      <Tooltip message="AddIcon">
        <AddIcon/>
      </Tooltip>
      <Tooltip message="MoreItemsIcon">
        <MoreItemsIcon/>
      </Tooltip>
      <Tooltip message="LogoutIcon">
        <LogoutIcon/>
      </Tooltip>

      <h3>Ícones de status:</h3>
      <Tooltip message="CheckIcon">
        <CheckIcon/>
      </Tooltip>
      <Tooltip message="WarningIcon">
        <WarningIcon/>
      </Tooltip>
      <Tooltip message="DangerIcon">
        <DangerIcon/>
      </Tooltip>
      <Tooltip message="SpinnerIcon">
        <SpinnerIcon/>
      </Tooltip>
      <Tooltip message="LoadingIcon">
        <LoadingIcon/>
      </Tooltip>

      <h3>Ícones de indicação:</h3>
      <Tooltip message="ThickArrowUpIcon">
        <ThickArrowUpIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowDownIcon">
        <ThickArrowDownIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowLeftIcon">
        <ThickArrowLeftIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowRightIcon">
        <ThickArrowRightIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowUpLeftIcon">
        <ThickArrowUpLeftIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowUpRightIcon">
        <ThickArrowUpRightIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowDownRightIcon">
        <ThickArrowDownRightIcon/>
      </Tooltip>
      <Tooltip message="ThickArrowDownLeftIcon">
        <ThickArrowDownLeftIcon/>
      </Tooltip>
      <Tooltip message="MenuChevronUpIcon">
        <MenuChevronUpIcon/>
      </Tooltip>
      <Tooltip message="MenuChevronRightIcon">
        <MenuChevronRightIcon/>
      </Tooltip>
      <Tooltip message="MenuChevronDownIcon">
        <MenuChevronDownIcon/>
      </Tooltip>
      <Tooltip message="MenuChevronLeftIcon">
        <MenuChevronLeftIcon/>
      </Tooltip>
      <Tooltip message="ArrowUpIcon">
        <ArrowUpIcon/>
      </Tooltip>
      <Tooltip message="ArrowRightIcon">
        <ArrowRightIcon/>
      </Tooltip>
      <Tooltip message="ArrowDownIcon">
        <ArrowDownIcon/>
      </Tooltip>
      <Tooltip message="ArrowLeftIcon">
        <ArrowLeftIcon/>
      </Tooltip>
      <Tooltip message="ArrowUpLeftIcon">
        <ArrowUpLeftIcon/>
      </Tooltip>
      <Tooltip message="ArrowUpRightIcon">
        <ArrowUpRightIcon/>
      </Tooltip>
      <Tooltip message="ArrowDownLeftIcon">
        <ArrowDownLeftIcon/>
      </Tooltip>
      <Tooltip message="ArrowDownRightIcon">
        <ArrowDownRightIcon/>
      </Tooltip>
      <Tooltip message="ChevronUpIcon">
        <ChevronUpIcon/>
      </Tooltip>
      <Tooltip message="ChevronRightIcon">
        <ChevronRightIcon/>
      </Tooltip>
      <Tooltip message="ChevronDownIcon">
        <ChevronDownIcon/>
      </Tooltip>
      <Tooltip message="ChevronLeftIcon">
        <ChevronLeftIcon/>
      </Tooltip>

      <h3>Ícones de ações:</h3>
      <Tooltip message="ConfirmIcon">
        <ConfirmIcon/>
      </Tooltip>
      <Tooltip message="RemoveIcon">
        <RemoveIcon/>
      </Tooltip>
      <Tooltip message="MenuIcon">
        <MenuIcon/>
      </Tooltip>
      <Tooltip message="ExpandIcon">
        <ExpandIcon/>
      </Tooltip>
      <Tooltip message="CloseIcon">
        <CloseIcon/>
      </Tooltip>
      <Tooltip message="CleanIcon">
        <CleanIcon/>
      </Tooltip>
      <Tooltip message="DeleteIcon">
        <DeleteIcon/>
      </Tooltip>
      <Tooltip message="MoreIcon">
        <MoreIcon/>
      </Tooltip>
      <Tooltip message="LessIcon">
        <LessIcon/>
      </Tooltip>
      <Tooltip message="EditPanelIcon">
        <EditPanelIcon/>
      </Tooltip>
      <Tooltip message="DownloadIcon">
        <DownloadIcon/>
      </Tooltip>
      <Tooltip message="PlayIcon">
        <PlayIcon/>
      </Tooltip>
      <Tooltip message="ReverseIcon">
        <ReverseIcon/>
      </Tooltip>
      <Tooltip message="PauseIcon">
        <PauseIcon/>
      </Tooltip>
      <Tooltip message="PlayPauseIcon">
        <PlayPauseIcon/>
      </Tooltip>
      <Tooltip message="FastForwardIcon">
        <FastForwardIcon/>
      </Tooltip>
      <Tooltip message="FastReverseIcon">
        <FastReverseIcon/>
      </Tooltip>
      <Tooltip message="NextTrackIcon">
        <NextTrackIcon/>
      </Tooltip>
      <Tooltip message="PrevTrackIcon">
        <PrevTrackIcon/>
      </Tooltip>
      <Tooltip message="StopIcon">
        <StopIcon/>
      </Tooltip>
      <Tooltip message="EjectIcon">
        <EjectIcon/>
      </Tooltip>
      <Tooltip message="SoundIcon">
        <SoundIcon />
      </Tooltip>
      <Tooltip message="SoundUpIcon">
        <SoundUpIcon />
      </Tooltip>
      <Tooltip message="SoundDownIcon">
        <SoundDownIcon />
      </Tooltip>
      <Tooltip message="SoundMutedIcon">
        <SoundMutedIcon />
      </Tooltip>
      <Tooltip message="HelpIcon">
        <HelpIcon/>
      </Tooltip>
      <Tooltip message="ShareIcon">
        <ShareIcon/>
      </Tooltip>

      <h3>Ícones diversos:</h3>
      <Tooltip message="PhotoIcon">
        <PhotoIcon/>
      </Tooltip>
      <Tooltip message="VideoIcon">
        <VideoIcon/>
      </Tooltip>
      <Tooltip message="AreaIcon">
        <AreaIcon/>
      </Tooltip>
      <Tooltip message="MapIcon">
        <MapIcon/>
      </Tooltip>
      <Tooltip message="LocationIcon">
        <LocationIcon/>
      </Tooltip>
      <Tooltip message="BedroomIcon">
        <BedroomIcon/>
      </Tooltip>
      <Tooltip message="BathroomIcon">
        <BathroomIcon/>
      </Tooltip>
      <Tooltip message="GarageIcon">
        <GarageIcon/>
      </Tooltip>
      <Tooltip message="WaterBoilerIcon">
        <WaterBoilerIcon/>
      </Tooltip>
      <Tooltip message="CoworkingSpaceIcon">
        <CoworkingSpaceIcon/>
      </Tooltip>
      <Tooltip message="BarbecueIcon">
        <BarbecueIcon/>
      </Tooltip>
      <Tooltip message="PoolIcon">
        <PoolIcon/>
      </Tooltip>
      <Tooltip message="SportsCourtIcon">
        <SportsCourtIcon/>
      </Tooltip>
      <Tooltip message="CinemaRoomIcon">
        <CinemaRoomIcon/>
      </Tooltip>
      <Tooltip message="PlaygroundIcon">
        <PlaygroundIcon/>
      </Tooltip>
      <Tooltip message="GymIcon">
        <GymIcon/>
      </Tooltip>
      <Tooltip message="USBIcon">
        <USBIcon/>
      </Tooltip>
      <Tooltip message="GourmetIcon">
        <GourmetIcon/>
      </Tooltip>
      <Tooltip message="PartyIcon">
        <PartyIcon/>
      </Tooltip>
      <Tooltip message="GrillIcon">
        <GrillIcon/>
      </Tooltip>
      <Tooltip message="ClosedGarageIcon">
        <ClosedGarageIcon/>
      </Tooltip>
      <Tooltip message="AirConditionerIcon">
        <AirConditionerIcon/>
      </Tooltip>
      <Tooltip message="PizzaIcon">
        <PizzaIcon/>
      </Tooltip>
      <Tooltip message="GamesRoomIcon">
        <GamesRoomIcon/>
      </Tooltip>
      <Tooltip message="GamerSpaceIcon">
        <GamerSpaceIcon/>
      </Tooltip>
      <Tooltip message="ToysRoomIcon">
        <ToysRoomIcon/>
      </Tooltip>
      <Tooltip message="BicycleIcon">
        <BicycleIcon/>
      </Tooltip>
      <Tooltip message="PipedGasIcon">
        <PipedGasIcon/>
      </Tooltip>
      <Tooltip message="SpaIcon">
        <SpaIcon/>
      </Tooltip>
      <Tooltip message="SolariumIcon">
        <SolariumIcon/>
      </Tooltip>
      <Tooltip message="SteamRoomIcon">
        <SteamRoomIcon/>
      </Tooltip>
      <Tooltip message="GeneratorIcon">
        <GeneratorIcon/>
      </Tooltip>
      <Tooltip message="LaundryIcon">
        <LaundryIcon/>
      </Tooltip>
      <Tooltip message="BalconyIcon">
        <BalconyIcon/>
      </Tooltip>
      <Tooltip message="UnitIcon">
        <UnitIcon/>
      </Tooltip>
      <Tooltip message="TransportationIcon">
        <TransportationIcon/>
      </Tooltip>
      <Tooltip message="EducationIcon">
        <EducationIcon/>
      </Tooltip>
      <Tooltip message="HealthIcon">
        <HealthIcon/>
      </Tooltip>
      <Tooltip message="ServicesIcon">
        <ServicesIcon/>
      </Tooltip>
      <Tooltip message="PhoneIcon">
        <PhoneIcon/>
      </Tooltip>
      <Tooltip message="MutePhoneIcon">
        <MutePhoneIcon/>
      </Tooltip>
      <Tooltip message="WhatsappIcon">
        <WhatsappIcon/>
      </Tooltip>
      <Tooltip message="SearchIcon">
        <SearchIcon/>
      </Tooltip>
      <Tooltip message="HomeIcon">
        <HomeIcon/>
      </Tooltip>
      <Tooltip message="UserIcon">
        <UserIcon/>
      </Tooltip>
      <Tooltip message="BuildingsIcon">
        <BuildingsIcon/>
      </Tooltip>
      <Tooltip message="DeveloperIcon">
        <DeveloperIcon/>
      </Tooltip>
      <Tooltip message="DocumentsIcon">
        <DocumentsIcon/>
      </Tooltip>
      <Tooltip message="PersonIcon">
        <PersonIcon/>
      </Tooltip>
      <Tooltip message="AddPersonIcon">
        <AddPersonIcon/>
      </Tooltip>
      <Tooltip message="ClipboardIcon">
        <ClipboardIcon/>
      </Tooltip>
      <Tooltip message="LinkIcon">
        <LinkIcon/>
      </Tooltip>
      <Tooltip message="MessageIcon">
        <MessageIcon/>
      </Tooltip>
      <Tooltip message="GlobeIcon">
        <GlobeIcon/>
      </Tooltip>
      <Tooltip message="SunIcon">
        <SunIcon/>
      </Tooltip>
      <Tooltip message="MoonIcon">
        <MoonIcon/>
      </Tooltip>
      <Tooltip message="SeenMessageIcon">
        <SeenMessageIcon/>
      </Tooltip>
      <Tooltip message="ContactIcon">
        <ContactIcon/>
      </Tooltip>
      <Tooltip message="OptOutIcon">
        <OptOutIcon/>
      </Tooltip>
      <Tooltip message="MoneyIcon">
        <MoneyIcon/>
      </Tooltip>
      <Tooltip message="BookmarkIcon">
        <BookmarkIcon/>
      </Tooltip>
      <Tooltip message="HeartIcon">
        <HeartIcon/>
      </Tooltip>
      <Tooltip message="StarIcon">
        <StarIcon/>
      </Tooltip>
      <Tooltip message="PetIcon">
        <PetIcon/>
      </Tooltip>
      <Tooltip message="ElevatorIcon">
        <ElevatorIcon />
      </Tooltip>
      <Tooltip message="WarmPoolIcon">
        <WarmPoolIcon />
      </Tooltip>
      <Tooltip message="WetDeckIcon">
        <WetDeckIcon />
      </Tooltip>
      <Tooltip message="RoofIcon">
        <RoofIcon />
      </Tooltip>
      <Tooltip message="MeetingRoomIcon">
        <MeetingRoomIcon />
      </Tooltip>
      <Tooltip message="ExclusiveGarageIcon">
        <ExclusiveGarageIcon />
      </Tooltip>
      <Tooltip message="CalendarIcon">
        <CalendarIcon />
      </Tooltip>
      <Tooltip message="BeautyCareIcon">
        <BeautyCareIcon />
      </Tooltip>
      <Tooltip message="OutdoorFitnessIcon">
        <OutdoorFitnessIcon />
      </Tooltip>
      <Tooltip message="BeachCourtIcon">
        <BeachCourtIcon />
      </Tooltip>
    </DemoBox>

    <p>Alguns ícones podem ser chamados com o componente <code>BenefitIcon</code>, bastando especificar o tipo de ícone desejado.</p>

    <DemoBox>
      <Tooltip message="barbecue">
        <BenefitIcon benefit="barbecue" />
      </Tooltip>
      <Tooltip message="party_space">
        <BenefitIcon benefit="party_space" />
      </Tooltip>
      <Tooltip message="sports">
        <BenefitIcon benefit="sports" />
      </Tooltip>
      <Tooltip message="pizza_oven">
        <BenefitIcon benefit="pizza_oven" />
      </Tooltip>
      <Tooltip message="play_room">
        <BenefitIcon benefit="play_room" />
      </Tooltip>
      <Tooltip message="toy_room">
        <BenefitIcon benefit="toy_room" />
      </Tooltip>
      <Tooltip message="game_room">
        <BenefitIcon benefit="game_room" />
      </Tooltip>
      <Tooltip message="playground">
        <BenefitIcon benefit="playground" />
      </Tooltip>
      <Tooltip message="pool">
        <BenefitIcon benefit="pool" />
      </Tooltip>
      <Tooltip message="cinema">
        <BenefitIcon benefit="cinema" />
      </Tooltip>
      <Tooltip message="coworking">
        <BenefitIcon benefit="coworking" />
      </Tooltip>
      <Tooltip message="gourmet_space">
        <BenefitIcon benefit="gourmet_space" />
      </Tooltip>
      <Tooltip message="gym">
        <BenefitIcon benefit="gym" />
      </Tooltip>
      <Tooltip message="covered_parking_spot">
        <BenefitIcon benefit="covered_parking_spot" />
      </Tooltip>
      <Tooltip message="usb_spots">
        <BenefitIcon benefit="usb_spots" />
      </Tooltip>
      <Tooltip message="grill_space">
        <BenefitIcon benefit="grill_space" />
      </Tooltip>
      <Tooltip message="air_conditioning">
        <BenefitIcon benefit="air_conditioning" />
      </Tooltip>
      <Tooltip message="hot_water">
        <BenefitIcon benefit="hot_water" />
      </Tooltip>
      <Tooltip message="bike_spot">
        <BenefitIcon benefit="bike_spot" />
      </Tooltip>
      <Tooltip message="gas">
        <BenefitIcon benefit="gas" />
      </Tooltip>
      <Tooltip message="spa">
        <BenefitIcon benefit="spa" />
      </Tooltip>
      <Tooltip message="solarium">
        <BenefitIcon benefit="solarium" />
      </Tooltip>
      <Tooltip message="steam_room">
        <BenefitIcon benefit="steam_room" />
      </Tooltip>
      <Tooltip message="energy_bank">
        <BenefitIcon benefit="energy_bank" />
      </Tooltip>
      <Tooltip message="laundry_room">
        <BenefitIcon benefit="laundry_room" />
      </Tooltip>
      <Tooltip message="gourmet_balcony">
        <BenefitIcon benefit="gourmet_balcony" />
      </Tooltip>
      <Tooltip message="espaco_pet">
        <BenefitIcon benefit="espaco_pet" />
      </Tooltip>
      <Tooltip message="elevador_regenerativo">
        <BenefitIcon benefit="elevador_regenerativo" />
      </Tooltip>
      <Tooltip message="piscina_aquecida">
        <BenefitIcon benefit="piscina_aquecida" />
      </Tooltip>
      <Tooltip message="deck_molhado">
        <BenefitIcon benefit="deck_molhado" />
      </Tooltip>
      <Tooltip message="lazer_cobertura">
        <BenefitIcon benefit="lazer_cobertura" />
      </Tooltip>
      <Tooltip message="sala_reuniao">
        <BenefitIcon benefit="sala_reuniao" />
      </Tooltip>
      <Tooltip message="vagas_determinadas">
        <BenefitIcon benefit="vagas_determinadas" />
      </Tooltip>
      <Tooltip message="beauty_care">
        <BenefitIcon benefit="beauty_care" />
      </Tooltip>
      <Tooltip message="fitness_externo">
        <BenefitIcon benefit="fitness_externo" />
      </Tooltip>
      <Tooltip message="quadra_de_areia">
        <BenefitIcon benefit="quadra_de_areia" />
      </Tooltip>
    </DemoBox>

    <CodeBox>{ExampleCode}</CodeBox>

    <p>Em cada ícone, você pode utilizar outros parâmetros para configurar um elemento do <code>IconLibrary</code>.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>className</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>text</TableCell>
          <TableCell>Aplica uma classe específica para o ícone</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica uma cor específica para o ícone</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>grid</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Mostra um grid auxiliar para o posicionamento dos elementos do ícone</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default IconLibraryDocs;
