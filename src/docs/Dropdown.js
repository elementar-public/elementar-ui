import React from 'react';
import Template, { CodeBox, DemoBox } from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Dropdown, { DropdownItem } from '../lib/Dropdown';
import Button from '../lib/Button';
import ArrowIcon from '../lib/Icons/Arrow';

const ImportCode = `import Dropdown, {DropdownItem} from 'elementar-ui/dist/Dropdown';`;

const ExampleCode = `
<Dropdown trigger={<Button color="primary" label="Ações disponíveis" rightIcon={ArrowIcon} />}>
  <DropdownItem label="Abrir mensagens" action={...} />
  <DropdownItem label="Abrir lembretes" action={...} />
  <DropdownItem label="Ajustar configurações" action={...} />
  <DropdownItem separator />
  <DropdownItem label="Ir para outro site" href="..." />
</Dropdown>

<Dropdown up position="right" dark trigger={<Button color="success" outline label="Ações disponíveis" rightIcon={ArrowIcon} />}>
  <DropdownItem label="Abrir mensagens" action={...} />
  <DropdownItem label="Abrir lembretes" action={...} />
  <DropdownItem label="Ajustar configurações adicionais do dispositivo" action={...} />
</Dropdown>
`;

const DropdownDocs = () => (
  <Template title="Menus dropdown" id="dropdown">
    <p>Os <strong>Menus Dropdown</strong> são menus que permitem acomodar uma lista de ações, que é aberto apenas quando o usuário clica num elemento de gatilho.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <code>Dropdown</code> possui parâmetros que permitem customizar aspectos visuais como cor de fundo (claro como padrão, escuro como opção), posição e alinhamento. Ele demanda um elemento inicial que funciona como gatilho para o menu ser aberto.</p>

    <DemoBox>
      <Dropdown trigger={<Button color="primary" label="Ações disponíveis" rightIcon={ArrowIcon} />}>
        <DropdownItem label="Abrir mensagens" action={() => window.alert('Ação de mensagens')} />
        <DropdownItem label="Abrir lembretes" action={() => window.alert('Ação de lembretes')} />
        <DropdownItem label="Ajustar configurações" action={() => window.alert('Ação de configurações')} />
        <DropdownItem separator />
        <DropdownItem label="Ir para outro site" href="https://google.com" />
      </Dropdown>
      <hr/>
      <Dropdown up position="right" dark trigger={<Button color="success" outline label="Ações disponíveis" rightIcon={ArrowIcon} />}>
        <DropdownItem label="Abrir mensagens" action={() => window.alert('Ação de mensagens')} />
        <DropdownItem label="Abrir lembretes" action={() => window.alert('Ação de lembretes')} />
        <DropdownItem label="Ajustar configurações adicionais do dispositivo" action={() => window.alert('Ação de configurações')} />
      </Dropdown>
    </DemoBox>
    
    <CodeBox>{ExampleCode}</CodeBox>

    <p>Alguns parâmetros permitem customizar detalhes do componente <code>Dropdown</code></p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>trigger</TableCell>
          <TableCell italic>object</TableCell>
          <TableCell italic>{`<button />`}</TableCell>
          <TableCell>Especifica um elemento HTML padrão como gatilho para exibir o menu dropdown</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>position</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>left, right, center</TableCell>
          <TableCell>Especifica o alinhamento do menu dropdown em relação ao botão</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>up</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Faz com que o menu dropdown seja exibido acima do elemento de gatilho</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>dark</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Aplica o tema escuro para o menu dropdown</TableCell>
        </TableRow>
      </TableBody>
    </Table>

    <p>Existe também o componente <code>DropdownItem</code>, onde são descritos a mensagem da ação e a função que ela executará ao ser clicado.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>label</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>text</TableCell>
          <TableCell>Aplica um texto específico no item do menu dropdown</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>disabled</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Especifica se o item do menu estará disponível para uso</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>separator</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Faz com que o item de menu seja exibido como uma borda separadora</TableCell>
        </TableRow>
          <TableRow>
            <TableCell bold>action</TableCell>
            <TableCell italic>function</TableCell>
            <TableCell italic>null</TableCell>
            <TableCell>Controla a ação do item do menu</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>to</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica um link interno de destino para o item do menu</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>href</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>false</TableCell>
            <TableCell>Especifica um link externo de destino para o item do menu</TableCell>
          </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default DropdownDocs;
