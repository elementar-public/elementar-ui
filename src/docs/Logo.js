import React from 'react';
import styled from 'styled-components';
import Template from '../components/template';

const LogoGrid = styled.ul`
  display: grid;
  grid-template: auto / repeat(auto-fit, minmax(250px, 1fr));
  grid-gap: 20px;
  margin: 40px 0 100px;
  padding: 0;
  list-style: none;

  > li {
    border-radius: 10px;
    overflow: hidden;
    box-shadow: 0 4px 8px rgba(0,0,0,0.05), inset 0 0 0 1px rgba(0,0,0,0.1);
  }
`;

const LogoChip = styled.div`
  margin: 0;
  padding: 10px;
  background: ${({negative}) => negative ? 'black' : 'none'};

  
  > img {
    display: block;
    width: 100%;
    height: 100%;
  }
`;

const LogoDefinition = styled.dl`
  padding: 10px 20px 20px;
  line-height: 1.3rem;

  dt {
    font-size: 0.75rem;
    text-transform: uppercase;
    letter-spacing: 2px;
    margin: 10px 0 0;
    padding: 0;
    color: #89A;
  }

  dd {
    margin: 0;
    padding: 0;
  }
`;

const LogoDocs = () => (
  <Template title="Marca" id="logo">
    <p>A marca da Elementar Digital é o ponto de contato mais reconhecível do público com nosso time e o que fazemos. Seus traços simples e geométricos representam uma visão sobre como a tecnologia e a criatividade podem ajudar negócios a atingirem seus melhores resultados.</p>

    <p>Os arquivos da marca estão disponíveis em três formatos: <strong>PNG</strong> (bitmap), para uso em apresentações, avatares e imagens diversas; <strong>PDF</strong> (vetor), para referência e importação em materiais impressos; e <strong>SVG</strong> (vetor), para uso em interfaces para navegadores web modernos.</p>
    
    <p>A assinatura principal da marca conta com uma disposição simples de símbolo e logotipo, com ambos alinhados à esquerda e devidamente espaçados. O uso dessa assinatura <strong>é recomendada com prioridade</strong> para documentos, interfaces e toda sorte de material gráfico produzido para a empresa.</p>

    <LogoGrid>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logo-main.png" alt="Assinatura principal em preto" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Assinatura principal</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada em quaisquer fundos claros</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.png" download>Assinatura principal – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.svg" download>Assinatura principal – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.pdf" download>Assinatura principal – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
      <li>
        <LogoChip negative><img src="/elementar-ui/assets/logo-main-negative.png" alt="Assinatura principal em negativo" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Assinatura principal para fundo escuro</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada em fundos coloridos e escuros</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.png" download>Assinatura em negativo – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.svg" download>Assinatura em negativo – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.pdf" download>Assinatura em negativo – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logo-main-blue.png" alt="Assinatura principal em azul" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Assinatura principal em Azul</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada apenas em fundo branco</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.png" download>Assinatura em azul – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.svg" download>Assinatura em azul – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Principal-Positivo.pdf" download>Assinatura em azul – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
    </LogoGrid>
    
    <p>A assinatura secundária conta com uma disposição convencional de símbolo e logotipo lado a lado. Essa assinatura é recomendada apenas para casos em que a aplicação da assinatura principal <strong>prejudique a legibilidade da marca</strong>.</p>

    <LogoGrid>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logo-alt.png" alt="Assinatura secundária em preto" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Assinatura secundária</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada em quaisquer fundos claros</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.png" download>Assinatura alternativa – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.svg" download>Assinatura alternativa – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.pdf" download>Assinatura alternativa – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
      <li>
        <LogoChip negative><img src="/elementar-ui/assets/logo-alt-negative.png" alt="Assinatura secundária em negativo" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Assinatura secundária para fundo escuro</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada em quaisquer em fundos coloridos e escuros</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.png" download>Assinatura em negativo – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.svg" download>Assinatura em negativo – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.pdf" download>Assinatura em negativo – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logo-alt-blue.png" alt="Assinatura secundária em azul" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Assinatura secundária em Azul</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada apenas em fundo branco</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.png" download>Assinatura em azul – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.svg" download>Assinatura em azul – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.pdf" download>Assinatura em azul – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
    </LogoGrid>
    
    <p>O símbolo da marca, com as letras "E" e "D" ligadas por um traço superior, é a menor unidade possível de representação gráfica da marca. Deve ser utilizada <strong>apenas em peças institucionais</strong>, que já contem com outros elementos de identidade visual da marca.</p>

    <LogoGrid>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logo-symbol.png" alt="Símbolo em preto" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Símbolo</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada em quaisquer fundos claros</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.png" download>Símbolo – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.svg" download>Símbolo – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.pdf" download>Símbolo – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
      <li>
        <LogoChip negative><img src="/elementar-ui/assets/logo-symbol-negative.png" alt="Símbolo em negativo" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Símbolo para fundo escuro</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada em fundos coloridos e escuros</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.png" download>Símbolo em negativo – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.svg" download>Símbolo em negativo – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.pdf" download>Símbolo em negativo – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logo-symbol-blue.png" alt="Símbolo em azul" /></LogoChip>
        <LogoDefinition>
          <dt>Versão</dt>
          <dd>Símbolo em Azul</dd>
          <dt>Uso</dt>
          <dd>Pode ser aplicada apenas em fundo branco</dd>
          <dt>Links</dt>
          <dd>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.png" download>Símbolo em azul – PNG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.svg" download>Símbolo em azul – SVG</a><br/>
            <a href="/bucket/Elementar-Assinatura-Secundaria-Positivo.pdf" download>Símbolo em azul – PDF</a>
          </dd>
        </LogoDefinition>
      </li>
    </LogoGrid>

    <p>O logotipo da marca da Elementar Digital parte de uma malha construtiva retangular, e utiliza traços simples e geométricos para dar forma às letras customizadas para a marca. A largura dos traços do logotipo são usados como referência para o alinhamento com o símbolo. O uso isolado do logotipo <strong>não é recomendado</strong> em nenhuma circunstância, assim como seu uso com o símbolo em disposições diferentes das recomendadas nas assinaturas principal e secundária.</p>

    <LogoGrid>
      <li>
        <LogoChip><img src="/elementar-ui/assets/logotype.png" alt="Malha construtiva do logotipo" /></LogoChip>
      </li>
    </LogoGrid>
  </Template>
);

export default LogoDocs;