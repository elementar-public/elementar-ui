import React, {useState} from 'react';
import Template, {CodeBox, DemoBox, TableBox} from '../components/template';
import Table, {TableHead, TableBody, TableRow, TableHeading, TableCell} from '../lib/Table';
import Floater from '../lib/Floater';
import Button from '../lib/Button';
import LinkButton from '../lib/LinkButton';

const ImportCode = `import Floater from 'elementar-ui/dist/Floater';`;

const ExampleCode = `state = {
  floater: false
}

toggleFloater = () => this.setState({ floater: !this.state.floater });

...

<Floater active={floater} close={() => toggleDialog(false)}>
  <p>Caixinha</p>
  <Button label="Fechar" onClick={() => toggleDialog(false)} />
</Floater>
`;

const FloaterDocs = () => {
  const [floater, toggleDialog] = useState(false);

  return (
    <Template title="Floater" id="floater">
      <p><strong>Floaters</strong> são caixas flutuantes que acomodam conteúdo sem outros elementos distrativos e acessórios, como botões de fechar e títulos. Ao contrário de modais e caixas de diálogo, o componente <code>Floater</code> não contém em si o mecanismo de fechamento, devendo ser providenciado dentro do conteúdo.</p>
  
      <CodeBox>{ImportCode}</CodeBox>

      <p>Com o <code>Floater</code>, é possível exibir conteúdo intermitente com maior liberdade de composição dentro da caixa flutuante.</p>
  
      <DemoBox>
        <Button label="Ativar floater" color="primary" rounded onClick={() => toggleDialog(!floater)} />

        <Floater
          active={floater}
          cover
          close={() => toggleDialog(false)}
        >
          <p>O Antonov An-225 Mriya é uma aeronave de transporte cargueiro estratégico, a qual foi produzida durante a década de 1980 pela Antonov Design Bureau.</p>
          <LinkButton label="Fechar" onClick={() => toggleDialog(false)} />
        </Floater>
      </DemoBox>
      
      <CodeBox>{ExampleCode}</CodeBox>
  
      <p>Você pode utilizar estes parâmetros para configurar o componente <code>Floater</code>:</p>
      
      <TableBox>
        <Table closed striped>
          <TableHead>
            <TableRow>
              <TableHeading>Parâmetro</TableHeading>
              <TableHeading>Tipo</TableHeading>
              <TableHeading>Padrão</TableHeading>
              <TableHeading>Efeito</TableHeading>
            </TableRow>
          </TableHead>
    
          <TableBody>
            <TableRow>
              <TableCell bold>active</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Controla a exibição da caixa de diálogo</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>spacing</TableCell>
              <TableCell italic>integer</TableCell>
              <TableCell italic>10</TableCell>
              <TableCell>Especifica o espaçamento interno da caixa principal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>width</TableCell>
              <TableCell italic>integer</TableCell>
              <TableCell italic>250px</TableCell>
              <TableCell>Especifica a largura total da caixa principal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>height</TableCell>
              <TableCell italic>integer</TableCell>
              <TableCell italic>400px</TableCell>
              <TableCell>Especifica a altura máxima da caixa principal</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>minHeight</TableCell>
              <TableCell italic>integer</TableCell>
              <TableCell italic>200px</TableCell>
              <TableCell>Especifica a altura mínima da caixa principal</TableCell>
            </TableRow>
            <TableRow>
            <TableCell bold>cover</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Aplica largura e altura máxima à caixa flutuante quando está em uma tela de dispositivo móvel</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableBox>
    </Template>
  )
}

export default FloaterDocs;