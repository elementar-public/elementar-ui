import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Box from '../lib/Box';

const ImportCode = `import Box from 'elementar-ui/dist/Box';`;

const ExampleCode = [
`<Box elevation="2" spacing="20">
  <p>Esta caixa que você vê ao redor deste texto é um Box.</p>
        
  <ul>
    <li>Ele acomoda desde simples parágrafos</li>
    <li>Até conteúdos maiores como listas</li>
  </ul>
</Box>
`,
`removeBox = () => alert('Uma ação especial de fechar');

...

<Box alert message="Uma caixa de avisos" />
<Box alert="primary" message="Uma caixa de avisos comuns" />
<Box alert="success" message="Uma caixa de avisos positivos" />
<Box alert="warning" message="Uma caixa de avisos importantes para o usuário..." close />
<Box alert="danger" message="Uma caixa de avisos críticos..." close={this.removeBox} />
`];

const BoxDocs = () => {
  const removeBox = () => alert('Uma ação especial de fechar');

  return (
    <Template title="Boxes" id="box">
      <p>O <strong>box</strong> é uma caixa disposta na tela com o fim de acomodar conteúdos em espaços delimitados. O componente box permite aplicar rapidamente formatação a um espaço que precise destacar-se dos demais elementos na tela.</p>
  
      <CodeBox>{ImportCode}</CodeBox>
  
      <DemoBox>
        <p>Esta caixa que você vê ao redor deste texto é um Box.</p>
        
        <ul>
          <li>Ele acomoda desde simples parágrafos</li>
          <li>Até conteúdos maiores como listas</li>
        </ul>
      </DemoBox>
      
      <CodeBox>{ExampleCode[0]}</CodeBox>
  
      <p>Um box pode ter aparência simples, de fundo branco, mas também dispõe de parâmetros que transformam o box numa caixa de alerta para avisos gerais da interface.</p>
  
      <DemoBox>
        <Box alert message="Uma caixa de avisos" />
        <Box alert="primary" message="Uma caixa de avisos comuns" />
        <Box alert="success" message="Uma caixa de avisos positivos" />
        <Box alert="warning" message="Uma caixa de avisos importantes para o usuário; você pode fecha-la, e ela não aparecerá mais" close />
        <Box alert="danger" message="Uma caixa de avisos críticos; você pode fecha-la com uma função especial" close={removeBox} />
      </DemoBox>
  
      <CodeBox>{ExampleCode[1]}</CodeBox>
  
      <p>Além destes comandos, você pode utilizar outros parâmetros para configurar um elemento <code>Box</code>.</p>
  
      <Table closed striped>
        <TableHead>
          <TableRow>
            <TableHeading>Parâmetro</TableHeading>
            <TableHeading>Tipo</TableHeading>
            <TableHeading>Padrão</TableHeading>
            <TableHeading>Efeito</TableHeading>
          </TableRow>
        </TableHead>
  
        <TableBody>
          <TableRow>
            <TableCell bold>spacing</TableCell>
            <TableCell italic>integer</TableCell>
            <TableCell italic>5</TableCell>
            <TableCell>Aplica espaçamento interno ao box de conteúdo</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>elevation</TableCell>
            <TableCell italic>integer</TableCell>
            <TableCell italic>2</TableCell>
            <TableCell>Regula o efeito de profundidade ao box de conteúdo</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>overflowHidden</TableCell>
            <TableCell italic>bool</TableCell>
            <TableCell italic>disabled</TableCell>
            <TableCell>Esconde conteúdo que excede os limites do box</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>headingElement</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>h2</TableCell>
            <TableCell>Especifica o elemento no DOM que será usado para o título do componente</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>alert</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>default, primary, success, warning, alert</TableCell>
            <TableCell>Transforma o box em uma caixa de alerta com formatação específica</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>close</TableCell>
            <TableCell italic>bool, function</TableCell>
            <TableCell italic>removeBox</TableCell>
            <TableCell>Exibe botão que permite remover a caixa de alerta da tela</TableCell>
          </TableRow>
          <TableRow>
            <TableCell bold>message</TableCell>
            <TableCell italic>string</TableCell>
            <TableCell italic>Mensagem</TableCell>
            <TableCell>Aplica um texto específico à caixa de alerta</TableCell>
          </TableRow>
        </TableBody>
      </Table>
    </Template>
  );
};

export default BoxDocs;
