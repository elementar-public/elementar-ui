import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Accordion from '../lib/Accordion';

const ImportCode = `import Accordion from 'elementar-ui/dist/Accordion';`;

const ExampleCode = `<Accordion headingElement="h3" title="Estratégias em um novo paradigma globalizado" open={true}>
  <p>Não obstante, o fenômeno da Internet [...] setorial.</p>
</Accordion>

<Accordion headingElement="h3" title="O entendimento das metas propostas">
  <p>Ainda assim, existem dúvidas a respeito [...] fazemos parte.</p>
</Accordion>

<Accordion headingElement="h3" title="Definição de objetivos a nível organizacional">
  <p>Pensando mais a longo prazo, [...] excelência.</p>
  <p>O cuidado em identificar pontos críticos [...] adotados.</p>
</Accordion>
`;

const AccordionDocs = () => (
  <Template title="Accordion" id="accordion">
    <p>O componente <strong>Accordion</strong> permite adicionar conteúdo a uma página que não necessite estar imediatamente visível e possa ser formatado com títulos e textos curtos. Conforme o usuário deseja, ele pode clicar no título do item e revelar seu texto.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>Um <code>Accordion</code> pode ser formatado para utilizar diferentes níveis hierárquicos de títulos, além de aceitar parâmetros que deixam o texto interno aparente como estado inicial.</p>

    <DemoBox>
      <Accordion headingElement="h3" title="Estratégias em um novo paradigma globalizado" open={true}>
        <p>Não obstante, o fenômeno da Internet nos obriga à análise dos relacionamentos verticais entre as hierarquias. Por outro lado, o desafiador cenário globalizado é uma das consequências dos paradigmas corporativos. Assim mesmo, a contínua expansão de nossa atividade aponta para a melhoria das diretrizes de desenvolvimento para o futuro. No entanto, não podemos esquecer que a determinação clara de objetivos auxilia a preparação e a composição do orçamento setorial.</p>
      </Accordion>

      <Accordion headingElement="h3" title="O entendimento das metas propostas">
        <p>Ainda assim, existem dúvidas a respeito de como o consenso sobre a necessidade de qualificação garante a contribuição de um grupo importante na determinação do remanejamento dos quadros funcionais. A prática cotidiana prova que a adoção de políticas descentralizadoras estende o alcance e a importância dos níveis de motivação departamental. Neste sentido, o comprometimento entre as equipes cumpre um papel essencial na formulação dos procedimentos normalmente adotados. As experiências acumuladas demonstram que a consulta aos diversos militantes ainda não demonstrou convincentemente que vai participar na mudança da gestão inovadora da qual fazemos parte.</p>
      </Accordion>

      <Accordion headingElement="h3" title="Definição de objetivos a nível organizacional">
        <p>Pensando mais a longo prazo, a consolidação das estruturas oferece uma interessante oportunidade para verificação das posturas dos órgãos dirigentes com relação às suas atribuições. Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se a valorização de fatores subjetivos pode nos levar a considerar a reestruturação das formas de ação. O incentivo ao avanço tecnológico, assim como a mobilidade dos capitais internacionais agrega valor ao estabelecimento dos conhecimentos estratégicos para atingir a excelência.</p>
        <p>O cuidado em identificar pontos críticos na execução dos pontos do programa afeta positivamente a correta previsão de todos os recursos funcionais envolvidos. Todas estas questões, devidamente ponderadas, levantam dúvidas sobre se o julgamento imparcial das eventualidades deve passar por modificações independentemente do remanejamento dos quadros funcionais. Caros amigos, o comprometimento entre as equipes assume importantes posições no estabelecimento dos procedimentos normalmente adotados.</p>
      </Accordion>
    </DemoBox>
    <CodeBox>{ExampleCode}</CodeBox>

    <p>O componente <code>AccordionItem</code> possui alguns parâmetros que permitem customizar a aparência do item.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
        <TableHeading>Parâmetro</TableHeading>
        <TableHeading>Tipo</TableHeading>
        <TableHeading>Padrão</TableHeading>
        <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>

      <TableBody>
        <TableRow>
          <TableCell bold>title</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>Título</TableCell>
          <TableCell>Especifica o texto do título do componente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>element</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>div</TableCell>
          <TableCell>Especifica o elemento no DOM que será usado para o componente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>headingElement</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>h2</TableCell>
          <TableCell>Especifica o elemento no DOM que será usado para o título do componente</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>open</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Controla a abertura do componente ao monta-lo</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default AccordionDocs;
