import React from 'react';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Stepper from '../lib/Stepper';

const ImportCode = `import Label from 'elementar-ui/dist/Label';`;

const ExampleCode = `<Stepper
  step={2}
  steps={[
    { name: 'Dados pessoais' },
    { name: 'Endereço de entrega', warning: 'Seu endereço oferece opções de entrega mais rápidos' },
    { name: 'Dados de pagamento' },
    { name: 'Checkout' }
  ]}
/>`;

const StepperDocs = () => (
  <Template title="Stepper" id="stepper">
    <p><strong>Steppers</strong> são trechos de texto formatados com fundo colorido, que lembram pequenas etiquetas. Os labels existem para dar destaque a palavras e sentenças curtas.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <code>Label</code> possui parâmetros que permitem customizar aspectos visuais como cor de fundo, fonte e contorno.</p>

    <DemoBox>
      <Stepper
        step={2}
        steps={[
          { name: 'Dados pessoais' },
          { name: 'Endereço de entrega', warning: 'Seu endereço oferece opções de entrega mais rápidos' },
          { name: 'Dados de pagamento' },
          { name: 'Checkout' }
        ]}
      />
    </DemoBox>
    
    <CodeBox>{ExampleCode}</CodeBox>

    <p>Além destes comandos, você pode utilizar outros parâmetros para configurar um elemento <code>Label</code>.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>message</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>text</TableCell>
          <TableCell>Aplica um texto específico no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>rounded</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica laterais redondas no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>outline</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica esquema de cores de fundo branco no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>color</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>primary, success, warning, alert</TableCell>
          <TableCell>Aplica cor de fundo específica no label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>uppercase</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Aplica letras maiúsculas no texto do label</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>noWrap</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>disabled</TableCell>
          <TableCell>Impede quebra de linha do conteúdo do label</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default StepperDocs;
