import React, {useState} from 'react';
import Template, {CodeBox, DemoBox, TableBox} from '../components/template';
import Table, {TableHead, TableBody, TableRow, TableHeading, TableCell} from '../lib/Table';
import Dialog from '../lib/Dialog';
import Button from '../lib/Button';

const ImportCode = `import Dialog from 'elementar-ui/dist/Dialog';`;

const ExampleCode = [
`state = {
  dialog: false
}

toggleDialog = () => this.setState({ dialog: !this.state.dialog });

...

<Dialog
  type="success"
  message="Você aceita o fato de que essa caixa de diálogo é apenas temporária?"
  active={dialog}
  confirm={{
    label: 'Aceitar',
    onClick: this.toggleDialog
  }}
  stop={{
    label: 'Rejeitar',
    to: '/outra-pagina'
  }}
  dismiss={{
    label: 'Ignorar',
    onClick: this.toggleDialog
  }}
/>
`,
`teste`
];

const DialogDocs = () => {
  const [dialogThree, toggleDialogThree] = useState(false);
  const [dialogTwo, toggleDialogTwo] = useState(false);

  return (
    <Template title="Caixas de diálogo" id="dialog">
      <p><strong>Caixas de diálogo</strong> são elementos de interação que demandam do usuário uma pronta resposta para determinada ação. As caixas de diálogo são derivadas do componente de modal, mas não dependem dele para que funcionem corretamente.</p>
  
      <CodeBox>{ImportCode}</CodeBox>

      <p>Com a caixa de diálogo, é possível exibir rapidamente uma série de ações rápidas para um evento acionado pelo usuário. Por meio da caixa de diálogo, ele pode confirmar uma ação de dependa de explicação adicional, fazer uma ação inversa ou cancelar o procedimento desejado.</p>
  
      <DemoBox>
        <Button label="Ativar caixa de diálogo" color="primary" rounded onClick={() => toggleDialogThree(!dialogThree)} />
        <Dialog
          type="warning"
          message="Você aceita o fato de que essa caixa de diálogo é apenas temporária?"
          active={dialogThree}
          confirm={{
            label: 'Aceitar',
            onClick: () => toggleDialogThree(!dialogThree)
          }}
          stop={{
            label: 'Rejeitar',
            onClick: () => toggleDialogThree(!dialogThree)
          }}
          dismiss={{
            label: 'Ignorar',
            onClick: () => toggleDialogThree(!dialogThree)
          }}
          data-test="TEST"
        />
      </DemoBox>
      
      <CodeBox>{ExampleCode[0]}</CodeBox>
  
      <DemoBox>
        <Button label="Ativar caixa de diálogo" color="primary" rounded onClick={() => toggleDialogTwo(!dialogTwo)} />
        <Dialog
          emoji={`\u{1F1E7}\u{1F1F7}`}
          message="Você aceita o fato de que essa caixa de diálogo tem apenas duas opções?"
          active={dialogTwo}
          confirm={{
            label: 'Aceitar',
            loading: true,
            onClick: () => toggleDialogTwo(!dialogTwo)
          }}
          stop={{
            label: 'Rejeitar',
            onClick: () => toggleDialogTwo(!dialogTwo)
          }}
        />
      </DemoBox>
      
      <CodeBox>{ExampleCode[1]}</CodeBox>
  
      <p>Você pode utilizar estes parâmetros para configurar o componente <code>Dialog</code>:</p>
  
      <TableBox>

        <Table closed striped>
          <TableHead>
            <TableRow>
              <TableHeading>Parâmetro</TableHeading>
              <TableHeading>Tipo</TableHeading>
              <TableHeading>Padrão</TableHeading>
              <TableHeading>Efeito</TableHeading>
            </TableRow>
          </TableHead>
    
          <TableBody>
            <TableRow>
              <TableCell bold>active</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Controla a exibição da caixa de diálogo</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>message</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>Mensagem</TableCell>
              <TableCell>Insere conteúdo de texto dentro da caixa de diálogo</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>type</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>primary, success, warning, danger</TableCell>
              <TableCell>Especifica o ícone e cor da caixa de diálogo, de acordo com o contexto</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>confirm</TableCell>
              <TableCell italic>object</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica os textos e ações do botão de confimar da caixa de diálogo</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>stop</TableCell>
              <TableCell italic>object</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica os textos e ações do botão de parar da caixa de diálogo</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>dismiss</TableCell>
              <TableCell italic>object</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica os textos e ações do botão de cancelar da caixa de diálogo</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableBox>

      <p>Os parâmetros <code>confirm</code>, <code>stop</code> e <code>dismiss</code> correspondem a objetos que contêm configurações necessárias para cada ação.</p>

      <TableBox>
        <Table closed striped>
          <TableHead>
            <TableRow>
              <TableHeading>Parâmetro</TableHeading>
              <TableHeading>Tipo</TableHeading>
              <TableHeading>Padrão</TableHeading>
              <TableHeading>Efeito</TableHeading>
            </TableRow>
          </TableHead>
    
          <TableBody>
            <TableRow>
              <TableCell bold>label</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>Confirmar, Parar, Cancelar</TableCell>
              <TableCell>Especifica o texto dentro de cada botão</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>disabled</TableCell>
              <TableCell italic>bool</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Desabilita o botão da caixa de diálogo</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>top</TableCell>
              <TableCell italic>string</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica o destino dentro da aplicação para direcionar o usuário</TableCell>
            </TableRow>
            <TableRow>
              <TableCell bold>onClick</TableCell>
              <TableCell italic>function</TableCell>
              <TableCell italic>disabled</TableCell>
              <TableCell>Especifica as ações de cada botão da caixa de diálogo</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </TableBox>
    </Template>
  );
}

export default DialogDocs;