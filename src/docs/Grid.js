import React from 'react';
import styled from 'styled-components';
import Template, {CodeBox, DemoBox} from '../components/template';
import Table, { TableHead, TableBody, TableRow, TableHeading, TableCell } from '../lib/Table';
import Grid from '../lib/Grid';

const GridBoxDemo = styled.div`
  margin-bottom: 20px;
  border-radius: 2px;
  padding: 6px;
  background: cyan;
  text-align: center;
  font-weight: bold;
`;

const ImportCode = `import Grid from 'elementar-ui/dist/Grid';`;

const ExampleCode = [
`<Grid container>
  <Grid row>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
    <Grid md="1">1</Grid>
  </Grid>
  <Grid row>
    <Grid md="2">2</Grid>
    <Grid md="2">2</Grid>
    <Grid md="2">2</Grid>
    <Grid md="2">2</Grid>
    <Grid md="2">2</Grid>
    <Grid md="2">2</Grid>
  </Grid>
  <Grid row>
    <Grid md="3">3</Grid>
    <Grid md="3">3</Grid>
    <Grid md="3">3</Grid>
    <Grid md="3">3</Grid>
  </Grid>
  <Grid row>
    <Grid md="4">4</Grid>
    <Grid md="4">4</Grid>
    <Grid md="4">4</Grid>
  </Grid>
  <Grid row>
    <Grid md="5">5</Grid>
    <Grid md="7">7</Grid>
  </Grid>
  <Grid row>
    <Grid md="6">6</Grid>
    <Grid md="6">6</Grid>
  </Grid>
  <Grid row>
    <Grid md="7">7</Grid>
    <Grid md="4">4</Grid>
    <Grid md="1">1</Grid>
  </Grid>
  <Grid row>
    <Grid md="8">8</Grid>
    <Grid md="2">2</Grid>
    <Grid md="2">2</Grid>
  </Grid>
  <Grid row>
    <Grid md="9">9</Grid>
    <Grid md="3">3</Grid>
  </Grid>
  <Grid row>
    <Grid md="10">10</Grid>
    <Grid md="2">2</Grid>
  </Grid>
  <Grid row>
    <Grid md="11">11</Grid>
    <Grid md="1">1</Grid>
  </Grid>
  <Grid row>
    <Grid md="12">12</Grid>
  </Grid>
</Grid>`,
`<Grid container>
  <Grid row>
    <Grid xs="6"><GridBoxDemo>xs</GridBoxDemo></Grid>
    <Grid xs="6"><GridBoxDemo>xs</GridBoxDemo></Grid>
  </Grid>
  <Grid row>
    <Grid sm="6"><GridBoxDemo>sm</GridBoxDemo></Grid>
    <Grid sm="6"><GridBoxDemo>sm</GridBoxDemo></Grid>
  </Grid>
  <Grid row>
    <Grid md="6"><GridBoxDemo>md</GridBoxDemo></Grid>
    <Grid md="6"><GridBoxDemo>md</GridBoxDemo></Grid>
  </Grid>
  <Grid row>
    <Grid lg="6"><GridBoxDemo>lg</GridBoxDemo></Grid>
    <Grid lg="6"><GridBoxDemo>lg</GridBoxDemo></Grid>
  </Grid>
  <Grid row>
    <Grid xl="6"><GridBoxDemo>xl</GridBoxDemo></Grid>
    <Grid xl="6"><GridBoxDemo>xl</GridBoxDemo></Grid>
  </Grid>
</Grid>`
];

const GridDocs = () => (
  <Template title="Grid" id="grid">
    <p>O <strong>Grid</strong> é um sistema básico de estrutura, organização e disposição de conteúdo dentro do site. Com um grid, é possível distribuir com facilidade o conteúdo de uma página, de acordo com a necessidade da tela.</p>

    <CodeBox>{ImportCode}</CodeBox>

    <p>O componente <code>Grid</code> permite que sejam criados grids com 12 colunas e algumas larguras pré-determinadas, com base nas dimensões de telas de dispositivos modernos.</p>

    <DemoBox>
      <Grid container>
        <Grid row>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="3"><GridBoxDemo>3</GridBoxDemo></Grid>
          <Grid md="3"><GridBoxDemo>3</GridBoxDemo></Grid>
          <Grid md="3"><GridBoxDemo>3</GridBoxDemo></Grid>
          <Grid md="3"><GridBoxDemo>3</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="4"><GridBoxDemo>4</GridBoxDemo></Grid>
          <Grid md="4"><GridBoxDemo>4</GridBoxDemo></Grid>
          <Grid md="4"><GridBoxDemo>4</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="5"><GridBoxDemo>5</GridBoxDemo></Grid>
          <Grid md="7"><GridBoxDemo>7</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="6"><GridBoxDemo>6</GridBoxDemo></Grid>
          <Grid md="6"><GridBoxDemo>6</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="7"><GridBoxDemo>7</GridBoxDemo></Grid>
          <Grid md="4"><GridBoxDemo>4</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="8"><GridBoxDemo>8</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="9"><GridBoxDemo>9</GridBoxDemo></Grid>
          <Grid md="3"><GridBoxDemo>3</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="10"><GridBoxDemo>10</GridBoxDemo></Grid>
          <Grid md="2"><GridBoxDemo>2</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="11"><GridBoxDemo>11</GridBoxDemo></Grid>
          <Grid md="1"><GridBoxDemo>1</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="12"><GridBoxDemo>12</GridBoxDemo></Grid>
        </Grid>
      </Grid>
    </DemoBox>
    
    <CodeBox>{ExampleCode[0]}</CodeBox>

    <p>As colunas do sistema de grid podem ter seu tamanho variado de acordo com a largura do dispositivo que está exibindo a tela.</p>
    
    <DemoBox>
      <Grid container>
        <Grid row>
          <Grid xs="6"><GridBoxDemo>xs</GridBoxDemo></Grid>
          <Grid xs="6"><GridBoxDemo>xs</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid sm="6"><GridBoxDemo>sm</GridBoxDemo></Grid>
          <Grid sm="6"><GridBoxDemo>sm</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid md="6"><GridBoxDemo>md</GridBoxDemo></Grid>
          <Grid md="6"><GridBoxDemo>md</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid lg="6"><GridBoxDemo>lg</GridBoxDemo></Grid>
          <Grid lg="6"><GridBoxDemo>lg</GridBoxDemo></Grid>
        </Grid>
        <Grid row>
          <Grid xl="6"><GridBoxDemo>xl</GridBoxDemo></Grid>
          <Grid xl="6"><GridBoxDemo>xl</GridBoxDemo></Grid>
        </Grid>
      </Grid>
    </DemoBox>
    
    <CodeBox>{ExampleCode[1]}</CodeBox>

    <p>O componente <code>Grid</code> pode ser um <code>container</code>, <code>row</code> e uma coluna. De acordo com sua função, ele pode receber parâmetros diversos.</p>

    <Table closed striped>
      <TableHead>
        <TableRow>
          <TableHeading>Parâmetro</TableHeading>
          <TableHeading>Tipo</TableHeading>
          <TableHeading>Padrão</TableHeading>
          <TableHeading>Efeito</TableHeading>
        </TableRow>
      </TableHead>
      <TableBody>
        <TableRow>
          <TableCell bold>container</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Configura o elemento Grid como um container</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>breakpoint</TableCell>
          <TableCell italic>string</TableCell>
          <TableCell italic>xs, sm, md, lg, xl</TableCell>
          <TableCell>Define uma largura máxima para o grid em elementos configurados como container</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>row</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Configura o elemento Grid como um row</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>reverse</TableCell>
          <TableCell italic>bool</TableCell>
          <TableCell italic>false</TableCell>
          <TableCell>Exibe as colunas da direita para a esquerda, dentro de um elemento Grid configurado como row</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>gutter</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>20</TableCell>
          <TableCell>Define o espaço entre as colunas</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>xs</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..12</TableCell>
          <TableCell>Especifica a quantidade de espaços ocupados pela coluna em resoluções de até 576px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>xsOffsetLeft</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à esquerda em quantidade de espaços, em resoluções de até 576px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>xsOffsetRight</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à direita em quantidade de espaços, em resoluções de até 576px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>sm</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..12</TableCell>
          <TableCell>Especifica a quantidade de espaços ocupados pela coluna em resoluções de até 768px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>smOffsetLeft</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à esquerda em quantidade de espaços, em resoluções de até 768px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>smOffsetRight</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à direita em quantidade de espaços, em resoluções de até 768px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>md</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..12</TableCell>
          <TableCell>Especifica a quantidade de espaços ocupados pela coluna em resoluções de até 992px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>mdOffsetLeft</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à esquerda em quantidade de espaços, em resoluções de até 992px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>mdOffsetRight</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à direita em quantidade de espaços, em resoluções de até 992px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lg</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..12</TableCell>
          <TableCell>Especifica a quantidade de espaços ocupados pela coluna em resoluções de até 1200px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lgOffsetLeft</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à esquerda em quantidade de espaços, em resoluções de até 1200px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>lgOffsetRight</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à direita em quantidade de espaços, em resoluções de até 1200px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>xs</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..12</TableCell>
          <TableCell>Especifica a quantidade de espaços ocupados pela coluna em resoluções de até 1920px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>xsOffsetLeft</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à esquerda em quantidade de espaços, em resoluções de até 1920px.</TableCell>
        </TableRow>
        <TableRow>
          <TableCell bold>xsOffsetRight</TableCell>
          <TableCell italic>integer</TableCell>
          <TableCell italic>1..11</TableCell>
          <TableCell>Especifica uma distância lateral à direita em quantidade de espaços, em resoluções de até 1920px.</TableCell>
        </TableRow>
      </TableBody>
    </Table>
  </Template>
);

export default GridDocs;
