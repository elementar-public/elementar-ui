import React, { useState, useEffect, useCallback, useRef } from 'react';
import styled from 'styled-components';
import LinkButton from './LinkButton';

const PopoverContainer: any = styled.span`
  display: inline-block;
  vertical-align: top;
  position: relative;
  cursor: help;
`;

PopoverContainer.Box = styled.span<{ active: boolean, bottom: boolean, position: string, dark: boolean }>`
  display: ${({ active }) => (active) ? 'block' : 'none'};
  position: absolute;
  top: ${({ bottom }) => (bottom) ? 'calc(100% + 4px)' : 'auto'};
  bottom: ${({ bottom }) => (bottom) ? 'auto' : 'calc(100% + 4px)'};
  left: ${({ position }) => (position === 'left') ? '0' : (position === 'right' ? 'auto' : '50%')};
  right: ${({ position }) => (position === 'right') ? '0' : 'auto'};
  transform: ${({ position }) => (position === 'left' || position === 'right') ? 'none' : 'translateX(-50%)'};
  z-index: 1;
  font-size: 1rem;
  font-style: normal;
  font-weight: normal;
  line-height: 1.2rem;
  letter-spacing: 0;
  width: max-content;
  max-width: 300px;
  box-sizing: border-box;
  margin: 0;
  padding: 10px;
  background: ${({ dark }) => dark ? 'rgba(0,0,0,0.95)' : 'rgba(255,255,255,0.95)'};
  color: ${({ dark }) => dark ? 'white' : '#333'};
  border-radius: 4px;
  box-shadow: 0 0 0 1px rgba(0,0,0,0.1), 0 2px 4px rgba(0,0,0,0.1);
`;

PopoverContainer.Message = styled.span`
  display: block;
  margin: 0 0 10px;
  padding: 0;
`;

PopoverContainer.Action = styled.span`
  display: block;
  margin: 0;
  padding: 0;
`;

interface PopoverProps {
  children: React.ReactNode | React.ReactNode[],
  message: string,
  label: string,
  align: string,
  position: 'left' | 'right',
  bottom: boolean,
  action: (e?: any) => void,
  href: string,
  dark: boolean,
  to: string,
}

const Popover = ({
  children,
  message = 'Mensagem do popover',
  label = 'Fechar',
  align = 'center',
  position,
  bottom,
  action,
  href,
  dark,
  to,
  ...otherProps
}: PopoverProps) => {
  const [open, toggleOpen] = useState(false);
  const popoverRef = useRef<HTMLSpanElement>(null);

  const handleMenuClick = useCallback(({ target }: any) => {
    if (popoverRef && open === true && !popoverRef.current?.contains(target)) toggleOpen(false);
  }, [open]);

  useEffect(() => {
    document.addEventListener('mousedown', handleMenuClick, false);

    return () => {
      document.removeEventListener('mousedown', handleMenuClick);
    }
  });

  return (
    <PopoverContainer onClick={() => toggleOpen(!open)} {...otherProps}>
      <span>{children}</span>

      <PopoverContainer.Box active={open} dark={dark} position={position} bottom={bottom} ref={popoverRef}>
        <PopoverContainer.Message>
          {message}
        </PopoverContainer.Message>

        <PopoverContainer.Action>
          {(to) ? (
            <LinkButton dark={dark} to={to} small label={label} />
          ) : (
            (href) ? (
              <LinkButton dark={dark} href={href} target="_blank" rel="noopener noreferrer" small label={label} />
            ) : (
              <LinkButton dark={dark} small label={label} onClick={action || null} />
            )
          )}
        </PopoverContainer.Action>
      </PopoverContainer.Box>
    </PopoverContainer>
  );
}

export default Popover;

