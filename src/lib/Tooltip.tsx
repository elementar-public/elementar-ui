import React from 'react';
import styled from 'styled-components';

const TooltipContainer: any = styled.span<{ active: boolean, bottom: boolean, position: string, light: boolean }>`
  display: inline-block;
  vertical-align: top;
  position: relative;

  &:before,
  &:after {
    display: ${({ active }) => active ? 'inline-block' : 'none'};
    position: absolute;
    top: ${({ bottom }) => bottom ? 'calc(100% + 4px)' : 'auto'};
    bottom: ${({ bottom }) => bottom ? 'auto' : 'calc(100% + 4px)'};
    left: ${({ position }) => position === 'left' ? '0' : (position === 'right' ? 'auto' : '50%')};
    right: ${({ position }) => position === 'right' ? '0' : 'auto'};
    pointer-events: none;
    z-index: 1;
  }

  &:before {
    transform: ${({ position }) => position === 'left' || position === 'right' ? 'none' : 'translateX(-50%)'};
    content: attr(data-tooltip);
    font-size: 0.85rem;
    font-style: normal;
    font-weight: normal;
    line-height: 1rem;
    letter-spacing: 0;
    background: ${({ light }) => light ? 'rgba(255,255,255,0.95)' : 'rgba(0,0,0,0.95)'};
    color: ${({ light }) => light ? '#333' : 'white'};
    padding: 5px;
    text-align: center;
    width: max-content;
    max-width: 200px;
    border-radius: 4px;
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    text-transform: none;
  }

  &:after {
    content: '';
    border: 5px solid transparent;
    border-top-color: ${({ bottom, light }) => bottom ? 'transparent' : (light ? 'rgba(255,255,255,0.95)' : 'rgba(0,0,0,0.95)')};
    border-bottom-color: ${({ bottom, light }) => bottom ? (light ? 'rgba(255,255,255,0.95)' : 'rgba(0,0,0,0.95)') : 'transparent'};
    transform: translate(-50%, ${({ bottom }) => bottom ? '-100%' : '100%' });
    left: 50%;
    right: auto;
  }

  &:hover:before,
  &:hover:after {
    display: inline-block;
  }
`;

type TooltipProps = {
  className?: string,
  position?: string,
  message?: string,
  bottom?: boolean,
  light?: boolean,
  active?: boolean,
  children: React.ReactNode,
  [otherProps: string]: any,
}

const Tooltip = ({
  message = 'Mensagem',
  position = 'center',
  className,
  children,
  light,
  bottom,
  active,
  ...otherProps
}: TooltipProps) => (
  <TooltipContainer
    className={className}
    data-tooltip={message}
    active={active}
    light={light}
    bottom={bottom}
    position={position}
    {...otherProps}
  >
    {children}
  </TooltipContainer>
)

export default Tooltip;
