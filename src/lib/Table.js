import styled from 'styled-components';

const Table = styled.table`
  font-size: 0.9rem;
  width: 100%;
  border-spacing: 0;
  border-collapse: collapse;
  margin: ${({ noMargin }) => noMargin ? '0' : '12px 0'};
  background: ${({ closed }) => closed ? 'white' : 'none'};
  table-layout: ${({ fixed }) => fixed ? 'fixed' : 'auto'};

  ${({ closed }) => closed && `
    border: 1px solid rgba(0,0,0,0.1);
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    background: #FFF;
  `}

  ${({ striped }) => striped && `
    tbody tr:nth-child(odd) td {
      background: rgba(0,0,0,0.05);
    }
  `}
  
  ${({ compact }) => compact && `
    tbody tr:nth-child(odd) td {
      padding: 6px 12px;
    }
  `}

  ${({ verticalAlign }) => verticalAlign && `
    th, td {
      vertical-align: ${verticalAlign}
    }
  `}
`;

const TableHead = styled.thead`
  font-weight: bold;
`;

const TableBody = styled.tbody``;

const TableFooter = styled.tfoot`
  font-weight: bold;
`;

const TableRow = styled.tr``;

const TableHeading = styled.th`
  text-align: left;
  font-weight: bold;
  padding: 12px;
  border-top: 1px solid rgba(0,0,0,0.1);
  border-bottom: 1px solid rgba(0,0,0,0.1);
  margin: 0;
`;

const TableCell = styled.td`
  text-align: left;
  padding: 12px;
  border-top: 1px solid rgba(0,0,0,0.1);
  border-bottom: 1px solid rgba(0,0,0,0.1);
  margin: 0;
  font-weight: ${({ bold }) => bold ? 'bold' : 'auto'};
  font-style: ${({ italic }) => italic ? 'italic' : 'auto'};
  text-align: ${({ align, number }) => number ? 'right' : (align || 'auto')};
`;

export default Table;
export {
  TableHead,
  TableBody,
  TableFooter,
  TableRow,
  TableHeading,
  TableCell
}