import React from 'react';
import styled from 'styled-components';

const FloaterBox: any = styled.div<{
  width: string,
  spacing: number,
  minHeight: string,
  height: string,
  active: boolean,
  cover: boolean
}>`
  background: white;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  border-radius: 4px;
  padding: ${({ spacing }) => spacing * 2 + 'px'};
  width: 100%;
  min-width: 200px;
  max-width: ${({ width }) => width};
  min-height: ${({ minHeight }) => minHeight};
  max-height: ${({ height }) => height};
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(${({ active }) => active ? '1, 1' : '0.8, 0.8'});
  overflow: hidden;
  transition: all 0.25s ease-in-out;

  ${({cover}) => cover && `
    @media screen and (max-width: 419px) {
      height: 100%;
      transform: none;
      width: calc(100vw - 20px);
      max-width: 100%;
      max-height: calc(100vh - 20px);
      min-width: 0;
      top: 10px;
      left: 10px;
      right: 10px;
      bottom: 10px;
    }
  `}
`;

FloaterBox.Backdrop = styled.div<{ active: boolean }>`
  visibility: ${({ active }) => active ? 'visible' : 'hidden'};
  backface-visibility: hidden;
  opacity:  ${({ active }) => active ? '1' : '0'};
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.3);
  z-index: 12;
  transition: all 0.25s ease-in-out;
`;

type FloaterProps = {
  className?: string,
  width?: string,
  minHeight?: string,
  height?: string,
  spacing?: number,
  active: boolean,
  cover?: boolean,
  children: React.ReactNode,
  [otherProps: string]: any,
}

const Floater = ({
  width = '250px',
  spacing = 10,
  height = '400px',
  active,
  className,
  minHeight,
  cover,
  children,
  ...otherProps
}: FloaterProps) => (
  <FloaterBox.Backdrop active={active}>
    <FloaterBox
      className={className}
      width={width}
      minHeight={minHeight}
      height={height}
      spacing={spacing}
      cover={cover}
      active={active}
      {...otherProps}
    >
      {children}
    </FloaterBox>
  </FloaterBox.Backdrop>
);

export default Floater;