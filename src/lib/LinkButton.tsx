import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';

interface BasicLinkButtonProps {
  className?: string,
  children?: React.ReactNode | React.ReactNode[],
  type?: string,
  small?: boolean,
  label: string,
  to?: string,
  href?: string,
  dark?: boolean,
  noWrap?: boolean,
  [otherProps: string]: any
}

const BasicLinkButton = ({
  className,
  children,
  type = 'button',
  small,
  label,
  to,
  href,
  dark,
  noWrap,
  ...otherProps
}: BasicLinkButtonProps) => {
  if (to) {
    return <Link className={className} to={to}>{label || children}</Link>;
  } else {
    return React.createElement(
      (href ? 'a' : 'button'),
      {
        className: className,
        href: (href || null),
        type: (href ? null : type),
        ...otherProps
      },
      (label || children)
    );
  }
}

const LinkButton = styled(BasicLinkButton)<{ color: string, lowercase: boolean }>`
  font-family: ${({ theme }) => theme.typography.fontFamily};
  font-size: ${({ small, theme }) => small ? theme.typography.fontSize.small : theme.typography.fontSize.default};
  line-height: ${({ small, theme }) => small ? theme.buttons.sizes.small.lineHeight : theme.buttons.sizes.default.lineHeight};
  white-space: ${({ noWrap }) => noWrap ? 'no-wrap' : 'normal'};
  color: ${({ color, theme }) => color ? theme.colors[color].regular : theme.colors.text.regular};
  background: none;
  border: none;
  padding: 2px 4px;
  text-transform: ${({ lowercase }) => lowercase ? 'lowercase' : 'auto'};
  display: inline-block;
  text-decoration: underline;
  transition: color 0.2s;
  overflow: hidden;
  box-sizing: border-box;
  cursor: pointer;

  &:hover,
  &.hover {
    color: ${({ color, theme }) => color ? theme.colors[color].hover : theme.colors.text.hover};
  }

  &:active,
  &.active {
    color: ${({ color, theme }) => color ? theme.colors[color].active : theme.colors.text.active};
  }

  &:disabled,
  &.disabled {
    opacity: 0.5;
    cursor: default;
    pointer-events: none;
  }
`;

export default LinkButton;

LinkButton.defaultProps = {
  type: 'button'
}
