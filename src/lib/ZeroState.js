import React from 'react';
import styled from 'styled-components';
import Button from './Button';
import WarningIcon from './Icons/Warning';
import InfoIcon from './Icons/Info';

const ZeroStateContainer = styled.div`
  text-align: center;
  width: 100%;
  margin: 100px 0;
`;

const ZeroStateIcon = styled.div`
  width: 72px;
  height: 72px;
  border-radius: 36px;
  margin: 0 auto 20px;
  display: flex;
  align-items: center;
  justify-content: center;
  color: white;
  background: ${({ theme, error }) => error ? theme.colors.warning.regular : theme.colors.dimmed.active};

  svg {
    display: block;
    width: 36px;
    height: 36px;
  }
`;

const ZeroStateMessage = styled.p`
  color: ${({ theme }) => theme.colors.default.regular};
  font-size: 1.2rem;
  margin: 30px 50px;
`;

const ZeroStateError = styled.p`
  font-size: 0.8rem;
  color: ${({ theme }) => theme.colors.dimmed.active};
  margin: 30px 0 0;
`;

const ZeroStateActions = styled.div`
  display: flex;
  justify-content: center;

  > * {
    margin-left: 10px;

    &:first-child {
      margin: 0;
    }
  }

  @media screen and (max-width: 599px) {
    flex-direction: column;

    > * {
      margin-top: 10px; 
      margin-left: 0;

      &:first-child {
        margin: 0;
      }
    }
  }
`;

const ZeroState = ({
  action,
  error,
  callback,
  message,
  event,
  ...otherProps
}) => {
  if (event) event();

  if (error) return (
    <ZeroStateContainer {...otherProps}>
      <ZeroStateIcon error>
        <WarningIcon />
      </ZeroStateIcon>

      { message
        ?
        <ZeroStateMessage>{message}</ZeroStateMessage>
        :
        <ZeroStateMessage>Houve um erro ao carregar a tela. Clique no botão abaixo para tentar novamente, ou selecione outra tela no menu.</ZeroStateMessage>
      }

      <ZeroStateActions>
        <Button outline color="warning" label={(callback && callback.label) || 'Tentar novamente'} onClick={callback && callback.onClick} disabled={callback && callback.disabled} loading={callback && callback.pending} />
        {action && <Button outline light label={action.label || 'Desfazer'} onClick={action.onClick} disabled={action.disabled} loading={action.pending} />}
      </ZeroStateActions>

      <ZeroStateError>{error.toString()}</ZeroStateError>
    </ZeroStateContainer>
  );

  return (
    <ZeroStateContainer {...otherProps}>
      <ZeroStateIcon>
        <InfoIcon />
      </ZeroStateIcon>

      { message
        ?
        <ZeroStateMessage>{message}</ZeroStateMessage>
        :
        <ZeroStateMessage>Não foi encontrado nenhum resultado para esta tela. {action ? 'Clique no botão abaixo para fazer uma ação.' : 'Selecione outra opção, ou volte nesta tela mais tarde.'}</ZeroStateMessage>
      }
  
      {action && <ZeroStateActions><Button outline light color="primary" label={action.label || 'Ação'} onClick={action.onClick} disabled={action.disabled} loading={action.pending} /></ZeroStateActions>}
    </ZeroStateContainer>
  );
}

export default ZeroState;
