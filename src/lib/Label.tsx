import React from 'react';
import styled from 'styled-components';

const LabelElement = styled.span<{ bold: boolean, noWrap: boolean, rounded: boolean, uppercase: boolean, outline: boolean }>`
  display: inline-block;
  color: white;
  font-weight: ${({ bold }) => bold ? 'bold' : 'normal'};
  font-size: 0.9rem;
  line-height: 1rem;
  letter-spacing: 0;
  white-space: ${({ noWrap }) =>  noWrap ? 'nowrap' : 'normal'};
  border-radius: ${({ rounded }) => rounded ? '20px' : '2px'};
  text-transform: ${({ uppercase }) =>  uppercase ? 'uppercase' : 'none'};

  ${({ theme, outline, color, rounded }) => (outline ? `
    background: none;
    color: ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
    padding: ${(rounded) ? theme.labels.outline.roundedPadding : theme.labels.outline.padding};
    border: 1px solid ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
  `
  :
  `
    background: ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
    color: ${(color) ? theme.colors[color].fontColor : theme.colors.default.fontColor};
    padding: ${(rounded) ? theme.labels.regular.roundedPadding : theme.labels.regular.padding};
    border: none;
    text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.2);
    box-shadow: inset 0 0 0 1px rgba(0, 0, 0, 0.1);
  `
  )}
`;

const Label = ({
  className,
  children,
  message = 'Texto',
  bold,
  color,
  rounded,
  outline,
  noWrap,
  uppercase,
  ...otherProps
}: {
  className?: string,
  children?: React.ReactNode,
  message?: string,
  color?: string,
  bold?: boolean,
  rounded?: boolean,
  outline?: boolean,
  noWrap?: boolean,
  uppercase?: boolean
}) => React.createElement(LabelElement, { className, rounded, outline, bold, color, noWrap, uppercase, ...otherProps}, (children || message));

export default Label;
