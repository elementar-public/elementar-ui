import React from 'react';
import styled from 'styled-components';
import Tooltip from './Tooltip';

const StepperBar = styled.ul`
  margin: 0 0 20px;
  padding: 0;
  list-style: none;
  display: flex;
`;

const StepperUnit = styled.li`
  flex: 0 1 100%;
`;

const StepperLabel = styled.p`
  margin: 0;
  padding: 0;
  font-size: 0.8rem;
  font-weight: ${({current}) => current ? 'bold' : 'normal'};
  line-height: 0.9rem;
  text-align: center;
  text-transform: uppercase;
`;

const StepperIconContainer = styled.div`
  color: ${({ theme, warning, color }) => warning ? theme.colors.warning.regular : theme.colors[color].regular};
`;

const StepperIcon = ({
  step,
  length,
  current,
  warning
}) => (
  <svg width="100%" height="40">
    {step > 0 &&            <line x1="0" x2="50%" y1="20" y2="20" stroke="#EEE" strokeWidth="2px" />}
    {step < (length - 1) && <line x1="50%" x2="100%" y1="20" y2="20" stroke="#EEE" strokeWidth="2px" />}
    <circle cx="50%" cy="20" r="15" fill={step <= current ? (step === current ? '#999' : 'currentColor') : '#CCC'} />

    <svg x="50%" y="50%" overflow="visible">
      {step >= current
        ?
          <g>
            <text x="0" y="6" fill="#FFF" textAnchor="middle" style={{fontWeight:'bold'}}>{step + 1}</text>
          </g>
        :
          warning
          ?
            <g>
              <line x1="0" x2="0" y1="-5" y2="0" stroke="white" strokeWidth="2px" strokeLinecap="round" />
              <circle cx="0" cy="5" r="1.5" fill="white" stroke="none" />
            </g>
          :
            <path d="M -8 0 l 5 5 l 10 -10" fill="none" stroke="white" strokeWidth="2px" />
      }
    </svg>
  </svg>
);

const Stepper = ({
  steps,
  step,
  color,
  ...otherProps
}) => (
  <StepperBar {...otherProps}>
    {steps.length > 0 && steps.map((s, i) =>
      s.warning
      ?
      <StepperUnit key={i}>
        <Tooltip message={s.warning}>
          <StepperIconContainer color={color} warning={s.warning}>
            <StepperIcon step={i} current={step} length={steps.length} warning={s.warning} />
          </StepperIconContainer>
          <StepperLabel current={step === i}>{s.name || 'Passo ' + i}</StepperLabel>
        </Tooltip>
      </StepperUnit>
      :
      <StepperUnit key={i}>
        <StepperIconContainer color={color} warning={s.warning}>
          <StepperIcon step={i} current={step} length={steps.length} warning={s.warning} />
        </StepperIconContainer>
        <StepperLabel current={step === i}>{s.name || 'Passo ' + i}</StepperLabel>
      </StepperUnit>
    )}
  </StepperBar>
);

export default Stepper;

Stepper.defaultProps = {
  color: 'success'
}
