import React, { useState } from 'react';
import styled from 'styled-components';

const Icon = ({ className, open }) => (
  <svg xmlns="http://www.w3.org/2000/svg" width="30" height="30" viewBox="0 0 30 30" className={className}>
    { open
      ?
      <path d="M8,12l7,7,7-7" fill="none" stroke="currentColor" strokeWidth="2" />
      :
      <path d="M8,19l7-7,7,7" fill="none" stroke="currentColor" strokeWidth="2" />
    }
  </svg>
);

const AccordionContainer = styled.div`
  display: block;
  padding: 10px 0;
`;

const AccordionTitle = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  cursor: pointer;
`;

const AccordionTitlePlace = styled.h2`
  margin: 0;
  padding: 0;
  user-select: none;
  pointer-events: none;
`;

const AccordionContent = styled.div`
  display: ${props => props.open ? 'block' : 'none'};
`;

const AccordionIcon = styled(Icon)`
  display: block;
  pointer-events: none;
`;

const Accordion = ({
  children,
  title,
  headingElement,
  element,
  open,
  ...otherProps
}) => {
  const [ openItem, setOpen ] = useState(open);

  return (
    <AccordionContainer as={element} {...otherProps}>
      <AccordionTitle onClick={() => setOpen(!openItem)} key="title">
        <AccordionTitlePlace as={headingElement}>{title}</AccordionTitlePlace>
        <AccordionIcon open={openItem} />
      </AccordionTitle>
      <AccordionContent open={openItem} key="content">{children}</AccordionContent>
    </AccordionContainer>
  )
};

export default Accordion;

Accordion.defaultProps = {
  title: 'Título',
  open: false
}