import React from 'react';
import styled from 'styled-components';
import CloseIcon from './Icons/Close';
import CheckCircleIcon from './Icons/CheckCircle';
import InfoIcon from './Icons/Info';
import WarningIcon from './Icons/Warning';
import ErrorIcon from './Icons/Error';

const BoxContainer = styled.div<{ noMargin?: boolean, spacing: number, overflowHidden?: boolean, elevation: number }>`
  background: white;
  border-radius: 4px;
  margin-bottom: ${({ noMargin }) => noMargin ? '0' : '12px'};
  padding: ${({ spacing }) => `${spacing * 4}px`};
  overflow: ${({ overflowHidden }) => overflowHidden ? 'hidden' : 'visible'};
  box-shadow: ${({ elevation }) => elevation === 0 ? 'none' : `0 ${elevation}px ${elevation * 2}px rgba(0,0,0,0.2)`};
`;

const BoxAlertContainer: any = styled.div<{ alert?: string, noMargin?: boolean }>`
  background: ${({ theme, alert }) => {
    switch (alert) {
      case 'primary':
        return theme.colors.primary.regular;
      case 'success':
        return theme.colors.success.regular;
      case 'warning':
        return theme.colors.warning.regular;
      case 'danger':
        return theme.colors.danger.regular;
      default:
        return theme.colors.default.regular;
  }}};

  color: white;
  margin-bottom: ${({ noMargin }) => noMargin ? '0' : '12px'};
  font-weight: 500;
  font-size: 1rem;
  padding: 12px;
  text-align: left;
  position: relative;
  border-radius: 4px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.2);
  text-shadow: 0 1px 0 rgba(0,0,0,0.1);
`;

BoxAlertContainer.Message = styled.p`
  margin: 0;
  line-height: 1.4rem;
  padding-left: 32px;
`;

BoxAlertContainer.CloseButton = styled.button`
  position: absolute;
  top: 6px;
  right: 6px;
  padding: 0;
  margin: 0;
  background: none;
  border: none;
  color: white;
  opacity: 0.6;
  font-size: 1rem;
  line-height: 1rem;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }

  svg {
    font-size: 1rem;
    transform: scale(1);
  }
`;

BoxAlertContainer.CheckCircleIcon = styled(CheckCircleIcon)`
  font-size: 1.2rem;
  margin-left: -32px;
  vertical-align: text-top;
  float: left;
  fill: currentColor;
  filter: drop-shadow(0 1px 0 rgba(0,0,0,0.25));
  transform: scale(1);
`;

BoxAlertContainer.InfoIcon = styled(InfoIcon)`
  font-size: 1.2rem;
  margin-left: -32px;
  vertical-align: text-top;
  float: left;
  fill: currentColor;
  filter: drop-shadow(0 1px 0 rgba(0,0,0,0.25));
  transform: scale(1);
`;

BoxAlertContainer.WarningIcon = styled(WarningIcon)`
  font-size: 1.2rem;
  margin-left: -32px;
  vertical-align: text-top;
  float: left;
  fill: currentColor;
  filter: drop-shadow(0 1px 0 rgba(0,0,0,0.25));
  transform: scale(1);
`;

BoxAlertContainer.ErrorIcon = styled(ErrorIcon)`
  font-size: 1.2rem;
  margin-left: -32px;
  vertical-align: text-top;
  float: left;
  fill: currentColor;
  filter: drop-shadow(0 1px 0 rgba(0,0,0,0.25));
  transform: scale(1);
`;

interface BoxProps {
  className?: string,
  spacing?: number,
  elevation?: number,
  alert?: string,
  close?: (e?: any) => void,
  message?: string,
  noMargin?: boolean,
  children?: React.ReactNode,
  [otherProps: string]: any,
}

const Box = ({
  className,
  children,
  spacing = 5,
  elevation = 2,
  alert,
  close,
  message = 'Mensagem do box',
  noMargin,
  ...otherProps
}: BoxProps) => {
  if (alert) {
    return (
      <BoxAlertContainer
        className={'elementar-ui-box-container'}
        alert={alert}
        {...otherProps}
      >
        {(close) && (
          <BoxAlertContainer.CloseButton
            type="button"
            onClick={typeof close === 'function' && close}
          >
            <CloseIcon />
          </BoxAlertContainer.CloseButton>
        )}

        <BoxAlertContainer.Message>
          {(() => {
              switch (alert) {
                case 'primary':
                  return <BoxAlertContainer.InfoIcon />;
                case 'success':
                  return <BoxAlertContainer.CheckCircleIcon />;
                case 'warning':
                  return <BoxAlertContainer.WarningIcon />;
                case 'danger':
                  return <BoxAlertContainer.ErrorIcon />;
                default:
                  return <BoxAlertContainer.InfoIcon />;
              }
          })()}

          {message}
        </BoxAlertContainer.Message>
      </BoxAlertContainer>
    )
  } else {
    return (
      <BoxContainer
        className={className}
        spacing={spacing}
        elevation={elevation}
        {...otherProps}
      >
        {children}
      </BoxContainer>
    )
  }
}

export default Box;
