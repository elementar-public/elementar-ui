import React from 'react';
import styled from 'styled-components';

const SelectContainer: any = styled.div<{ width: string, noMargin: string }>`
  width: ${({ width }) => (width) || ('100%')};
  margin: ${({ noMargin }) => (noMargin) ? ('0') : ('0 0 12px')};
`;

SelectContainer.Box = styled.div<{ mobile: boolean }>`
  display: flex;
  flex-direction: ${({ mobile }) => (mobile) ? 'row' : 'column'};
  align-items: stretch;
  justify-content: space-between;
`;

SelectContainer.Label = styled.label<{ mobile: boolean, color: string }>`
  margin: ${({ mobile }) => (mobile) ? ('0') : ('0 0 4px')};
  font-weight: 500;
  font-size: 0.9rem;
  line-height: ${({ mobile }) => (mobile) ? ('29px') : ('1rem')};
  display: block;
  color: ${({ theme, color }) => (color) ? theme.colors[color].regular : theme.colors.default.regular};

  ${({ theme, mobile, color }) => (mobile) && `
    flex: 0 0 auto;
    padding-right: 5px;
    border-bottom: 1px solid ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
  `}
`;

SelectContainer.Place = styled.div`
  flex: 0 1 auto;
  width: 100%;
`;

SelectContainer.Element = styled.select<{ mobile: boolean, color: string, size: string }>`
  width: 100%;
  display: block;
  font-family: ${({ theme }) => theme.typography.fontFamily};
  color: ${({ theme, color }) => (color) ? theme.colors[color].active : theme.colors.text.regular};
  height: ${({ theme, size }) => theme.selects[size].height};
  font-size: ${({ theme, size }) => theme.selects[size].fontSize};
  outline: none;
  overflow: auto;
  box-sizing: border-box;
  margin: 0;
  padding: 0 ${({ theme, size }) => theme.selects[size].padding};
  padding-right: 30px;
  cursor: pointer;
  -webkit-appearance: none;
  -moz-appearance: none;

  ${({ mobile, color, theme, size }) => (mobile) ? `
    border: none;
    border-bottom: 1px solid ${(color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
    border-radius: 0;
  ` : `
    border: 1px solid ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
    border-radius: ${theme.textInputs[size].borderRadius};
  `}

  ${({ color }) => (color) === 'hollow' ?
    `background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iMCAwIDMwIDMwIj48cGF0aCBjbGFzcz0iYiIgZD0iTTcsMTJsNyw3bDcsLTdsLTEsLTFsLTYsNmwtNiwtNmwtMSwxeiIgZmlsbD0iI0ZGRiIgLz48L3N2Zz4=") no-repeat center right;`
    :
    `background: url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIzMCIgaGVpZ2h0PSIzMCIgdmlld0JveD0iMCAwIDMwIDMwIj48cGF0aCBjbGFzcz0iYiIgZD0iTTcsMTJsNyw3bDcsLTdsLTEsLTFsLTYsNmwtNiwtNmwtMSwxeiIgZmlsbD0iIzMzMyIgLz48L3N2Zz4=") no-repeat center right;`
  }

  &:invalid {
    border-color: ${({ theme, mobile, color }) => (mobile) ? ((color) ? theme.colors[color].regular : theme.colors.default.regular) : theme.colors.danger.regular};
    color: ${({ theme, mobile, color }) => (mobile) ? theme.colors.danger.active : ((color) ? theme.colors[color].active : theme.colors.text.regular)};
    outline: none;
    box-shadow: none;
  }

  &:valid {
    color: ${({ theme, color }) => (color) ? theme.colors[color].active : theme.colors.text.regular};
  }

  &:focus {
    outline: none;
  }

  &:disabled {
    opacity: 0.4;
  }

  option {
    padding-left: ${({ theme, size }) => theme.selects[size].paddingOption};
  }
`;

SelectContainer.Help = styled.small`
  color: ${({ theme, color }) => (color) === 'hollow' ? theme.colors.hollow.regular : theme.colors.default.regular};
  font-size: 0.8rem;
  line-height: 1rem;
  display: block;
  margin-top: 4px;
  opacity: 0.6;
`;

type OptionProps = {
  value: string | number,
  label: string,
};

type OptionsProps = OptionProps[];

type SelectProps = {
  id?: string,
  label?: string,
  className?: string,
  containerClassname?: string,
  noMargin?: boolean,
  mobile?: boolean,
  width?: string,
  color?: string,
  size?: string,
  help?: string,
  name?: string,
  inputRef?: any,
  options: OptionsProps,
  [otherProps: string]: any
}

const Select = ({
  width = '100%',
  size = 'default',
  noMargin,
  className,
  containerClassname,
  label,
  mobile,
  color,
  value,
  id,
  help,
  name,
  inputRef,
  options,
  ...otherProps
}: SelectProps) => (
  <SelectContainer
    noMargin={noMargin}
    className={containerClassname}
    width={width}
  >
    <SelectContainer.Box mobile={mobile}>
      {(label) && <SelectContainer.Label mobile={mobile} color={color} htmlFor={id}>{label}</SelectContainer.Label>}
      <SelectContainer.Place>
        <SelectContainer.Element
          dir={mobile ? 'rtl' : 'ltr'}
          name={name}
          id={id}
          value={value}
          color={color}
          ref={inputRef}
          size={size}
          mobile={mobile}
          {...otherProps}
        >
          {(options.map((option: OptionProps | string | number, i: number) =>
            (typeof option === 'object') ? (
              <option key={i} value={option.value || option.label}>{option.label}</option>
            ) : (
              <option key={i}>{option}</option>
            )
          ))}
        </SelectContainer.Element>
      </SelectContainer.Place>
    </SelectContainer.Box>
    {(help) && <SelectContainer.Help color={color}>{help}</SelectContainer.Help>}
  </SelectContainer>
);

export default Select;

