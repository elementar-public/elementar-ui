import React from 'react';
import { IconProps } from './IconTypes';

const EjectIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <path d="M 12 8 l -5.5 5.5 h 11 l -5.5 -5.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="6" x2="18" y1="16.5" y2="16.5" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default EjectIcon;
