import React from 'react';
import { IconProps } from './IconTypes';

const GourmetIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <line x1="3" x2="21" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5.5 20.5 q 1 -6.5 6.5 -6.5 q 5.5 0 6.5 6.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 10.5 14 v -1.5 h 3 v 1.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    <path d="M 7.5 4 c -3 3 3 3 0 6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 11.5 3 c -3 3 3 3 0 6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 15.5 4 c -3 3 3 3 0 6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default GourmetIcon;
