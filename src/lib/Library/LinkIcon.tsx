import React from 'react';
import { IconProps } from './IconTypes';

const LinkIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <path d="M 10.5 12 c 0 3, 1.5 4.5, 4.5 4.5 h 1 c 3 0, 4.5 -1.5, 4.5 -4.5 c 0 -3, -1.5 -4.5, -4.5 -4.5 h -1" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 13.5 12 c 0 -3, -1.5 -4.5, -4.5 -4.5 h -1 c -3 0, -4.5 1.5, -4.5 4.5 c 0 3, 1.5 4.5, 4.5 4.5 h 1" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default LinkIcon;
