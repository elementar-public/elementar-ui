import React from 'react';
import { IconProps } from './IconTypes';

const USBIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="5" cy="19" r="1.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="6" x2="16" y1="18" y2="8" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 18.5 5.5 l -1 4 l -3 -3 l 4 -1 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 7.5 16.5 v -5 l 2 -2" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 8.5 15.5 h 5 l 1 -1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 13.5 13.5 l 2 2 l 2 -2 l -2 -2 l -2 2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="11" cy="8" r="1.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default USBIcon;
