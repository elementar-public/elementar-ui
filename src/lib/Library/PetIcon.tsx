import React from 'react';
import { IconProps } from './IconTypes';

const PetIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 8.5 6.5 v 10 q 0 2, -2 2 q -3 0 -3 -3 v -3 q 0 -6 6 -6 h 2 c 4 0, 3 6, 7 6 q 1 0 1 1 q 0 5 -5 5 h -2" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="12" cy="11" r="1" fill={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PetIcon;