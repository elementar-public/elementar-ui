import React from 'react';
import { IconProps } from './IconTypes';

const PauseIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <line x1="9.5" x2="9.5" y1="6" y2="18" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="14.5" x2="14.5" y1="6" y2="18" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PauseIcon;
