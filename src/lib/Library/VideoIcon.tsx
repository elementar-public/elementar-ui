import React from 'react';
import { IconProps } from './IconTypes';

const VideoIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <rect x="3.5" y="5.5" width="17" height="13" rx="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 9.5 8.5 v 7 l 6 -3.5 l -6 -3.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="6.5" x2="17.5" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default VideoIcon;
