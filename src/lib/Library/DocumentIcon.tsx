import React from 'react';
import { IconProps } from './IconTypes';

const DocumentIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 6 4 v 16 h 12 v -12 l -4 -4 h -8 z" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M 13 4 v 4 h 4" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinejoin="round" />
    <line x1="10" x2="14" y1="12" y2="12" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinecap="round" />
    <line x1="10" x2="13" y1="16" y2="16" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinecap="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default DocumentIcon;
