type IconProps = {
  className?: string,
  color?: string,
  grid?: boolean,
}

export type { IconProps };