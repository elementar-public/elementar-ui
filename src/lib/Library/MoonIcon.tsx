import React from 'react';
import { IconProps } from './IconTypes';

const MoonIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <path d="M 12 5.5 c -3.5 0, -6.5 2, -6.5 6.5 c 0 3.5, 2 6.5, 6.5 6.5 c 3.5 0, 6.5 -2, 6.5 -6.5 c -1 1.5, -2.5 3, -4.5 3 c -3 0, -5 -2, -5 -5 c 0 -2.5, 1 -3, 3 -4.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MoonIcon;
