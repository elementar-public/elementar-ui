import React from 'react';
import { IconProps } from './IconTypes';

const MapIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 6.5 v 13 l 5 -2 v -13 l -5 2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinejoin="bevel" />
    <path d="M 8.5 4.5 v 13 l 5 2 v -13 l -5 -2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinejoin="bevel" />
    <path d="M 13.5 6.5 v 13 l 5 -2 v -3.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinejoin="bevel" />
    <path d="M 18.5 4.5 q 3 0 3 3 q 0 2 -3 5 q -3 -3 -3 -5 q 0 -3 3 -3 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="18.5" cy="7.5" r="1.5" fill={color || 'currentColor'} stroke="none" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MapIcon;
