import React from 'react';
import { IconProps } from './IconTypes';

const BeautyCareIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="12" cy="11" r="7.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="14" y1="12" y2="7" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="10" x2="15" y1="15" y2="10" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5" x2="19" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BeautyCareIcon;
