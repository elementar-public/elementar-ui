import React from 'react';
import { IconProps } from './IconTypes';

const PartyIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <line x1="5" x2="12" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 8.5 20.5 v -6 l -3 -3 h 6 l -3 3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    <line x1="14" x2="19" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="15" x2="18" y1="9.5" y2="9.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 16.5 20.5 v -4 q -2 -0.5 -2 -2 v -8 h 4 v 8 q 0 1.5 -2 2" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    <line x1="5.5" x2="7.5" y1="2.5" y2="4.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9.5" x2="11.5" y1="4.5" y2="2.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5.5" x2="7.5" y1="8.5" y2="6.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9.5" x2="11.5" y1="6.5" y2="8.5" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PartyIcon;