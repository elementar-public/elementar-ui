import React from 'react';
import { IconProps } from './IconTypes';

const BathroomIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 13.5 h 17 v 1 q 0 4, -4 4 h -9 q -4 0, -4 -4 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="6.5" x2="6.5" y1="18" y2="21" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="17.5" x2="17.5" y1="18" y2="21" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5.5 13.5 v -7 q 0 -3, 3 -3 q 3 0, 3 3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 9.5 7.5 q 2 -3, 4 0 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9.5" x2="9.5" y1="10" y2="12" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="11.5" x2="11.5" y1="9" y2="11" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="13.5" x2="13.5" y1="10" y2="12" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BathroomIcon;
