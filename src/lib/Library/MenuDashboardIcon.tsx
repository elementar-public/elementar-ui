import React from 'react';
import { IconProps } from './IconTypes';

const MenuDashboardIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 4 20 c -1 -1, -2 -3, -2 -6 c 0 -6, 4 -10, 10 -10 c 6 0, 10 4, 10 10 c 0 3, -1 5, -2 6" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinejoin="round" />
    <line x1="12" x2="16" y1="16" y2="11" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" />
    <circle cx="6" cy="15" r="1" fill={color || 'currentColor'} />
    <circle cx="7" cy="11" r="1" fill={color || 'currentColor'} />
    <circle cx="10" cy="8" r="1" fill={color || 'currentColor'} />
    <circle cx="14" cy="8" r="1" fill={color || 'currentColor'} />
    <circle cx="18" cy="15" r="1" fill={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuDashboardIcon;
