import React from 'react';
import { IconProps } from './IconTypes';

const AddPersonIcon = ({
  className,
  color,
  grid
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="16" cy="8" r="5.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 8.5 24 c 0 -4.5, 3 -7.5, 7.5 -7.5 c 4.5 0, 7.5 3, 7.5 7.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="0" x2="9" y1="14.5" y2="14.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="4.5" x2="4.5" y1="10" y2="19" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default AddPersonIcon;
