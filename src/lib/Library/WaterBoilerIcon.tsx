import React from 'react';
import { IconProps } from './IconTypes';

const WaterBoilerIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 10.5 8.5 q -5.5 0.5 -6 6 q 0.5 5.5 7 6 q 7.5 -0.5 8 -8 q -0.5 -7.5 -9 -8 q 4 2 4 6 q 0 4 -4 6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default WaterBoilerIcon;
