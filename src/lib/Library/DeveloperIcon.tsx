import React from 'react';
import { IconProps } from './IconTypes';

const DeveloperIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <line x1="9.5" x2="9.5" y1="3" y2="20" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5" x2="18" y1="7.5" y2="7.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 4.5 12 v -4.5 l 4.5 -5 l 9.5 5 v 4" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="2.5" y="12.5" width="4" height="5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="21" y1="20.5" y2="20.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default DeveloperIcon;
