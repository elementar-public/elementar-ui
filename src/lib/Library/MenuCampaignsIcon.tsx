import React from 'react';
import { IconProps } from './IconTypes';

const MenuCampaignsIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 4 4 v 14 h 6 v 4 l 4 -4 h 6 v -14 h -14 z" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M 9 11 l 2 2 l 4 -4" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuCampaignsIcon;
