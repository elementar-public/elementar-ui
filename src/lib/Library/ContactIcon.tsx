import React from 'react';
import { IconProps } from './IconTypes';

const ContactIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 5.5 v 10 l 3 -3 h 9 v -7 h -12 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 8.5 14.5 v 1 h 9 l 3 3 v -10 h -3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5" x2="13" y1="7.5" y2="7.5" fill="none" strokeWidth="1px" stroke={color || 'currentColor'} />
    <line x1="5" x2="10" y1="9.5" y2="9.5" fill="none" strokeWidth="1px" stroke={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default ContactIcon;
