import React from 'react';
import { IconProps } from './IconTypes';

const ShareIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="18" cy="6" r="2.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="8" x2="16" y1="11" y2="7" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="6" cy="12" r="2.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="8" x2="16" y1="13" y2="17" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="18 " cy="18" r="2.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default ShareIcon;
