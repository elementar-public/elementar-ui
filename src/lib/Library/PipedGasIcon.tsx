import React from 'react';
import { IconProps } from './IconTypes';

const PipedGasIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <rect x="8.5" y="14.5" width="7" height="5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="9" y1="15.5" y2="15.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="9" y1="18.5" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="15" x2="21" y1="15.5" y2="15.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="15" x2="21" y1="18.5" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="12" x2="14" y1="8" y2="6" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="12" cy="8" r="4.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PipedGasIcon;
