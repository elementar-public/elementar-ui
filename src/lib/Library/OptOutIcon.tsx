import React from 'react';
import { IconProps } from './IconTypes';

const OptOutIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="12" cy="12" r="8.5" fill="none" strokeWidth="1px" stroke={color || 'currentColor'} />
    <rect x="9.5" y="7.5" width="5" height="9" fill="none" stroke="black" strokeWidth="1" />
    <line x1="11" x2="13" y1="8.5" y2="8.5" fill="none" strokeWidth="1px" stroke={color || 'currentColor'} />
    <line x1="6" x2="18" y1="6" y2="18" fill="none" strokeWidth="1px" stroke={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default OptOutIcon;
