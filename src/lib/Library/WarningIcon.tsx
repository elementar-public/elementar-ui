import React from 'react';
import { IconProps } from './IconTypes';

const WarningIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 12 4.5 l 8.5 15 h -17 l 8.5 -15 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />
    <rect rx="1" width="2" height="4" x="11" y="11" fill={color || 'currentColor'} stroke="none" />
    <rect rx="1" width="2" height="2" x="11" y="16" fill={color || 'currentColor'} stroke="none" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default WarningIcon;
