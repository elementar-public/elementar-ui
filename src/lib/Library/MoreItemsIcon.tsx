import React from 'react';
import { IconProps } from './IconTypes';

const MoreItemsIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <line x1="6" x2="18" y1="9" y2="9" stroke={color || 'currentColor'} strokeWidth="2px" />
    <line x1="6" x2="14" y1="13" y2="13" stroke={color || 'currentColor'} strokeWidth="2px" />
    <line x1="6" x2="14" y1="17" y2="17" stroke={color || 'currentColor'} strokeWidth="2px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MoreItemsIcon;
