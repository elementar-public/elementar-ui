import React from 'react';
import { IconProps } from './IconTypes';

const LaundryIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 8.5 l 2 12 h 13 l 2 -12" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 6 12.5 q 2 0 3 -2 q 1 2 3 2 q 2 0 3 -2 q 1 2 3 2" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinejoin="bevel" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default LaundryIcon;
