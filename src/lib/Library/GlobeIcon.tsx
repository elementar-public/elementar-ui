import React from 'react';
import { IconProps } from './IconTypes';

const GlobeIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <circle cx="11.5" cy="11.5" r="8" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="5" x2="18" y1="7.5" y2="7.5" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="3" x2="20" y1="11.5" y2="11.5" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="5" x2="18" y1="15.5" y2="15.5" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="11.5" x2="11.5" y1="3" y2="20" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 11.5 3 c -5.5 6, -5.5 11, 0 17" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 11.5 3 c 5.5 6, 5.5 11, 0 17" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default GlobeIcon;
