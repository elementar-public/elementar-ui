import React from 'react';
import { IconProps } from './IconTypes';

const HelpIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="12" cy="12" r="10.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <circle cx="12" cy="6" r="1" fill={color || 'currentColor'} stroke="none" />
    <path d="M 9, 9.5 h 3.5 v 7.5 h 3.5 h -7" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default HelpIcon;
