import React from 'react';
import { IconProps } from './IconTypes';

const LogoutIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 12 8 v -4 h -8 v 16 h 8 v -4" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" />
    <line x1="10" x2="20" y1="12" y2="12" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinecap="round" />
    <path d="M 16 8 l 4 4 l -4 4" fill="none" stroke={color || 'currentColor'} strokeWidth="2" strokeLinejoin="round" strokeLinecap="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default LogoutIcon;
