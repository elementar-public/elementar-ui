import React from 'react';
import { IconProps } from './IconTypes';

const SunIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <circle cx="11.5" cy="11.5" r="4" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="11.5" x2="11.5" y1="3" y2="6" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="5" x2="7" y1="5" y2="7" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="3" x2="6" y1="11.5" y2="11.5" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="5" x2="7" y1="18" y2="16" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="11.5" x2="11.5" y1="17" y2="20" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="16" x2="18" y1="16" y2="18" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="17" x2="20" y1="11.5" y2="11.5" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="16" x2="18" y1="7" y2="5" stroke={color || 'currentColor'} strokeWidth="1px" />
    

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default SunIcon;
