import React from 'react';
import { IconProps } from './IconTypes';

const DeleteIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <line x1="5" x2="19" y1="6.5" y2="6.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 9.5 6 v -2.5 h 5 v 2.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    <line x1="10.5" x2="10.5" y1="8" y2="18" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="13.5" x2="13.5" y1="8" y2="18" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 7.5 8 v 10.5 q 0 2 2 2 h 5 q 2 0 2 -2 v -10.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default DeleteIcon;
