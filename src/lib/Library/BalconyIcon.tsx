import React from 'react';
import { IconProps } from './IconTypes';

const BalconyIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <line x1="2" x2="22" y1="8.5" y2="8.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="4.5" x2="4.5" y1="8" y2="19" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9.5" x2="9.5" y1="8" y2="19" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="14.5" x2="14.5" y1="8" y2="19" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="19.5" x2="19.5" y1="8" y2="19" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 3.5 18.5 h 17 q 0 2 -2 2 h -13 q -2 0 -2 -2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BalconyIcon;
