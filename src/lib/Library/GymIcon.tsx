import React from 'react';
import { IconProps } from './IconTypes';

const GymIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 16.5 l 4 4 l 2 -2 l -4 -4 l -2 2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 3.5 12.5 l 8 8 l 2 -2 l -8 -8 l -2 2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 16.5 3.5 l -2 2 l 4 4 l 2 -2 l -4 -4 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 12.5 3.5 l -2 2 l 8 8 l 2 -2 l -8 -8 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="4" x2="5.5" y1="20" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="18.5" x2="20" y1="5.5" y2="4" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9.5" x2="14.5" y1="14.5" y2="9.5" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default GymIcon;
