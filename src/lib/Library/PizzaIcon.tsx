import React from 'react';
import { IconProps } from './IconTypes';

const PizzaIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 11.5 12.5 v -6 c -4 0 -6 2 -6 6 c 0 4 2 6 6 6 c 4 0 6 -2 6 -6 h -6 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 13.5 10.5 v -6 c 4 0 6 2 6 6 h -6 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="16.5" cy="7.5" r="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="8.5" cy="10.5" r="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="9.5" cy="14.5" r="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="13.5" cy="15.5" r="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PizzaIcon;
