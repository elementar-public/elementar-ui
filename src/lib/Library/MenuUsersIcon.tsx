import React from 'react';
import { IconProps } from './IconTypes';

const MenuUsersIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <circle cx="12" cy="6" r="4" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" />
    <path d="M 6 20 v -3 c 0 -1.5, 1.5 -3, 3 -3 h 6 c 1.5 0, 3 1.5, 3 3 v 3" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuUsersIcon;
