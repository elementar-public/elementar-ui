import React from 'react';
import { IconProps } from './IconTypes';

const MenuSalesIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <circle cx="8" cy="8" r="2" fill={color || 'currentColor'} />
    <path d="M 2 2 v 10 l 10 10 l 10 -10 l -10 -10 h -10 z" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinejoin="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuSalesIcon;
