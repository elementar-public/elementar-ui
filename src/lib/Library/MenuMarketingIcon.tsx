import React from 'react';
import { IconProps } from './IconTypes';

const MenuMarketingIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <path d="M 16 7 v 10 l -5 -2 h -5 c -2.5 0, -4 -1.5, -4 -4 c 0 -2.5, 1.5 -4, 4 -4 h 5 l 5 -2" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <line x1="7" x2="7" y1="7" y2="19" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <line x1="11" x2="11" y1="7" y2="15" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <line x1="19" x2="22" y1="7" y2="6" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <line x1="19" x2="22" y1="11" y2="11" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <line x1="19" x2="22" y1="15" y2="16" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuMarketingIcon;
