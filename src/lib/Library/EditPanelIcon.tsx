import React from 'react';
import { IconProps } from './IconTypes';

const EditPanelIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M14,4.5 h-9.5 v15 h15 v-9.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M9.5,14.5 h2 l10,-10 l-2,-2 l-10,10 v2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <line x1="10" x2="12" y1="12" y2="14" stroke={color || 'currentColor'} strokeWidth="1px" />
    <rect x="0" y="0" width="24" height="24" fill="none" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default EditPanelIcon;
