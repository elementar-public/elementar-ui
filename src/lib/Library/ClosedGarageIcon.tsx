import React from 'react';
import { IconProps } from './IconTypes';

const ClosedGarageIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 20.5 v -12 l 8.5 -4 l 8.5 4 v 12 h -17 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="5.5" y="10.5" width="13" height="10" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="17" y1="12.5" y2="12.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="17" y1="14.5" y2="14.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="17" y1="16.5" y2="16.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="17" y1="18.5" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default ClosedGarageIcon;
