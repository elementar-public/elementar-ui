import React from 'react';
import { IconProps } from './IconTypes';

const GamesRoomIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="6" cy="9" r="2.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 2.5 20.5 v -3 h 1 l 1 -6 h 3 l 1 6 h 1 v 3 h -5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 11.5 20.5 v -4 h 2 l 1 -6 l -3 -1 v -5 h 2 v 2 h 2 v -2 h 2 v 2 h 2 v -2 h 2 v 5 l -3 1 l 1 6 h 2 v 4 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="2" x2="8" y1="17.5" y2="17.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="12" x2="20" y1="16.5" y2="16.5" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default GamesRoomIcon;
