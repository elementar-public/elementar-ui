import React from 'react';
import { IconProps } from './IconTypes';

const UserIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <circle cx="12" cy="8" r="3.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 6.5 19.5 c 0 -2.5, 2.5 -5.5, 5.5 -5.5 c 2.5 0, 5.5 2.5, 5.5 5.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default UserIcon;
