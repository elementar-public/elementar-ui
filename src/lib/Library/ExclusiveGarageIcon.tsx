import React from 'react';
import { IconProps } from './IconTypes';

const ExclusiveGarageIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="7" cy="18" r="1.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="17" cy="18" r="1.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 4 18.5 h -0.5 q -2 0 -2 -2 v -2 l 6 -1 l 4 -3 h 9 l 2 3 v 3 q 0 2 -2 2 h -0.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="10" x2="14" y1="18.5" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="21" y1="14.5" y2="14.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="17.5" x2="17.5" y1="12" y2="14" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="2" x2="22" y1="5.5" y2="5.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="2.5" x2="2.5" y1="3" y2="8" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="21.5" x2="21.5" y1="3" y2="8" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default ExclusiveGarageIcon;
