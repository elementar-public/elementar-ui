import React from 'react';
import { IconProps } from './IconTypes';

const MessageIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <rect width="17" height="11" x="3.5" y="6.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 3.5 7.5 l 8.5 6.5 l 8.5 -6.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MessageIcon;
