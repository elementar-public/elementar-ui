import React from 'react';
import { IconProps } from './IconTypes';

const CinemaRoomIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <rect x="4.5" y="9.5" width="15" height="8" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="4.5" y="6.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="7.5" y="6.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="10.5" y="6.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="13.5" y="6.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="16.5" y="6.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="4.5" y="17.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="7.5" y="17.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="10.5" y="17.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="13.5" y="17.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="16.5" y="17.5" width="3" height="3" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default CinemaRoomIcon;
