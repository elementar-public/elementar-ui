import React from 'react';
import { IconProps } from './IconTypes';

const PhoneIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 5.5 q 0 15 15 15 q 2 0, 2 -2 v -4 h -5 q -1 0 -2 2 q -5 -1, -6 -6 q 2 -1, 2 -2 v -5 h -4 q -2 0 -2 2 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 12.5 3.5 v 8 l 3 -3 h 5 v -5 h -8 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PhoneIcon;
