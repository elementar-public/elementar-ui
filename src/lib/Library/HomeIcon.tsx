import React from 'react';
import { IconProps } from './IconTypes';

const HomeIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 5.5 11.5 v 9 h 13 v -9" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 2.5 13.5 l 9.5 -9.5 l 9.5 9.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="9.5" y="11.5" width="5" height="9" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default HomeIcon;
