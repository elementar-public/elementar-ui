import React from 'react';
import { IconProps } from './IconTypes';

const SportsCourtIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <rect x="3.5" y="7.5" width="17" height="13" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="1.5" y="11.5" width="2" height="5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="20.5" y="11.5" width="2" height="5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 3.5 9.5 h 1 q 3 0 3 3 v 3 q 0 3 -3 3 h -1 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 20.5 9.5 h -1 q -3 0 -3 3 v 3 q 0 3 3 3 h 1 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="12" cy="14" r="2.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default SportsCourtIcon;
