import React from 'react';
import { IconProps } from './IconTypes';

const SpinnerIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <line x1="11.5" x2="11.5" y1="3" y2="8" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.875s" repeatCount="indefinite"></animate>
    </line>
    <line x1="5" x2="9" y1="5" y2="9" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s" repeatCount="indefinite"></animate>
    </line>
    <line x1="3" x2="8" y1="11.5" y2="11.5" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.625s" repeatCount="indefinite"></animate>
    </line>
    <line x1="5" x2="9" y1="18" y2="14" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>
    </line>
    <line x1="11.5" x2="11.5" y1="15" y2="20" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.375s" repeatCount="indefinite"></animate>
    </line>
    <line x1="14" x2="18" y1="14" y2="18" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s" repeatCount="indefinite"></animate>
    </line>
    <line x1="15" x2="20" y1="11.5" y2="11.5" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.125s" repeatCount="indefinite"></animate>
    </line>
    <line x1="14" x2="18" y1="9" y2="5" stroke={color || 'currentColor'} strokeWidth="1px">
      <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>
    </line>

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default SpinnerIcon;
