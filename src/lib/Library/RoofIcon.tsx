import React from 'react';
import { IconProps } from './IconTypes';

const RoofIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3 9 c 4 0 6 -2 6 -6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3.5" x2="3.5" y1="11" y2="14" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="11" x2="14" y1="3.5" y2="3.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="11" y1="9" y2="11" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="4.5" x2="4.5" y1="20" y2="22" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="8.5" x2="8.5" y1="20" y2="22" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="12.5" x2="12.5" y1="20" y2="22" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="16.5" x2="16.5" y1="20" y2="22" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="20.5" x2="20.5" y1="13" y2="22" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="21" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 4 16.5 q 1 2 2.5 2 q 1.5 0 2.5 -2 q 1 2 2.5 2 q 1.5 0 2.5 -2 q 1 2 2.5 2 q 1.5 0 2.5 -2" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinejoin="bevel" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default RoofIcon;
