import React from 'react';
import { IconProps } from './IconTypes';

const MenuOpsIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3 12 h 2 l 2 -4 l 2 8 l 2 -4 h 2 l 2 -4 l 2 8 l 2 -4 h 2" fill="none" strokeWidth="2px" stroke={color || 'currentColor'} strokeLinejoin="round" />
    <rect x="4" y="5" width="16" height="14" rx="2" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuOpsIcon;
