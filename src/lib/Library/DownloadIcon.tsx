import React from 'react';
import { IconProps } from './IconTypes';

const DownloadIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <line x1="11.5" x2="11.5" y1="4" y2="16" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />
    <line x1="5" x2="18" y1="18.5" y2="18.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />
    <path d="M 5.5 10.5 l 6 6 l 6 -6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default DownloadIcon;
