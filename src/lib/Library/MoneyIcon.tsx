import React from 'react';
import { IconProps } from './IconTypes';

const MoneyIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="11.5" cy="11.5" r="9" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="11.5" cy="11.5" r="7" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="11.5" x2="11.5" y1="6" y2="17" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 14 7.5 h -3.5 q -2 0 -2 2 q 0 2 2 2 h 2 q 2 0 2 2 q 0 2 -2 2 h -3.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MoneyIcon;
