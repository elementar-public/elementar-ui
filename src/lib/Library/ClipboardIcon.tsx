import React from 'react';
import { IconProps } from './IconTypes';

const ClipboardIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <rect width="100%" height="100%" fill="none" />
    <path d="M 4.5 4 v 17.5 h 12 l 3 -3 v -14.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 7.5 5.5 h 9 v -1 q 0 -2 -2 -2 h -5 q -2 0 -2 2 v 1 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="17" y1="11.5" y2="11.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="17" y1="14.5" y2="14.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="15" y1="17.5" y2="17.5" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default ClipboardIcon;
