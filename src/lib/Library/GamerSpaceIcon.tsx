import React from 'react';
import { IconProps } from './IconTypes';

const GamerSpaceIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 7.5 19.5 c -2.5 0 -4 -1.5 -4 -4 c 0 -2.5 1.5 -4 4 -4 h 9 c 2.5 0 4 1.5 4 4 c 0 2.5 -1.5 4 -4 4 h -9 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="13.5" cy="15.5" r="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="17.5" cy="15.5" r="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5" x2="10" y1="15.5" y2="15.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7.5" x2="7.5" y1="13" y2="18" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 10.5 11.5 c 0 -5 4 -4 4 -9" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default GamerSpaceIcon;
