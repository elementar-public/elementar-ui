import React from 'react';
import { IconProps } from './IconTypes';

const CalendarIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <rect x="3.5" y="4.5" width="16" height="15" fill="none" rx="2" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5" x2="18" y1="7.5" y2="7.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="8.5" x2="8.5" y1="3" y2="6" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="14.5" x2="14.5" y1="3" y2="6" stroke={color || 'currentColor'} strokeWidth="1" />

    <rect x="11" y="9" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="13" y="9" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="15" y="9" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="17" y="9" width="1" height="1" stroke="none" fill={color || 'currentColor'} />

    <rect x="5" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="7" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="9" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="11" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="13" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="15" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="17" y="11" width="1" height="1" stroke="none" fill={color || 'currentColor'} />

    <rect x="5" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="7" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="9" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="11" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="13" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="15" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="17" y="13" width="1" height="1" stroke="none" fill={color || 'currentColor'} />

    <rect x="5" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="7" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="9" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="11" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="13" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="15" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="17" y="15" width="1" height="1" stroke="none" fill={color || 'currentColor'} />

    <rect x="5" y="17" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="7" y="17" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="9" y="17" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="11" y="17" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="13" y="17" width="1" height="1" stroke="none" fill={color || 'currentColor'} />
    <rect x="15" y="17" width="1" height="1" stroke="none" fill={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default CalendarIcon;
