import React from 'react';
import { IconProps } from './IconTypes';

const PhotoIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <rect x="3.5" y="5.5" width="15" height="13" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 3.5 16.5 l 4 -4 l 4 4" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 18.5 13.5 l -4 -4 l -5 5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="9" cy="9" r="1.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 4.5 20.5 h 16 v -14" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default PhotoIcon;
