import React from 'react';
import { IconProps } from './IconTypes';

const AirConditionerIcon = ({
  className,
  color,
  grid
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 5 7.5 c 5 -4 9 4 14 0" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5 10.5 c 5 -4 9 4 14 0" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5 13.5 c 5 -4 9 4 14 0" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5 16.5 c 5 -4 9 4 14 0" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default AirConditionerIcon;
