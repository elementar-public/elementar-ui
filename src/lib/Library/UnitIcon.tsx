import React from 'react';
import { IconProps } from './IconTypes';

const UnitIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 3.5 l 17 17 h -17 v -17 z z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 7.5 12.5 l 4 4 h -4 v -4 z z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    
    <line x1="3" x2="6" y1="8.5" y2="8.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="6" y1="10.5" y2="10.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="6" y1="12.5" y2="12.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="6" y1="14.5" y2="14.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="6" y1="16.5" y2="16.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="6" y1="18.5" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default UnitIcon;
