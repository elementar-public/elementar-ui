import React from 'react';
import { IconProps } from './IconTypes';

const BuildingsIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <rect x="4.5" y="5.5" width="7" height="15" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="7" y="8" width="2" height="2" fill={color || 'currentColor'} />
    <rect x="7" y="12" width="2" height="2" fill={color || 'currentColor'} />
    <rect x="7" y="16" width="2" height="4" fill={color || 'currentColor'} />

    <rect x="11.5" y="1.5" width="7" height="19" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="14" y="4" width="2" height="2" fill={color || 'currentColor'} />
    <rect x="14" y="8" width="2" height="2" fill={color || 'currentColor'} />
    <rect x="14" y="12" width="2" height="2" fill={color || 'currentColor'} />
    <rect x="14" y="16" width="2" height="4" fill={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BuildingsIcon;