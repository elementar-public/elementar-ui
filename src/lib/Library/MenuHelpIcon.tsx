import React from 'react';
import { IconProps } from './IconTypes';

const MenuHelpIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="12" cy="12" r="9" fill="none" strokeWidth="2px" stroke={color || 'currentColor'} />
    <circle cx="12" cy="8.5" r="1.5" fill={color || 'currentColor'} />
    <rect x="11" y="12" width="2" height="5" fill={color || 'currentColor'} />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuHelpIcon;
