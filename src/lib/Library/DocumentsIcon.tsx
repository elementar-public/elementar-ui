import React from 'react';
import { IconProps } from './IconTypes';

const DocumentsIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 7.5 3.5 v 15 h 11 v -10 l -5 -5 h -6 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M 7.5 5.5 h -2 v 15 h 11 v -2" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" strokeLinejoin="round" />
    <path d="M 13.5 3.5 v 5 h 5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinejoin="round" />
    <line x1="10" x2="16" y1="11.5" y2="11.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />
    <line x1="10" x2="16" y1="13.5" y2="13.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />
    <line x1="10" x2="14" y1="15.5" y2="15.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" strokeLinecap="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default DocumentsIcon;
