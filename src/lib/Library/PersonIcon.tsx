import React from 'react';
import { IconProps } from './IconTypes';

const PersonIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="12" cy="8" r="5.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 4.5 24 c 0 -4.5, 3 -7.5, 7.5 -7.5 c 4.5 0, 7.5 3, 7.5 7.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
  </svg>
);

export default PersonIcon;
