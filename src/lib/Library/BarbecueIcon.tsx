import React from 'react';
import { IconProps } from './IconTypes';

const BarbecueIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <line x1="5" x2="19" y1="8.5" y2="8.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3" x2="5" y1="10.5" y2="10.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5.5 10.5 q 0.5 6 6.5 6.5 q 6 -0.5 6.5 -6.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="8" y1="17" y2="21" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="15" x2="16" y1="17" y2="21" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7.5" x2="7.5" y1="4" y2="7" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="10.5" x2="10.5" y1="3" y2="6" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="13.5" x2="13.5" y1="4" y2="7" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="16.5" x2="16.5" y1="3" y2="6" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BarbecueIcon;
