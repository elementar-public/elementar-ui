import React from 'react';
import { IconProps } from './IconTypes';

const ExpandIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 5.5 9.5 v -4 h 4" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 18.5 9.5 v -4 h -4" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 5.5 14.5 v 4 h 4" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 18.5 14.5 v 4 h -4" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5.5" x2="9.5" y1="5.5" y2="9.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5.5" x2="9.5" y1="18.5" y2="14.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="18.5" x2="14.5" y1="5.5" y2="9.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="18.5" x2="14.5" y1="18.5" y2="14.5" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default ExpandIcon;
