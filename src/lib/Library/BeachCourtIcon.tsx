import React from 'react';
import { IconProps } from './IconTypes';

const BeachCourtIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3 9 c 4 0 6 -2 6 -6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="3.5" x2="3.5" y1="11" y2="14" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="11" x2="14" y1="3.5" y2="3.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="10.5" y1="9" y2="10.5" stroke={color || 'currentColor'} strokeWidth="1" />

    <circle cx="16" cy="14" r="4.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 15.5 9.5 c 2.5 4, 2.5 5, 0 9" fill="none" stroke={color || 'currentColor'} strokeWidth="1px" />
    <path d="M 2.5 20.5 l 2 -1 l 2 1 l 2 -1 l 2 1 l 2 -1 l 2 1 l 2 -1 l 2 1 l 2 -1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BeachCourtIcon;

// c 5.5 6, 5.5 11, 0 17