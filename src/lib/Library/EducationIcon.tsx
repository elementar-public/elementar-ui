import React from 'react';
import { IconProps } from './IconTypes';

const EducationIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 12 5.5 l 8 4 l -8 4 l -8 -4 l 8 -4 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 7.5 11 v 6 l 4.5 2 l 4.5 -2 v -6" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="19.5" x2="19.5" y1="10" y2="16" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default EducationIcon;
