import React from 'react';
import { IconProps } from './IconTypes';

const HeartIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 12 20 l 6.5 -6.5 c 1 -1, 2 -2, 2 -4.5 c 0 -2.5, -1.75 -4.5, -4.25 -4.5 c -2 0, -4 2, -4.25 4.5 c -0.25, -2.5, -2.25, -4.5, -4.25 -4.5 c -2.5 0, -4.25 2, -4.25 4.5 c 0 2.5, 1 3.5, 2 4.5 l 6.5 6.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default HeartIcon;
