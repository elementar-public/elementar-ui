import React from 'react';
import { IconProps } from './IconTypes';

const WhatsappIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 3.5 12 c 0 -5, 3.5 -8.5, 8.5 -8.5 c 5 0, 8.5 3.5, 8.5 8.5 c 0 5, -3.5 8.5, -8.5 8.5 c -2 0, -3.5 0, -4.5 -1 l -4 1 l 1 -4 c -1 -1, -1 -2.5, -1 -4.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 8.5 7.5 h 1.5 l 0.5 2 l -1 1 q 0.5 3.5, 4 4 l 1 -1 l 2 0.5 v 1.5 l -1.5 1 q -7.5 0, -7.5 -7.5 l 1 -1.5 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default WhatsappIcon;
