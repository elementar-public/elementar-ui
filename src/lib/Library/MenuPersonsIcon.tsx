import React from 'react';
import { IconProps } from './IconTypes';

const MenuPersonsIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <circle cx="7" cy="4" r="3" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" />
    <path d="M 4 21 v -6 h -1 v -8 h 8 v 8 h -1 v 6 z" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />
    <circle cx="16" cy="4" r="3" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" />
    <path d="M 13 21 v -6 h -1 v -8 h 8 v 8 h -1 v 6 z" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" strokeLinejoin="round" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuPersonsIcon;
