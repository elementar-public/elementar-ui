import React from 'react';
import { IconProps } from './IconTypes';

const LoadingIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg className={className} width="24" height="24" viewBox="0 0 24 24">
    <rect x="4" y="8" width="4" height="8" rx="1" fill={color || 'currentColor'}>
      <animate
        attributeName="height"
        begin="0.5s"
        dur="1s"
        values="12;11;10;9;8;7;6;5;4;16;12"
        calcMode="linear"
        repeatCount="indefinite"
      />
      
      <animate
        attributeName="y"
        begin="0.5s"
        dur="1s"
        values="6;6.5;7;7.5;8;8.5;9;9.5;10;4;6"
        calcMode="linear"
        repeatCount="indefinite"
      />
    </rect>

    <rect x="10" y="6" width="4" height="12" rx="1" fill={color || 'currentColor'}>
      <animate
        attributeName="height"
        begin="0.25s"
        dur="1s"
        values="12;11;10;9;8;7;6;5;4;16;12"
        calcMode="linear"
        repeatCount="indefinite"
      />
      
      <animate
        attributeName="y"
        begin="0.25s"
        dur="1s"
        values="6;6.5;7;7.5;8;8.5;9;9.5;10;4;6"
        calcMode="linear"
        repeatCount="indefinite"
      />
    </rect>

    <rect x="16" y="4" width="4" height="16" rx="1" fill={color || 'currentColor'}>
      <animate
        attributeName="height"
        begin="0s"
        dur="1s"
        values="12;11;10;9;8;7;6;5;4;16;12"
        calcMode="linear"
        repeatCount="indefinite"
      />
      
      <animate
        attributeName="y"
        begin="0s"
        dur="1s"
        values="6;6.5;7;7.5;8;8.5;9;9.5;10;4;6"
        calcMode="linear"
        repeatCount="indefinite"
      />
    </rect>
    <rect x="0" y="0" width="100%" height="100%" fill="none" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default LoadingIcon;
