import React from 'react';
import { IconProps } from './IconTypes';

const MenuTasksIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg viewBox="0 0 24 24" width="24" height="24" className={className}>
    <path d="M 2 4 v 6 h 6" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" />
    <path d="M 4 3 l 3 3 l 5 -5" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" />
    <line x1="14" x2="20" y1="8" y2="8" stroke={color || 'currentColor'} strokeWidth="2px" />

    <path d="M 2 16 v 6 h 6" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" />
    <path d="M 4 15 l 3 3 l 5 -5" fill="none" stroke={color || 'currentColor'} strokeWidth="2px" strokeLinecap="round" />
    <line x1="14" x2="22" y1="20" y2="20" stroke={color || 'currentColor'} strokeWidth="2px" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default MenuTasksIcon;
