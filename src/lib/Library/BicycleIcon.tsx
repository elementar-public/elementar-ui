import React from 'react';
import { IconProps } from './IconTypes';

const BicycleIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="7" cy="16" r="3.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <circle cx="17" cy="16" r="3.5" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 6.5 15.5 l 5 1 l 2 -5 h -4 l -3 4 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 12.5 9.5 h 3 l 2 7" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="7" x2="10" y1="9.5" y2="9.5" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BicycleIcon;
