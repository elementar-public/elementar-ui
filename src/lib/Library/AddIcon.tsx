import React from 'react';
import { IconProps } from './IconTypes';

const AddIcon = ({
  className,
  color,
  grid
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <circle cx="12" cy="12" r="8" fill="none" stroke={color || 'currentColor'} strokeWidth="2" />
    <line x1="12" x2="12" y1="7" y2="17" stroke={color || 'currentColor'} strokeWidth="2" />
    <line x1="7" x2="17" y1="12" y2="12" stroke={color || 'currentColor'} strokeWidth="2" />

    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default AddIcon;
