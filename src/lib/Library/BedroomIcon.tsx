import React from 'react';
import { IconProps } from './IconTypes';

const BedroomIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <line x1="3" x2="8" y1="20.5" y2="20.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="5.5" x2="5.5" y1="8" y2="21" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 3.5 8.5 h 4 l -1 -4 h -2 l -1 4 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <path d="M 9.5 21 v -10 q 5.5 -4, 11 0 v 10" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="21" y1="15.5" y2="15.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="9" x2="21" y1="18.5" y2="18.5" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="12.5" y="12.5" width="5" height="3" rx="1" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <rect x="0" y="0" width="24" height="24" fill="none" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default BedroomIcon;
