import React from 'react';
import { IconProps } from './IconTypes';

const SpaIcon = ({
  className,
  color,
  grid,
}: IconProps) => (
  <svg width="24" height="24" viewBox="0 0 24 24" className={className}>
    <path d="M 12.5 17.5 c -6.5 -4 -6.5 -10 0 -14 c 6.5 4 6.5 10 0 14 z" fill="none" stroke={color || 'currentColor'} strokeWidth="1" />
    <line x1="12.5" x2="12.5" y1="8" y2="21" stroke={color || 'currentColor'} strokeWidth="1" />
    
    { grid &&
      <g opacity="0.2">
        <rect x="0.5" y="0.5" width="23" height="23" fill="none" stroke="black" strokeWidth="1" />
        <rect x="3.5" y="3.5" width="17" height="17" fill="none" stroke="black" strokeWidth="1" />
        <circle cx="12" cy="12" r="11.5" fill="none" stroke="black" strokeWidth="1" />
      </g>
    }
  </svg>
);

export default SpaIcon;
