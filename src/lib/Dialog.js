import React, { useEffect, useRef } from 'react';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import Theme from './Theme';
import CheckCircleIcon from './Icons/CheckCircle';
import InfoIcon from './Icons/Info';
import WarningIcon from './Icons/Warning';
import ErrorIcon from './Icons/Error';
import ButtonLoading from './Icons/ButtonLoading';

const DialogBox = styled.div`
  background: white;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  border-radius: 4px;
  padding: 0;
  width: 300px;
  max-height: 400px;
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(${({active}) => active ? '1, 1' : '0.8, 0.8'});
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-content: stretch;
  transition: all 0.25s ease-in-out;
`;

const DialogBoxHead = styled.div`
  padding: 20px;
  display: flex;
  flex-direction: column;
`;

const DialogBoxEmoji = styled.span`
  display: block;
  text-align: center;
  font-size: 72px;
  pointer-events: none;
  user-select: none;
`;

const DialogBoxMessage = styled.p`
  margin: 10px 0;
`;

const DialogBoxOptions = styled.ul`
  display: flex;
  flex-direction: ${({elements}) => elements > 2 ? 'column' : 'row-reverse'};
  list-style: none;
  margin: 0;
  padding: 0;
`;

const DialogBoxOption = styled.li`
  flex: 0 1 auto;
  border-top: 1px solid #DDD;
  width: 100%;
`;

const DialogBoxOptionAction = css`
  display: block;
  color: ${({theme}) => theme.colors.text.regular};
  font-family: ${({theme}) => theme.typography.fontFamily};
  background: white;
  border: none;
  font-size: 1rem;
  padding: 0;
  margin: 0;
  text-align: center;
  width: 100%;
  height: 40px;
  line-height: 40px;
  position: relative;
  cursor: pointer;

  &:hover {
    background: #EEE;
  }
  
  &:disabled {
    opacity: 0.5;
    cursor: not-allowed;
    border-top-color: #BBB;
  }

  &.confirm {
    color: ${({theme}) => theme.colors.success.regular};
    font-weight: bold;
  }

  &.stop {
    color: ${({theme}) => theme.colors.danger.regular};
  }

  &.loading span {
    opacity: 0.4;
  }

  .loading {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    opacity: 1;
  }
`;

const DialogBoxOptionButton = styled.button`${DialogBoxOptionAction}`;
const DialogBoxOptionLink = styled(Link)`${DialogBoxOptionAction}`;

const DialogBackdrop = styled.div`
  visibility: ${({active}) => active ? 'visible' : 'hidden'};
  backface-visibility: hidden;
  opacity:  ${({active}) => active ? '1' : '0'};
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.3);
  z-index: 12;
  transition: all 0.25s ease-in-out;
`;

const DialogInfoIcon = styled(InfoIcon)`
  flex: 0 0 auto;
  margin: 0 auto;
  width: 72px;
  height: 72px;
  color: ${({theme}) => theme.colors.primary.regular};
  transform: scale(1);
`;

const DialogCheckCircleIcon = styled(CheckCircleIcon)`
  flex: 0 0 auto;
  margin: 0 auto;
  width: 72px;
  height: 72px;
  color: ${({theme}) => theme.colors.success.regular};
  transform: scale(1);
`;

const DialogWarningIcon = styled(WarningIcon)`
  flex: 0 0 auto;
  margin: 0 auto;
  width: 72px;
  height: 72px;
  color: ${({theme}) => theme.colors.warning.regular};
  transform: scale(1);
`;

const DialogErrorIcon = styled(ErrorIcon)`
  flex: 0 0 auto;
  margin: 0 auto;
  width: 72px;
  height: 72px;
  color: ${({theme}) => theme.colors.danger.regular};
  transform: scale(1);
`;

const Dialog = ({
  className,
  active,
  type,
  emoji,
  message,
  confirm,
  stop,
  dismiss,
  ...otherProps
}) => {
  const backdropEl = useRef();

  useEffect(() => {
    if (active && backdropEl.current.isConnected) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }

    return () => {
      if (active) document.body.style.overflow = 'auto';
    };
  }, [active]);

  return (
    <DialogBackdrop className={className} ref={backdropEl} active={active}>
      <DialogBox active={active} {...otherProps}>
        <DialogBoxHead>
          {emoji
            ?
            <DialogBoxEmoji role="img" aria-label="Ícone">{emoji}</DialogBoxEmoji>
            :
            (() => {
            switch (type) {
              case 'primary':
                return <DialogInfoIcon />;
              case 'success':
                return <DialogCheckCircleIcon />;
              case 'warning':
                return <DialogWarningIcon />;
              case 'danger':
                return <DialogErrorIcon />;
              default:
                return <DialogInfoIcon />;
            }
          })()}
          <DialogBoxMessage>{message}</DialogBoxMessage>
        </DialogBoxHead>

        <DialogBoxOptions elements={[confirm, stop, dismiss].filter(e => e).length}>
          { confirm &&
            <DialogBoxOption>
              {confirm.to
                ?
                  <DialogBoxOptionLink to={confirm.to} className={confirm.loading ? 'confirm loading' : 'confirm'}>
                    <span>{confirm.label || 'Confirmar'}</span>
                    {confirm.loading && <ButtonLoading className="loading" />}
                  </DialogBoxOptionLink>
                :
                  <DialogBoxOptionButton type="button" onClick={confirm.onClick} disabled={confirm.loading} className={confirm.loading ? 'confirm loading' : 'confirm'}>
                    <span>{confirm.label || 'Confirmar'}</span>
                    {confirm.loading && <ButtonLoading className="loading" />}
                  </DialogBoxOptionButton>
              }
            </DialogBoxOption>
          }
          { stop &&
            <DialogBoxOption>
              {stop.to
                ?
                  <DialogBoxOptionLink to={stop.to} className={stop.loading ? 'stop loading' : 'stop'}>
                    <span>{stop.label || 'Parar'}</span>
                    {stop.loading && <ButtonLoading className="loading" />}
                  </DialogBoxOptionLink>
                :
                  <DialogBoxOptionButton type="button" onClick={stop.onClick} disabled={stop.loading} className={stop.loading ? 'stop loading' : 'stop'}>
                    <span>{stop.label || 'Parar'}</span>
                    {stop.loading && <ButtonLoading className="loading" />}
                  </DialogBoxOptionButton>
              }
            </DialogBoxOption>
          }
          { dismiss &&
            <DialogBoxOption>
              {dismiss.to
                ?
                  <DialogBoxOptionLink to={dismiss.to} className={dismiss.loading ? 'dismiss loading' : 'dismiss'}>
                    <span>{dismiss.label || 'Cancelar'}</span>
                    {dismiss.loading && <ButtonLoading className="loading" />}
                  </DialogBoxOptionLink>
                :
                  <DialogBoxOptionButton type="button" onClick={dismiss.onClick} disabled={dismiss.loading} className={dismiss.loading ? 'dismiss loading' : 'dismiss'}>
                    <span>{dismiss.label || 'Cancelar'}</span>
                    {dismiss.loading && <ButtonLoading className="loading" />}
                  </DialogBoxOptionButton>
              }
            </DialogBoxOption>
          }
        </DialogBoxOptions>
        
        
      </DialogBox>
    </DialogBackdrop>
  )
}

export default Dialog;

Dialog.defaultProps = {
  message: 'Mensagem',
  type: 'primary',
  active: false,
  theme: {
    colors: Theme.colors
  }
}