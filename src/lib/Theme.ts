const defaultVerticalPadding = 8;
const defaultHorizontalPadding = 10;
const xsWidthBreakpoint = 576;
const smWidthBreakpoint = 768;
const mdWidthBreakpoint = 992;
const lgWidthBreakpoint = 1200;
const xlWidthBreakpoint = 1920;

const Theme = {
  colors: {
    text: {
      regular: '#333',
      titles: '#789',
      link: '#09D',
      linkHover: '#04B',
      linkActive: '#039'
    },
    default: {
      regular: '#89A',
      hover: '#789',
      active: '#678',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    primary: {
      regular: '#09D',
      hover: '#08C',
      active: '#07B',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    success: {
      regular: '#0D8',
      hover: '#0C7',
      active: '#0B6',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    warning: {
      regular: '#F80',
      hover: '#E70',
      active: '#D60',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    danger: {
      regular: '#E24',
      hover: '#D13',
      active: '#C02',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    help: {
      regular: '#B2D',
      hover: '#A1C',
      active: '#90B',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    dark: {
      regular: '#333',
      hover: '#222',
      active: '#111',
      fontColor: '#FFF',
      fontColorHover: '#FFF',
      fontColorActive: '#FFF'
    },
    dimmed: {
      regular: '#F6F8FB',
      hover: '#EAF0F6',
      active: '#CCD6DD',
      fontColor: '#89A',
      fontColorHover: '#789',
      fontColorActive: '#678'
    },
    hollow: {
      regular: '#FFF',
      hover: '#FFF',
      active: '#FFF',
      fontColor: '#89A',
      fontColorHover: '#789',
      fontColorActive: '#678'
    }
  },
  buttons: {
    borderRadius: '4px',
    sizes: {
      small: {
        regular: `${defaultVerticalPadding - 2}px ${defaultHorizontalPadding}px`,
        outline: `${defaultVerticalPadding - 3}px ${defaultHorizontalPadding - 1}px`,
        outlineHover: `${defaultVerticalPadding - 4}px ${defaultHorizontalPadding - 2}px`,
        fontSize: '0.9rem',
        lineHeight: '1rem'
      },
      default: {
        regular: `${defaultVerticalPadding}px ${defaultHorizontalPadding}px`,
        outline: `${defaultVerticalPadding - 1}px ${defaultHorizontalPadding - 1}px`,
        outlineHover: `${defaultVerticalPadding - 2}px ${defaultHorizontalPadding - 2}px`,
        fontSize: '1.2rem',
        lineHeight: '1.2rem'
      },
      large: {
        regular: `${defaultVerticalPadding}px ${defaultHorizontalPadding}px`,
        outline: `${defaultVerticalPadding - 1}px ${defaultHorizontalPadding - 1}px`,
        outlineHover: `${defaultVerticalPadding - 2}px ${defaultHorizontalPadding - 2}px`,
        fontSize: '1.4rem',
        lineHeight: '1.6rem'
      },
      xLarge: {
        regular: `${defaultVerticalPadding + 2}px ${defaultHorizontalPadding + 2}px`,
        outline: `${defaultVerticalPadding + 1}px ${defaultHorizontalPadding + 1}px`,
        outlineHover: `${defaultVerticalPadding}px ${defaultHorizontalPadding}px`,
        fontSize: '1.6rem',
        lineHeight: '1.8rem'
      }
    },
  },

  textInputs: {
    default: {
      height: '30px',
      fontSize: '0.9rem',
      borderRadius: '4px',
      padding: '6px',
      paddingWithIcon: '6px 24px 6px 6px',
    },
    medium: {
      height: '40px',
      fontSize: '1.3rem',
      borderRadius: '4px',
      padding: '8px',
      paddingWithIcon: '8px 24px 8px 8px',
    },
    large: {
      height: '50px',
      fontSize: '1.8rem',
      borderRadius: '6px',
      padding: '10px',
      paddingWithIcon: '10px 24px 10px 10px',
    },
    xLarge: {
      height: '64px',
      fontSize: '2.2rem',
      borderRadius: '8px',
      padding: '12px',
      paddingWithIcon: '12px 24px 12px 12px',
    }
  },

  selects: {
    default: {
      height: '30px',
      fontSize: '0.9rem',
      borderRadius: '2px',
      padding: '2px',
      paddingOption: '10px',
    },
    medium: {
      height: '40px',
      fontSize: '1.3rem',
      borderRadius: '4px',
      padding: '4px',
      paddingOption: '20px',
    },
    large: {
      height: '50px',
      fontSize: '1.8rem',
      borderRadius: '6px',
      padding: '6px',
      paddingOption: '30px',
    },
    xLarge: {
      height: '60px',
      fontSize: '2.2rem',
      borderRadius: '8px',
      padding: '8px',
      paddingOption: '40px',
    }
  },

  breakpoints: {
    xs: {
      min: null,
      max: (xsWidthBreakpoint - 1) + 'px',
      queryStrict: `(min-width: 0) and (max-width: ${xsWidthBreakpoint - 1}px)`,
      queryRecursive: `(min-width: 0)`,
      width: xsWidthBreakpoint + 'px'
    },
    sm: {
      min: xsWidthBreakpoint + 'px',
      max: (smWidthBreakpoint - 1) + 'px',
      queryStrict: `(min-width: ${xsWidthBreakpoint}px) and (max-width: ${smWidthBreakpoint - 1}px)`,
      queryRecursive: `(min-width: ${xsWidthBreakpoint}px)`,
      width: smWidthBreakpoint + 'px'
    },
    md: {
      min: smWidthBreakpoint + 'px',
      max: (lgWidthBreakpoint - 1) + 'px',
      queryStrict: `(min-width: ${smWidthBreakpoint}px) and (max-width: ${mdWidthBreakpoint - 1}px)`,
      queryRecursive: `(min-width: ${smWidthBreakpoint}px)`,
      width: mdWidthBreakpoint + 'px'
    },
    lg: {
      min: lgWidthBreakpoint + 'px',
      max: (xlWidthBreakpoint - 1) + 'px',
      queryStrict: `(min-width: ${mdWidthBreakpoint}px) and (max-width: ${lgWidthBreakpoint - 1}px)`,
      queryRecursive: `(min-width: ${mdWidthBreakpoint}px)`,
      width: lgWidthBreakpoint + 'px'
    },
    xl: {
      min: xlWidthBreakpoint + 'px',
      max: null,
      queryStrict: `(min-width: ${lgWidthBreakpoint}px) and (max-width: ${xlWidthBreakpoint - 1}px)`,
      queryRecursive: `(min-width: ${lgWidthBreakpoint}px)`,
      width: xlWidthBreakpoint + 'px'
    },
  },
  labels: {
    regular: {
      padding: '4px',
      roundedPadding: '4px 10px'
    },
    outline: {
      padding: '3px',
      roundedPadding: '3px 9px'
    }
  },
  typography: {
    fontFamily: '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
    fontSize: {
      default: '1rem',
      small: '0.85rem'
    }
  }
}

export default Theme;
