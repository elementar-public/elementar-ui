import React, { useRef, useCallback, useEffect } from 'react';
import styled from 'styled-components';
import CloseIcon from './Icons/Close';

const ModalBox: any = styled.div<{ width: string, minHeight: string, height: string, active: boolean, cover: boolean }>`
  background: white;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  border-radius: 4px;
  padding: 20px;
  width: 100%;
  max-width: ${({ width }) => width};
  min-width: 300px;
  min-height: ${({ minHeight }) => minHeight};
  max-height: ${({ height }) => height};
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%) scale(${({ active }) => (active) ? ('1, 1') : ('0.8, 0.8')});
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-content: stretch;
  transition: all 0.25s ease-in-out;

  @media screen and (max-width: 419px) {
    transform: none;
    border-radius: 0;
    width: 100vw;
    max-height: 100vh;
    min-width: 0;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;

    ${({ cover }) => cover && `
      height: 100%;
    `}
  }
`;

ModalBox.Content = styled.div`
  text-align: left;
  flex-grow: 1;
  overflow-y: auto;
  overflow-x: hidden;
`;

ModalBox.Head = styled.div`
  flex-grow: 0;
  flex-shrink: 0;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding-bottom: 16px;
`;

ModalBox.Title = styled.h2`
  flex: 0 1 auto;
  text-align: left;
  margin: 0;
  padding: 0;
  color: ${({ theme }) => theme.colors.text.titles};
  font-size: 1.2rem;
  line-height: 1.6rem;
  letter-spacing: -0.5px;
  min-width: 0;
`;

ModalBox.Button = styled.button`
  border: none;
  background: none;
  padding: 0;
  margin: 0;
  width: 24px;
  height: 24px;
  color: ${({ theme }) => theme.colors.text.titles};
  flex: 0 0 auto;
  opacity: 0.6;
  transition: opacity 0.25s;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }

  svg {
    display: block;
    pointer-events: none;
  }
`;

ModalBox.Backdrop = styled.div<{ active: boolean }>`
  visibility: ${({ active }) => (active) ? ('visible') : ('hidden')};
  backface-visibility: hidden;
  opacity: ${({ active }) => (active) ? ('1') : ('0')};
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.3);
  z-index: 12;
  transition: all 0.25s ease-in-out;
`;

type ModalProps = {
  className?: string,
  active: boolean,
  title?: string,
  close?: () => void,
  width?: string,
  height?: string,
  minHeight?: string,
  cover?: boolean,
  closeClassName?: string,
  children: React.ReactNode,
}

const Modal = ({
  width = '400px',
  height = '400px',
  active = false,
  title = 'Modal',
  className,
  children,
  close,
  minHeight,
  cover,
  closeClassName,
  ...otherProps
}: ModalProps) => {
  const backdropEl = useRef<HTMLDivElement>(null);

  const removeModal = () => {
    if (backdropEl.current && backdropEl.current.parentNode) {
      backdropEl.current.parentNode.removeChild(backdropEl.current);
      document.body.style.overflow = 'auto';
    }
  }

  const closeModal = useCallback(() => {
    document.body.style.overflow = 'auto';

    if (close)
      close()
    else
      removeModal();
  }, [close]);

  useEffect(() => {
    if (active && backdropEl.current?.isConnected) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  }, [active]);

  useEffect(() => {
    return () => {
      document.body.style.overflow = 'auto'
    };
  }, []);

  return (
    <ModalBox.Backdrop
      className={className}
      ref={backdropEl}
      active={active}
    >
      <ModalBox
        width={width}
        height={height}
        minHeight={minHeight}
        cover={cover}
        active={active}
        {...otherProps}
      >
        <ModalBox.Head>
          <ModalBox.Title>{title}</ModalBox.Title>
          <ModalBox.Button onClick={closeModal} className={closeClassName}>
            <CloseIcon/>
          </ModalBox.Button>
        </ModalBox.Head>

        <ModalBox.Content>
          {children}
        </ModalBox.Content>
      </ModalBox>
    </ModalBox.Backdrop>
  );
};

export default Modal;

