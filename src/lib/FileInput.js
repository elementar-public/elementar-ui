import React from 'react';
import UploadIcon from './Icons/Upload';
import LoadingSpinner from './Icons/LoadingSpinner';
import styled from 'styled-components';

const FileInputContainer = styled.div`
  width: 100%;
  margin: ${props => props.noMargin ? '0' : '0 0 12px'};
`

const FileInputBox = styled.div`
  display: flex;
  flex-direction: ${props => props.mobile ? 'row' : 'column'};
  align-items: stretch;
  justify-content: space-between;
  width: 100%;
  font-family: ${props => props.theme.typography.fontFamily};
  color: ${props => props.color ? props.theme.colors[props.color].active : props.theme.colors.text.regular};
  box-sizing: border-box;
  font-size: 1rem;
  border: none;
  border-bottom: 1px solid ${props => props.color ? props.theme.colors[props.color].regular : props.theme.colors.default.regular};
  border-radius: 0;
  padding: 0;
  background: none;
  opacity: ${({disabled}) => disabled ? '0.4' : '1'};
`

const FileInputLabel = styled.label`
  margin: ${props => props.mobile ? '0' : '0 0 4px'};
  font-weight: 500;
  font-size: 0.9rem;
  line-height: ${props => props.mobile ? '29px' : '1rem'};
  display: block;
  color: ${props => props.color ? props.theme.colors[props.color].regular : props.theme.colors.default.regular};
  ${ props => props.mobile && `
    flex: 0 0 auto;
    padding-right: 5px;
  `
  }
`

const FileInputPlace = styled.label`
  flex: 0 1 auto;
  width: auto;
  min-width: 0;
  overflow: hidden;
  display: flex;
  flex-direction: ${props => props.mobile ? 'row-reverse' : 'row' };
`

const FileInputElement = styled.input`
  display: none;
`

const FileUploadIcon = styled(UploadIcon)`
  width: 24px;
  height: 24px;
  flex-shrink: 0;
  flex-grow: 0;
  fill: ${props => props.color ? props.theme.colors[props.color].regular : props.theme.colors.default.regular};
`

const FileLoadingIcon = styled(LoadingSpinner)`
  width: 24px;
  height: 24px;
  flex-shrink: 0;
  flex-grow: 0;
  fill: ${props => props.color ? props.theme.colors[props.color].regular : props.theme.colors.default.regular};
`

const FileInputSpan = styled.div`
  margin-left: 6px;
  flex-shrink: 1;
  flex-grow: 0;
  min-width: 0;
  width: auto;
  height: 29px;
  text-align: ${props => props.mobile ? 'right' : 'left'};

  span {
    font-size: 0.9rem;
    line-height: 29px;
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    display: block;
    user-select: none;
  }
`

const FileInputHelp = styled.small`
  color: ${props => props.color === 'hollow' ? props.theme.colors.hollow.regular : props.theme.colors.default.regular};
  font-size: 0.8rem;
  line-height: 1rem;
  display: block;
  margin-top: 4px;
  opacity: 0.6;
`

const FileInput = ({
  className,
  noMargin,
  containerClassname,
  id,
  mobile,
  color,
  label,
  file,
  help,
  disabled,
  loading,
  placeholder,
  ...otherProps
}) => {
  return (
    <FileInputContainer noMargin={noMargin} className={containerClassname}>
      <FileInputBox disabled={disabled} loading={loading} mobile={mobile} color={color}>
        { label && <FileInputLabel mobile={mobile} color={color} htmlFor={id}>{label}</FileInputLabel> }
        <FileInputPlace mobile={mobile}>
          <FileInputElement type="file" disabled={disabled || loading} {...otherProps} />
          {loading ? <FileLoadingIcon color={color} /> : <FileUploadIcon color={color} />}
          <FileInputSpan mobile={mobile}>
            <span>{file ? file[0].name.substring(file[0].name.lastIndexOf("\\") + 1) : placeholder || 'Escolha um arquivo'}</span>
          </FileInputSpan>
        </FileInputPlace>
      </FileInputBox>

      { help && <FileInputHelp color={color}>{help}</FileInputHelp>}
    </FileInputContainer>
  );
}

export default FileInput;
