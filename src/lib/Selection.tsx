import React from 'react';
import styled from 'styled-components';
import Theme from './Theme';

const themes = ['default', 'primary', 'success', 'warning', 'danger'];

interface SelectionSvgProps {
  checked: boolean,
  type?: string,
  mobile?: boolean,
  loading?: boolean,
  className?: string,
  slug: string
}

const SelectionSvg = ({
  checked,
  type,
  mobile,
  loading,
  className,
  slug
}: SelectionSvgProps) => {
  switch(type) {
    case 'checkbox':
      return (
        <svg viewBox="0 0 18 18" width="18" height="18" className={className}>
          <defs>
            <mask id={`checkBoxHole-${slug}`}>
              <rect width="100%" height="100%" fill="white"/>
              <rect x="3" y="3" width="12" height="12" rx="2" fill="black" />
            </mask>
          </defs>

          <rect x="1" y="1" width="16" height="16" rx="4" mask={checked ? 'false' : `url(#checkBoxHole-${slug})`} />

          {(checked) && (
            <path d="M 3,9.5 l 4.5,4.5 l 7.5,-7.5 l -1.5,-1.5 l -6,6 l -3,-3 l -1.5,1.5 z" fill="white" stroke="none" />
          )}
        </svg>
      )

    case 'radio':
      return (
        <svg viewBox="0 0 18 18" width="18" height="18" className={className}>
          <defs>
            <mask id={`radioHole-${slug}`}>
              <rect width="100%" height="100%" fill="white" stroke="none"/>
              <circle r="6" cx="9" cy="9" fill="black" stroke="none" />
            </mask>
          </defs>

          <circle r="8" cx="9" cy="9" mask={`url(#radioHole-${slug})`} stroke="none" />

          {(checked) && (
            <circle r="3" cx="9" cy="9" />
          )}
        </svg>
      )

    default:
      if (mobile)
        return (
          <svg viewBox="0 0 48 30" width="48" height="30" className={className}>
            <defs>
              <mask id={`mobileToggleHole-${slug}`}>
                <rect width="100%" height="100%" fill="white" stroke="none" />
                <rect x="3" y="3" width="42" height="24" rx="12" fill="black" stroke="none" />
              </mask>
            </defs>

            {(loading) ? (
              <rect x="1" y="1" width="46" height="28" rx="14" mask={`url(#mobileToggleHole-${slug})`} stroke="none" />
            ) : (
              <rect x="1" y="1" width="46" height="28" rx="14" mask={checked ? 'false' : `url(#mobileToggleHole-${slug})`} stroke="none" />
            )}

            {(loading) && (
              <g>
                <rect width="4" height="4" x="16" y="13" rx="2">
                  <animate attributeName="height" values="4;12;4" dur="1.5s" repeatCount="indefinite" />
                  <animate attributeName="y" values="13;9;13" dur="1.5s" repeatCount="indefinite" />
                </rect>
                <rect width="4" height="12" x="22" y="9" rx="2">
                  <animate attributeName="height" values="12;4;12" dur="1.5s" repeatCount="indefinite" />
                  <animate attributeName="y" values="9;13;9" dur="1.5s" repeatCount="indefinite" />
                </rect>
                <rect width="4" height="4" x="28" y="13" rx="2">
                  <animate attributeName="height" values="4;12;4" dur="1.5s" repeatCount="indefinite" />
                  <animate attributeName="y" values="13;9;13" dur="1.5s" repeatCount="indefinite" />
                </rect>
              </g>
            )}

            {!(loading) && (
              (checked) ? (
                <circle r="12" cx="33" cy="15" fill="white" />
              ) : (
                <g>
                  <circle r="14" cx="15" cy="15" />
                  <circle r="12" cx="15" cy="15" fill="white" />
                </g>
              )
            )}
          </svg>
        )
      else
        return (
          <svg viewBox="0 0 32 18" width="32" height="18" className={className}>
            <defs>
              <mask id={`toggleHole-${slug}`}>
                <rect width="100%" height="100%" fill="white" stroke="none" />
                <rect x="3" y="3" width="26" height="12" rx="6" fill="black" stroke="none" />
              </mask>
            </defs>

            <rect x="1" y="1" width="30" height="16" rx="8" mask={checked ? 'false' : `url(#toggleHole-${slug})`} stroke="none" />

            {(checked) ? (
              <circle r="6" cx="23" cy="9" fill="white" />
            ) : (
              <g>
                <circle r="8" cx="9" cy="9" />
                <circle r="6" cx="9" cy="9" fill="white" />
              </g>
            )}
          </svg>
        );
  };
}

const SelectionBox: any = styled.label<{ mobile?: boolean, noMargin?: boolean, inline?: boolean }>`
  display: flex;
  gap: 4px;
  font-size: 0.9rem;
  line-height: 1rem;
  padding: 4px 0;
  position: relative;
  user-select: none;
  cursor: pointer;
  -webkit-user-select: none;
  -khtml-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;

  ${({ mobile, noMargin }) => mobile && `
    display: flex;
    flex-direction: row-reverse;
    justify-content: space-between;
    align-items: center;
    margin-bottom: ${noMargin ? '0' : '12px'};
  `}

  ${({ inline }) => inline && `
    display: inline-flex;
    margin-right: 12px;
  `}
`;

SelectionBox.Icon = styled(SelectionSvg)<{ disabled?: boolean, color?: string }>`
  flex: 0 0 auto;
  margin: 0;
  vertical-align: sub;
  transform: scale(1);
  opacity: ${({ disabled }) => disabled ? '0.4' : '1'};
  fill: ${({ theme, color }) => color ? (themes.includes(color) ? theme.colors[color].regular : color) : theme.colors.default.regular};
`;

SelectionBox.Input = styled.input`
  position: absolute;
  top: 0;
  left: 0;
  visibility: hidden;
`;

SelectionBox.Label = styled.span<{ disabled?: boolean, mobile?: boolean }>`
  opacity: ${({ disabled }) => disabled ? '0.6' : '1'};
  display: flex;
  flex-direction: column;
  gap: 2px;

  ${({ mobile }) => mobile && `
    font-weight: 500;
  `}
`;

SelectionBox.Label.Message = styled.span`
  margin: 0;
  padding: 0;
`

SelectionBox.Label.Help = styled.span`
  font-size: 0.8rem;
  opacity: 0.6;
`

interface SelectionProps {
  id?: string,
  label?: string,
  type?: string,
  name?: string,
  checked: boolean,
  color: string,
  inline?: boolean,
  disabled?: boolean,
  containerClassName?: string,
  theme?: any,
  className?: string,
  help?: string,
  onChange: (e?: any) => void,
  mobile?: boolean,
  loading?: boolean,
  noMargin?: boolean,
  [otherProps: string]: any,
}

const Selection = ({
  id,
  label = 'Opção',
  type,
  name,
  checked,
  color,
  inline,
  disabled,
  containerClassName,
  theme = {
    colors: Theme.colors
  },
  className,
  onChange,
  mobile,
  loading,
  noMargin,
  help,
  ...otherProps
}: SelectionProps) => {
  const slug = Math.random().toString().replace(/^0./, '');

  return (
    <SelectionBox
      htmlFor={id}
      inline={inline}
      className={containerClassName}
      noMargin={noMargin}
      mobile={mobile}
    >
      <SelectionBox.Icon
        type={type}
        checked={checked}
        disabled={disabled}
        loading={loading}
        color={color}
        mobile={mobile}
        slug={slug}
      />

      <SelectionBox.Input
        type={type || 'checkbox'}
        id={id}
        className={className}
        checked={checked}
        onChange={onChange}
        disabled={disabled || loading}
        name={name}
        {...otherProps}
      />

      <SelectionBox.Label
        disabled={disabled || loading}
        mobile={mobile}
      >
        <SelectionBox.Label.Message>{label}</SelectionBox.Label.Message>
        {(help) && <SelectionBox.Label.Help>{help}</SelectionBox.Label.Help>}
      </SelectionBox.Label>
    </SelectionBox>
  )
}

export default Selection;
