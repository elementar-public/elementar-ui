import React, { useState, useEffect, useCallback, useRef } from 'react';
import { Link } from 'react-router-dom';
import styled, { css } from 'styled-components';
import Theme from './Theme';

const DropdownItemSeparator = styled.hr`
  border: none;
  border-top: 1px dotted rgba(0,0,0,0.3);
  margin: 10px;
  padding: 0;
  height: 0;
`;

const DropdownItemBlock = styled.li`
  margin: 0;
  padding: 0;
`;

const DropdownItemBasicAction = ({
  className,
  children,
  disabled,
  type,
  label,
  to,
  href,
  color,
  onClick
}) => {
  if (to) {
    return <Link className={className} to={to} color={color}>{label || children}</Link>;
  } else {
    return React.createElement(
      (href ? 'a' : 'button'),
      {
        className,
        href: (href || null),
        type: (href ? null : type),
        onClick: (onClick || null),
        color,
        disabled
      },
      (label || children)
    );
  }
}

const DropdownItemActionStyle = css`
  display: block;
  width: 100%;
  box-sizing: border-box;
  text-align: left;
  font-size: 1rem;
  font-weight: none;
  font-style: normal;
  line-height: 1.2rem;
  letter-spacing: 0;
  text-decoration: none;
  color: ${({color, theme}) => color ? theme.colors[color].regular : 'inherit'};
  margin: 0;
  padding: 8px 10px;
  background: none;
  border: none;
  cursor: pointer;

  &:hover {
    background: rgba(0,0,0,0.1);
  }

  &:disabled {
    background: none;
    opacity: 0.5;
    cursor: default;
  }
`;

const DropdownItemAction = styled(DropdownItemBasicAction)`${DropdownItemActionStyle}`;

const DropdownItemActionDummy = styled.span`${DropdownItemActionStyle}`;

const DropdownItem = ({
  href,
  to,
  action,
  disabled,
  separator,
  color,
  label,
  ...otherProps
}) => (
  <DropdownItemBlock>
    {separator
      ?
      <DropdownItemSeparator />
      :
      to
        ?
        <DropdownItemAction to={to} label={label} color={color} {...otherProps} />
        :
        href
          ?
          <DropdownItemAction href={href} target="_blank" rel="noopener noreferrer" label={label} color={color} {...otherProps} />
          :
          <DropdownItemAction label={label} disabled={disabled} onClick={action || null} color={color} {...otherProps} />
    }
  </DropdownItemBlock>
);

const DropdownContainer = styled.div`
  position: relative;
  width: max-content;
  margin: 0;
  padding: 0;
`;

const DropdownMenu = styled.ul`
  display: ${({ active }) => active ? 'block' : 'none'};
  min-width: 200px;
  max-width: 300px;
  width: max-content;
  margin: ${({ up }) => up ? '0 0 4px' : '4px 0 0'};
  padding: 8px 0;
  list-style: none;
  position: absolute;
  z-index: 1;
  top: ${({ up }) => up ? 'auto' : '100%'};
  bottom: ${({ up }) => up ? '100%' : 'auto'};
  left: ${({ position }) => position === 'center' ? '50%' : (position === 'right' ? 'auto' : '0')};
  right: ${({ position }) => position === 'right' ? '0' : 'auto'};
  transform: ${({ position }) => position === 'center' ? 'translateX(-50%)' : 'none'};
  background: ${({ dark, theme }) => dark ? theme.colors.dark.regular : 'white'};
  border-radius: 4px;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1), inset 0 0 0 1px rgba(0,0,0,0.1);

  ${DropdownItemAction} {
    color: ${({ dark, theme }) => dark ? theme.colors.dark.fontColor : 'auto'};
  }
`;

const Dropdown = ({
  trigger,
  children,
  disabled,
  up,
  autoClose,
  position,
  dark,
  ...otherProps
}) => {
  const [active, toggleActive] = useState(false);
  const dropdownMenuRef = useRef(null);
  const dropdownTriggerRef = useRef(null);

  const clickFunction = () => toggleActive(false);

  const handleMenuClick = useCallback(({ target }) => {
    if (dropdownMenuRef && dropdownMenuRef.current && dropdownTriggerRef && dropdownTriggerRef.current && !dropdownMenuRef.current.contains(target) && !dropdownTriggerRef.current.contains(target) && active === true) toggleActive(false);
  }, [active]);

  useEffect(() => {
    document.addEventListener('mousedown', handleMenuClick, false);

    return () => {
      document.removeEventListener('mousedown', handleMenuClick);
    }
  });

  return (
    <DropdownContainer {...otherProps}>
      {(trigger) ? (
        React.cloneElement(trigger, {ref: dropdownTriggerRef, disabled, active, onClick: () => toggleActive(!active)})
      ) : (
        <button type="button" ref={dropdownTriggerRef} onClick={() => toggleActive(!active)}>Ações</button>
      )}

      <DropdownMenu
        up={up}
        position={position}
        dark={dark}
        active={active}
        ref={dropdownMenuRef}
        onClick={autoClose && clickFunction}
      >
        {(children) || (
          <DropdownItemBlock>
            <DropdownItemActionDummy>Nenhuma ação disponível</DropdownItemActionDummy>
          </DropdownItemBlock>
        )}
      </DropdownMenu>
    </DropdownContainer>
  )
}

export default Dropdown;

export {
  DropdownItem
}

Dropdown.defaultProps = {
  position: 'left',
  up: false,
  theme: Theme
}

DropdownItem.defaultProps = {
  label: 'Ação',
  theme: Theme
}
