import React, { useState, useEffect } from 'react';
import styled from 'styled-components';
import Theme from './Theme';

const TabContainer = styled.div`
  margin: 0;
  padding: 0;
  display: flex;
  flex-direction: column;
  height: ${props => props.height + 'px'};
  max-height: ${props => props.maxHeight + 'px'};
`;

const TabBar = styled.ul`
  display: flex;
  list-style: none;
  width: 100%;
  box-sizing: border-box;
  margin: 0;
  padding: 0;
  ${({mobile, color, align, theme}) => mobile 
  ?
    `
    border: 1px solid ${theme.colors[color].regular};
    border-radius: 2px;
    margin-bottom: 10px;
    li { flex 0 1 100%; }
    `
  :
    `
    border-bottom: 1px solid rgba(128,128,128,0.2);
    ${align === 'left'   ? `justify-content: flex-start;` : ''}
    ${align === 'center' ? `justify-content: center;` : ''}
    ${align === 'right'  ? `justify-content: flex-end;` : ''}
    ${align === 'spaced' ? `justify-content: space-around;` : ''}
    ${align === 'fixed'  ? `justify-content: space-evenly;` : ''}
    ${align === 'fill'   ? `li { flex: 0 1 100%; }` : ''}
    `
  }

`;

const TabBarButton = styled.button`
  display: block;
  box-sizing: border-box;
  width: 100%;
  background: none;
  border: none;
  font-size: 0.8rem;
  font-weight: bold;
  text-transform: uppercase;
  padding: 10px;
  cursor: pointer;
  outline: none;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;

  ${({mobile, color, current, theme}) => mobile
  ?
    `
    color: ${current ? theme.colors.hollow.regular : theme.colors[color].regular};
    background-color: ${current ? theme.colors[color].regular : theme.colors.hollow.regular};
    `
  :
    `
    margin-bottom: -2px;
    color: ${current ? theme.colors.text.regular : theme.colors.text.titles};
    border-bottom: ${current ? `2px solid ${theme.colors[color].regular}` : 'none'};
    `
  }

  &:disabled {
    opacity: 0.5;
  }
`;

const TabBox = styled.div`
  overflow-x: hidden;
  overflow-y: auto;
`;

const TabUnit = ({ children }) => children;

const Tab = ({
  children,
  height,
  maxHeight,
  color,
  align,
  mobile,
  ...otherProps
}) => {
  const [tab, toggleTab] = useState(0);

  useEffect(() => {
    if (Array.isArray(children))
      children.map((c, i) => c.type === TabUnit && c.props.active && toggleTab(i));
  }, [children]);

  return (
    <TabContainer height={height} maxHeight={maxHeight} {...otherProps}>
      <TabBar align={align} color={color} mobile={mobile}>
        {Array.isArray(children)
          ?
          children.map((c, i) => c.type === TabUnit && <li key={i}><TabBarButton type="button" current={tab === i} mobile={mobile} color={color} disabled={c.props.disabled} onClick={() => toggleTab(i)}>{c.props.title || 'Aba ' + (i+1)}</TabBarButton></li>)
          :
          <li><TabBarButton type="button" current mobile={mobile} color={color} onClick={() => null}>{children.props.title || 'Aba'}</TabBarButton></li>
        }
      </TabBar>

      <TabBox>
        { Array.isArray(children) ? children[tab] : children }
      </TabBox>
    </TabContainer>
  );
}

export default Tab;

export {
  TabUnit
};

Tab.defaultProps = {
  height: 'auto',
  maxHeight: 'auto'
}

TabBar.defaultProps = {
  align: 'left',
  color: 'default'
}

TabBarButton.defaultProps = {
  color: 'default',
  theme: {
    colors: Theme.colors
  }
}
