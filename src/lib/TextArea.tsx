import React from 'react';
import styled from 'styled-components';

const TextAreaContainer: any = styled.div<{ noMargin: boolean }>`
  width: 100%;
  margin: ${({ noMargin }) => noMargin ? '0' : '0 0 12px'};
`;

TextAreaContainer.Label = styled.label<{ color: string }>`
  margin: 0 0 4px;
  font-weight: 500;
  font-size: 0.9rem;
  line-height: 1rem;
  display: block;
  color: ${({ theme, color }) => (color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
`;

TextAreaContainer.Help = styled.small`
  color: ${({ color, theme }) => (color === 'hollow') ? (theme.colors.hollow.regular) : (theme.colors.default.regular)};
  font-size: 0.8rem;
  line-height: 1rem;
  display: block;
  margin-top: 4px;
  opacity: 0.6;
`;

TextAreaContainer.Element = styled.textarea<{ resize: boolean, mobile: boolean, height: string }>`
  width: 100%;
  display: block;
  resize: ${({ resize }) => (resize) ? ('vertical') : ('none')};
  font-family: ${({ theme }) => theme.typography.fontFamily};
  color: ${({ theme, color }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
  height: ${({ height }) => (height) || ('100px') };
  outline: none;
  overflow: auto;
  background: none;
  border-radius: 0;
  box-sizing: border-box;
  margin: 0;
  padding: ${({ theme }) => theme.textInputs.default.padding};
  min-height: 25px;
  font-size: 0.9rem;

  ${({ mobile, theme, color }) => (mobile) ? `
    border: none;
    border-bottom: 1px solid ${(color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
    border-radius: 0;
  ` : `
    border: 1px solid ${(color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
    border-radius: ${theme.textInputs.default.borderRadius};
  `}

  &:invalid {
    border-bottom-color: ${({ theme, mobile }) => (mobile) ? (theme.colors.default.regular) : (theme.colors.danger.regular)};
    color: ${({ color, theme, mobile }) => (mobile) ? (theme.colors.danger.active) : ((color) ? (theme.colors[color].active) : (theme.colors.text.regular))};
    outline: none;
    box-shadow: none;
  }

  &:valid {
    color: ${({ color, theme }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
  }

  &:focus {
    border-bottom-color: ${({ mobile, color, theme }) => (color) ? ((mobile) ? (theme.colors.default.regular) : (theme.colors[color].active)) : ((mobile) ? (theme.colors.default.regular) : (theme.colors.default.active))};
  }

  &::-webkit-input-placeholder {
    color: ${({ theme, color }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
    opacity: 0.4;
  }
  &::-moz-placeholder {
    color: ${({ theme, color }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
    opacity: 0.4;
  }
  &:-ms-input-placeholder {
    color: ${({ theme, color }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
    opacity: 0.4;
  }
  &:-moz-placeholder {
    color: ${({ theme, color }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
    opacity: 0.4;
  }
`;

type TextAreaProps = {
  id?: string,
  label?: string,
  name?: string,
  className?: string,
  containerClassname?: string,
  resize?: string,
  noMargin?: boolean,
  mobile?: boolean,
  inputRef?: any,
  color?: string,
  help?: string,
  [otherProps: string]: any
}

const TextArea = ({
  className,
  name,
  id,
  label,
  containerClassname,
  noMargin,
  inputRef,
  resize,
  color,
  help,
  mobile,
  ...otherProps
}: TextAreaProps) => (
  <TextAreaContainer noMargin={noMargin} className={containerClassname}>
    { label && <TextAreaContainer.Label mobile={mobile} color={color} htmlFor={id}>{label}</TextAreaContainer.Label> }
    <TextAreaContainer.Element
      id={id}
      name={name}
      ref={inputRef}
      color={color}
      mobile={mobile}
      className={className}
      {...otherProps}
    />
      
    { help && <TextAreaContainer.Help color={color}>{help}</TextAreaContainer.Help>}
  </TextAreaContainer>
)

export default TextArea;
