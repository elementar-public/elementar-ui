import React from 'react';
import styled from 'styled-components';
import Theme from './Theme';

const LoadingContainer: any = styled.div<{ color?: string }>`
  width: 100px;
  height: 100px;
  margin: 0 auto;
  color: ${({ theme, color }) => (color) ? theme.colors[color].regular : 'inherit'};
`;

interface LoadingProps {
  type: number,
  color: string,
  theme?: any,
  [otherProps: string]: any
}

const Loading = ({
  type,
  theme = Theme,
  color,
  ...otherProps
}: LoadingProps) => {
  switch (type) {
    case 1:
      return (
        <LoadingContainer color={color} {...otherProps}>
          <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100">
            <g transform="rotate(0 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.9375s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(22.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.875s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(45 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.8125s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(67.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.75s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(90 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.6875s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(112.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.625s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(135 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5625s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(157.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.5s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(180 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.4375s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(202.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.375s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(225 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.3125s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(247.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.25s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(270 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.1875s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(292.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.125s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(315 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="-0.0625s" repeatCount="indefinite"></animate>
              </rect>
            </g>
            <g transform="rotate(337.5 50 50)">
              <rect x="47.5" y="25" rx="2.5" ry="2.5" width="5" height="10" fill="currentColor">
                <animate attributeName="opacity" values="1;0" keyTimes="0;1" dur="1s" begin="0s" repeatCount="indefinite"></animate>
              </rect>
            </g>
          </svg>
        </LoadingContainer>
      );
    case 2:
      return (
        <LoadingContainer color={color} {...otherProps}>
          <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100">
            <g transform="translate(80,50)">
              <g transform="rotate(0)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="1">
                  <animateTransform attributeName="transform" type="scale" begin="-0.9166666666666666s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.9166666666666666s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(75.98076211353316,65)">
              <g transform="rotate(29.999999999999996)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.9166666666666666">
                  <animateTransform attributeName="transform" type="scale" begin="-0.8333333333333334s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.8333333333333334s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(65,75.98076211353316)">
              <g transform="rotate(59.99999999999999)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.8333333333333334">
                  <animateTransform attributeName="transform" type="scale" begin="-0.75s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.75s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(50,80)">
              <g transform="rotate(90)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.75">
                  <animateTransform attributeName="transform" type="scale" begin="-0.6666666666666666s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.6666666666666666s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(35.00000000000001,75.98076211353316)">
              <g transform="rotate(119.99999999999999)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.6666666666666666">
                  <animateTransform attributeName="transform" type="scale" begin="-0.5833333333333334s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.5833333333333334s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(24.01923788646684,65)">
              <g transform="rotate(150.00000000000003)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.5833333333333334">
                  <animateTransform attributeName="transform" type="scale" begin="-0.5s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.5s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(20,50.00000000000001)">
              <g transform="rotate(180)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.5">
                  <animateTransform attributeName="transform" type="scale" begin="-0.4166666666666667s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.4166666666666667s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(24.019237886466836,35.00000000000001)">
              <g transform="rotate(209.99999999999997)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.4166666666666667">
                  <animateTransform attributeName="transform" type="scale" begin="-0.3333333333333333s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.3333333333333333s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(34.999999999999986,24.019237886466847)">
              <g transform="rotate(239.99999999999997)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.3333333333333333">
                  <animateTransform attributeName="transform" type="scale" begin="-0.25s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.25s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(49.99999999999999,20)">
              <g transform="rotate(270)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.25">
                  <animateTransform attributeName="transform" type="scale" begin="-0.16666666666666666s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.16666666666666666s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(65,24.019237886466843)">
              <g transform="rotate(300.00000000000006)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.16666666666666666">
                  <animateTransform attributeName="transform" type="scale" begin="-0.08333333333333333s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="-0.08333333333333333s"></animate>
                </circle>
              </g>
            </g>
            <g transform="translate(75.98076211353316,34.999999999999986)">
              <g transform="rotate(329.99999999999994)">
                <circle cx="0" cy="0" r="4" fill="currentColor" fillOpacity="0.08333333333333333">
                  <animateTransform attributeName="transform" type="scale" begin="0s" values="1.5 1.5;1 1" keyTimes="0;1" dur="1s" repeatCount="indefinite"></animateTransform>
                  <animate attributeName="fill-opacity" keyTimes="0;1" dur="1s" repeatCount="indefinite" values="1;0" begin="0s"></animate>
                </circle>
              </g>
            </g>
          </svg>
        </LoadingContainer>
      );
    case 3:
      return (
        <LoadingContainer color={color} {...otherProps}>
          <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100">
            <rect x="20" y="30" width="10" height="40" fill="currentColor">
              <animate attributeName="y" repeatCount="indefinite" dur="1s" calcMode="spline" keyTimes="0;0.5;1" values="10;30;30" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.2s"></animate>
              <animate attributeName="height" repeatCount="indefinite" dur="1s" calcMode="spline" keyTimes="0;0.5;1" values="80;40;40" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.2s"></animate>
            </rect>
            <rect x="45" y="30" width="10" height="40" fill="currentColor">
              <animate attributeName="y" repeatCount="indefinite" dur="1s" calcMode="spline" keyTimes="0;0.5;1" values="15;30;30" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.1s"></animate>
              <animate attributeName="height" repeatCount="indefinite" dur="1s" calcMode="spline" keyTimes="0;0.5;1" values="70;40;40" keySplines="0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.1s"></animate>
            </rect>
            <rect x="70" y="30" width="10" height="40" fill="currentColor">
              <animate attributeName="y" repeatCount="indefinite" dur="1s" calcMode="spline" keyTimes="0;0.5;1" values="15;30;30" keySplines="0 0.5 0.5 1;0 0.5 0.5 1"></animate>
              <animate attributeName="height" repeatCount="indefinite" dur="1s" calcMode="spline" keyTimes="0;0.5;1" values="70;40;40" keySplines="0 0.5 0.5 1;0 0.5 0.5 1"></animate>
            </rect>
          </svg>
        </LoadingContainer>
      );
    case 4:
      return (
        <LoadingContainer color={color} {...otherProps}>
          <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100">
            <circle cx="84" cy="50" r="10" fill="currentColor">
              <animate attributeName="r" repeatCount="indefinite" dur="0.5s" calcMode="spline" keyTimes="0;1" values="12;0" keySplines="0 0.5 0.5 1" begin="0s"></animate>
            </circle><circle cx="16" cy="50" r="10" fill="currentColor">
              <animate attributeName="r" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="0;0;12;12;12" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="0s"></animate>
              <animate attributeName="cx" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="16;16;16;50;84" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="0s"></animate>
            </circle><circle cx="50" cy="50" r="10" fill="currentColor">
              <animate attributeName="r" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="0;0;12;12;12" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.5s"></animate>
              <animate attributeName="cx" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="16;16;16;50;84" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="-0.5s"></animate>
            </circle><circle cx="84" cy="50" r="10" fill="currentColor">
              <animate attributeName="r" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="0;0;12;12;12" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="-1s"></animate>
              <animate attributeName="cx" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="16;16;16;50;84" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="-1s"></animate>
            </circle><circle cx="16" cy="50" r="10" fill="currentColor">
              <animate attributeName="r" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="0;0;12;12;12" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="-1.5s"></animate>
              <animate attributeName="cx" repeatCount="indefinite" dur="2s" calcMode="spline" keyTimes="0;0.25;0.5;0.75;1" values="16;16;16;50;84" keySplines="0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1;0 0.5 0.5 1" begin="-1.5s"></animate>
            </circle>
          </svg>
        </LoadingContainer>
      );
    case 5:
      return (
        <LoadingContainer color={color} {...otherProps}>
          <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100">
            <circle cx="28" cy="75" r="11" fill="currentColor">
              <animate attributeName="fill-opacity" repeatCount="indefinite" dur="2s" values="0;1;1" keyTimes="0;0.2;1" begin="0s"></animate>
            </circle>

            <path d="M28 47A28 28 0 0 1 56 75" fill="none" stroke="currentColor" strokeWidth="10">
              <animate attributeName="stroke-opacity" repeatCount="indefinite" dur="2s" values="0;1;1" keyTimes="0;0.2;1" begin="0.2s"></animate>
            </path>

            <path d="M28 25A50 50 0 0 1 78 75" fill="none" stroke="currentColor" strokeWidth="10">
              <animate attributeName="stroke-opacity" repeatCount="indefinite" dur="2s" values="0;1;1" keyTimes="0;0.2;1" begin="0.4s"></animate>
            </path>
          </svg>
        </LoadingContainer>
      );
    default:
      return (
        <LoadingContainer color={color} {...otherProps}>
          <svg xmlns="http://www.w3.org/2000/svg" width="100px" height="100px" viewBox="0 0 100 100">
            <circle cx="50" cy="50" fill="none" stroke="currentColor" strokeWidth="4" r="20" strokeDasharray="94.24777960769379 33.41592653589793">
              <animateTransform attributeName="transform" type="rotate" repeatCount="indefinite" dur="1s" values="0 50 50;360 50 50" keyTimes="0;1"></animateTransform>
            </circle>
          </svg>
        </LoadingContainer>
      );
  }
}

export default Loading;
