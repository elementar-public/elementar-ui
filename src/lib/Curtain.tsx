import React, { useEffect, useRef, useCallback } from 'react'
import styled from 'styled-components'
import Button from './Button';
import CloseIcon from './Icons/Close';

const CurtainBox: any = styled.div.attrs<{ width: number }>(({ width }) => ({
  style: {
    width: width ? `${width}px` : 'calc(100vw - 40px)'
  }
}))<{ active: boolean }>`
  background: white;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  display: flex;
  flex-direction: column;
  padding: 0;
  margin: 0;
  border-radius: 4px;
  min-width: 300px;
  max-width: calc(100vw - 40px);
  height: calc(100vh - 40px);
  position: fixed;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  transition: all 0.25s ease-in-out;
`;

CurtainBox.Backdrop = styled.div.attrs<{ active: boolean }>(({ active }) => ({
  style: {
    visibility: active ? 'visible' : 'hidden',
    opactiy: active ? 1 : 0
  }
}))<{ active: boolean }>`
  backface-visibility: hidden;
  position: fixed;
  width: 100vw;
  height: 100vh;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.3);
  z-index: 12;
  transition: all 0.25s ease-in-out;
`;

CurtainBox.Head = styled.div`
  flex: 0 0 auto;
  padding: 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid ${({ theme }) => theme.colors.dimmed.hover};

  h2 {
    flex: 0 1 auto;
    text-align: left;
    margin: 0;
    padding: 0;
    color: ${({ theme }) => theme.colors.text.titles};
    font-size: 1.2rem;
    line-height: 1.6rem;
    letter-spacing: -0.5px;
    min-width: 0;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`;

CurtainBox.Head.Close = styled.button`
  border: none;
  background: none;
  padding: 0;
  margin: 0;
  width: 24px;
  height: 24px;
  color: ${({ theme }) => theme.colors.text.titles};
  flex: 0 0 auto;
  opacity: 0.6;
  transition: opacity 0.25s;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }

  svg {
    display: block;
    pointer-events: none;
  }
`;

CurtainBox.Content = styled.div`
  flex: 0 1 100%;
  overflow: hidden;
  overflow-y: auto;
  margin: 0 0 auto;

  > div {
    padding: 24px 16px;
  }

  @media screen and (max-width: 419px) {
    margin: auto 0;
  }
`;

CurtainBox.Actions = styled.ul`
  list-style: none;
  margin: 0;
  padding: 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-top: 1px solid ${({ theme }) => theme.colors.dimmed.active};

  > li {
    display: flex;
  }
`;

interface CurtainProps {
  active: boolean,
  title?: string,
  width?: number,
  closeClassName?: string,
  children: React.ReactNode,
  dismiss?: {
    label: string,
    color?: string,
    action: (e?: any) => void
  },
  confirm?: {
    type: 'button' | 'submit',
    label: string,
    color?: string,
    pending?: boolean,
    disabled?: boolean,
    action: (e?: any) => void
  },
  onSubmit?: (e?: any) => void,
  close: (e?: any) => void
}

const Curtain: any = ({
  active,
  close,
  title = 'Título do conteúdo',
  children,
  width,
  dismiss,
  confirm,
  onSubmit,
  closeClassName,
}: CurtainProps) => {
  const backdropEl = useRef<HTMLDivElement>(null);

  const removeModal = () => {
    if (backdropEl.current) backdropEl.current.parentNode?.removeChild(backdropEl.current);
    document.body.style.overflow = 'auto';
  }

  const closeModal = useCallback(() => {
    document.body.style.overflow = 'auto';
    if (close)
      close();
    else
      removeModal();
  }, [close]);

  useEffect(() => {
    if (active && backdropEl.current?.isConnected) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  }, [active]);

  useEffect(() => {
    document.body.style.overflow = 'auto';
  }, []);

  return (
    <CurtainBox.Backdrop
      active={active}
      ref={backdropEl}
    >
      <CurtainBox
        active={active}
        width={width}
        as={onSubmit ? 'form' : 'div'}
        onSubmit={onSubmit}
      >
        <CurtainBox.Head>
          <h2>{title}</h2>
          <CurtainBox.Head.Close type="button" onClick={closeModal || removeModal} className={closeClassName}>
            <CloseIcon role="img" aria-label="Cancelar" />
          </CurtainBox.Head.Close>
        </CurtainBox.Head>
        
        <CurtainBox.Content>
          <div>
            {children}
          </div>
        </CurtainBox.Content>

        <CurtainBox.Actions>
          <li>
            {(dismiss) ? (
              <Button
                outline
                light
                label={dismiss.label || 'Cancelar'}
                onClick={dismiss.action || closeModal}
                color={dismiss.color || 'text'}
              />
            ) : (
              <Button
                outline
                light
                label="Cancelar"
                onClick={closeModal}
              />
            )}
          </li>

          {(confirm) && (
            <li>
              <Button
                onClick={confirm.type === 'submit' ? undefined : (confirm.action || closeModal)}
                label={confirm.label || 'Salvar'}
                color={confirm.color || 'primary'}
                type={confirm.type || 'button'}
                loading={confirm.pending}
                disabled={confirm.disabled}
              />
            </li>
          )}
        </CurtainBox.Actions>
      </CurtainBox>
    </CurtainBox.Backdrop>
  )
}

export default Curtain
