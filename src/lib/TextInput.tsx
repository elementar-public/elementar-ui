import React from 'react';
import styled from 'styled-components';

const TextInputContainer: any = styled.div<{ width: string, noMargin: boolean }>`
  width: ${({ width }) => (width) || ('100%')};
  margin: ${({ noMargin }) => (noMargin) ? ('0') : ('0 0 12px')};
`;

TextInputContainer.Box = styled.div<{ mobile: boolean }>`
  display: flex;
  flex-direction: ${({ mobile }) => (mobile) ? ('row') : ('column')};
  align-items: stretch;
  justify-content: space-between;
  width: 100%;
`;

TextInputContainer.Label = styled.label<{ mobile: boolean, color: string }>`
  margin: ${({ mobile }) => (mobile) ? ('0') : ('0 0 4px')};
  font-weight: 500;
  font-size: 0.9rem;
  line-height: ${({ mobile }) => (mobile) ? ('29px') : ('1rem')};
  display: block;
  color: ${({ theme, color }) => (color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
  ${({ theme, mobile, color }) => mobile && `
    flex: 0 0 auto;
    padding-right: 5px;
    border-bottom: 1px solid ${(color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
  `}
`;

TextInputContainer.Place = styled.div`
  flex: 0 1 auto;
  width: 100%;
  position: relative;
`;

TextInputContainer.Element = styled.input<{ mobile: boolean, align: string, icon: SVGAElement, size: string, type: string }>`
  ${({ theme, type, mobile, align, size, color, icon }) => ['text', 'email', 'number', 'password', 'date', 'tel'].includes(type) && `
    width: 100%;
    display: block;
    box-sizing: border-box;
    font-family: ${theme.typography.fontFamily};
    font-size: ${theme.textInputs[size].fontSize};
    color: ${(color) ? theme.colors[color].active : theme.colors.text.regular};
    text-align: ${(mobile) ? ('right') : (align)};
    height: ${theme.textInputs[size].height};
    padding: ${(icon) ? (theme.textInputs[size].paddingWithIcon) : (theme.textInputs[size].padding)};
    background: none;
    outline: none;

    ${(mobile) ? `
      border: none;
      border-bottom: 1px solid ${(color) ? (theme.colors[color].regular) : (theme.colors.default.regular)};
      border-radius: 0;
    ` : `
      border: 1px solid ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
      border-radius: ${theme.textInputs[size].borderRadius};
    `}

    &:hover {
      border-color: ${(color) ? (mobile ? theme.colors[color].regular : theme.colors[color].hover) : (mobile ? theme.colors.default.regular : theme.colors.default.hover)};
    }

    &:invalid {
      border-color: ${(mobile) ? ((color) ? theme.colors[color].regular : theme.colors.default.regular) : theme.colors.danger.regular};
      color: ${(mobile) ? (theme.colors.danger.active) : ((color) ? theme.colors[color].active : theme.colors.text.regular)};
      outline: none;
      box-shadow: none;
    }

    &:valid {
      color: ${(color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
    }

    &:focus {
      border-color: ${(color) ? ((mobile) ? (theme.colors[color].regular) : (theme.colors[color].active)) : ((mobile) ? (theme.colors.default.regular) : (theme.colors.default.active))};
    }

    &:disabled {
      opacity: 0.5;
    }

    &::-webkit-input-placeholder {
      color: ${(color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
      opacity: 0.4;
    }
    &::-moz-placeholder {
      color: ${(color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
      opacity: 0.4;
    }
    &:-ms-input-placeholder {
      color: ${(color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
      opacity: 0.4;
    }
    &:-moz-placeholder {
      color: ${(color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
      opacity: 0.4;
    }
  `
  }
`;

TextInputContainer.Icon = styled.div<{ color: string }>`
  position: absolute;
  right: 0;
  top: 50%;
  transform: translateY(-12px);
  color: ${({ theme, color }) => (color) ? (theme.colors[color].active) : (theme.colors.text.regular)};
  opacity: 0.4;
`;

TextInputContainer.Help = styled.small<{ color: string }>`
  color: ${({ theme, color }) => color === 'hollow' ? theme.colors.hollow.regular : theme.colors.default.regular};
  font-size: 0.8rem;
  line-height: 1rem;
  display: block;
  margin-top: 4px;
  opacity: 0.6;
`;

type TextInputProps = {
  id?: string,
  className?: string,
  containerClassname?: string,
  containerStyle?: any,
  label?: string,
  name?: string,
  type?: 'text' | 'email' | 'number' | 'password' | 'date' | 'tel',
  icon?: React.ReactNode,
  align?: string,
  noMargin?: boolean,
  inputRef?: any,
  size?: string,
  color?: string,
  help?: string,
  mobile?: boolean,
  width?: string,
  [otherProps: string]: any
}

const TextInput = ({
  type = 'text',
  width = '100%',
  align = 'left',
  size = 'default',
  className,
  id,
  name,
  label,
  icon,
  containerClassname,
  containerStyle,
  noMargin,
  inputRef,
  color,
  help,
  mobile,
  ...otherProps
}: TextInputProps) => (
  <TextInputContainer
    noMargin={noMargin}
    className={containerClassname}
    style={containerStyle}
    width={width}
  >
    <TextInputContainer.Box mobile={mobile}>
      {(label) && <TextInputContainer.Label mobile={mobile} color={color} htmlFor={id}>{label}</TextInputContainer.Label> }
      <TextInputContainer.Place>
        <TextInputContainer.Element
          id={id}
          type={type}
          ref={inputRef}
          name={name}
          color={color}
          size={size}
          align={align}
          mobile={mobile}
          className={className}
          icon={icon}
          {...otherProps}
        />

        {(icon) && (
          <TextInputContainer.Icon color={color}>{icon}</TextInputContainer.Icon>
        )}
      </TextInputContainer.Place>
    </TextInputContainer.Box>
    {(help) && <TextInputContainer.Help color={color}>{help}</TextInputContainer.Help>}
  </TextInputContainer>
);

export default TextInput;
