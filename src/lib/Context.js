import React, {useRef, useEffect, useCallback} from 'react';
import styled from 'styled-components';
import Button from './Button';
import Dropdown, {DropdownItem} from './Dropdown';
import MoreIcon from './Icons/More';
import CloseIcon from './Icons/Close';

const ContextBackdrop = styled.div`
  visibility: ${({active}) => active ? 'visible' : 'hidden'};
  backface-visibility: hidden;
  opacity: ${({active}) => active ? '1' : '0'};
  width: 100%;
  height: 100%;
  margin: 0;
  padding: 0;
  position: fixed;
  top: 0;
  bottom: 0;
  right: 0;
  left: 0;
  background: rgba(0, 0, 0, 0.3);
  z-index: 12;
  transition: all 0.25s ease-in-out;
`;

const ContextBody = styled.div.attrs(({size}) => {
  switch (size) {
    case 'small':
      return {style: {maxWidth: 400}};
    case 'large':
      return {style: {maxWidth: 800}};
    case 'x-large':
      return {style: {maxWidth: 968}};
    default:
      return {style: {maxWidth: 600}};
  }
})`
  background: white;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3);
  box-sizing: border-box;
  border-radius: 4px;
  padding: 0;
  position: fixed;
  top: 50%;
  left: 50%;
  width: 100%;
  min-height: 240px;
  max-height: calc(100vh - 20px);
  transform: translate(-50%, -50%) scale(${({active}) => active ? '1, 1' : '0.8, 0.8'});
  overflow: hidden;
  display: flex;
  flex-direction: column;
  align-content: stretch;
  transition: all 0.25s ease-in-out;

  @media screen and (max-width: 419px) {
    transform: none;
    border-radius: 0;
    width: 100vw;
    max-height: 100vh;
    min-width: 0;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;

    ${({cover}) => cover && `
      height: 100%;
    `}
  }
`;

const ContextHead = styled.div`
  flex: 0 0 auto;
  padding: 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-bottom: 1px solid ${({theme}) => theme.colors.dimmed.hover};

  h2 {
    flex: 0 1 auto;
    text-align: left;
    margin: 0;
    padding: 0;
    color: ${({theme}) => theme.colors.text.titles};
    font-size: 1.2rem;
    line-height: 1.6rem;
    letter-spacing: -0.5px;
    min-width: 0;
    white-space: nowrap;
    text-overflow: ellipsis;
    overflow: hidden;
  }
`;

const ContentClose = styled.button`
  border: none;
  background: none;
  padding: 0;
  margin: 0;
  width: 24px;
  height: 24px;
  color: ${({theme}) => theme.colors.text.titles};
  flex: 0 0 auto;
  opacity: 0.6;
  transition: opacity 0.25s;
  cursor: pointer;

  &:hover {
    opacity: 1;
  }

  svg {
    display: block;
    pointer-events: none;
  }
`;

const ContextContent = styled.div`
  flex: 0 1 auto;
  overflow: hidden;
  overflow-y: auto;
  margin: 0 0 auto;

  > div {
    padding: 24px 16px;
  }

  @media screen and (max-width: 419px) {
    margin: auto 0;
  }
`;

const ContextFoot = styled.div`
  flex: 0 0 auto;
  margin: 0;
  padding: 0;
`;

const ContextProgress = styled.div`
  height: 6px;
  width: 100%;
  margin: -6px 0 0;
  padding: 0;
`;

const ContextProgressBar = styled.span.attrs(({ theme, progress }) => ({
  style: {
    width: 100 * (progress.step / progress.steps) + '%',
    display: 'block',
    height: 6,
    background: theme.colors.dimmed.active,
    transition: 'width 0.25s ease-in-out'
  }
}))(() => '');

const ContextActions = styled.ul`
  list-style: none;
  margin: 0;
  padding: 16px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  border-top: 1px solid ${({theme}) => theme.colors.dimmed.active};

  > li {
    display: flex;
  }
`;

const Context = ({
  active,
  close,
  title,
  size,
  dismiss,
  confirm,
  progress,
  onSubmit,
  otherActions,
  children,
  closeClassName,
  ...otherProps
}) => {
  const backdropEl = useRef();

  const removeModal = () => {
    backdropEl.current.parentNode.removeChild(backdropEl.current);
    document.body.style.overflow = 'auto';
  }

  const closeModal = useCallback(() => {
    document.body.style.overflow = 'auto';
    if (close)
      close();
    else
      removeModal();
  }, [close]);

  useEffect(() => {
    if (active && backdropEl.current.isConnected) {
      document.body.style.overflow = 'hidden';
    } else {
      document.body.style.overflow = 'auto';
    }
  }, [active]);

  useEffect(() => (() => document.body.style.overflow = 'auto'), []);

  return (
    <ContextBackdrop active={active} ref={backdropEl}>
      <ContextBody
        active={active}
        size={size}
        as={onSubmit ? 'form' : 'div'}
        onSubmit={onSubmit}
        {...otherProps}
      >
        {title &&
          <ContextHead>
            <h2>{title}</h2>
            <ContentClose type="button" onClick={closeModal || removeModal} className={closeClassName}>
              <CloseIcon role="img" aria-label={dismiss ? dismiss.label : 'Cancelar'} />
            </ContentClose>
          </ContextHead>
        }

        <ContextContent>
          <div>
            {children}
          </div>
        </ContextContent>

        <ContextFoot>
          {progress &&
            <ContextProgress>
              <ContextProgressBar progress={progress}>&nbsp;</ContextProgressBar>
            </ContextProgress>
          }

          <ContextActions>
            <li>
              {dismiss ?
                <Button
                  outline
                  light
                  label={dismiss.label || 'Cancelar'}
                  onClick={dismiss.action || closeModal}
                  color={dismiss.color || 'text'}
                  dock={otherActions ? 'left' : null}
                />
                :
                <Button
                  outline
                  light
                  label="Cancelar"
                  onClick={closeModal}
                  dock={otherActions ? 'left' : null}
                />
              }

              {otherActions &&
                <Dropdown up trigger={<Button outline color="text" type="button" dock="right"><MoreIcon /></Button>}>
                  {otherActions.map((action, i) => <DropdownItem key={i} label={action.label} action={action.action} color={action.color} />)}
                </Dropdown>
              }
            </li>

            {confirm &&
              <li>
                <Button
                  onClick={confirm.type === 'submit' ? undefined : (confirm.action || closeModal)}
                  label={confirm.label || 'Salvar'}
                  color={confirm.color || 'primary'}
                  type={confirm.type || 'button'}
                  loading={confirm.pending}
                  disabled={confirm.disabled}
                />
              </li>
            }
          </ContextActions>
        </ContextFoot>
      </ContextBody>
    </ContextBackdrop>
  );
}

export default Context;
