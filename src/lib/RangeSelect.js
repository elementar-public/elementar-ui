import React from 'react';
import styled from 'styled-components';
import Theme from './Theme';

const RangeSelectContainer = styled.div`
  padding: 0;
  margin: ${props => props.noMargin ? '0' : '0 0 12px'};
`;

const RangeSelectBox = styled.div`
  display: flex;
  flex-direction: ${props => props.mobile ? 'row' : 'column'};
  align-items: stretch;
  justify-content: space-between;
  width: 100%;
`;

const RangeSelectLabel = styled.label`
  margin: ${props => props.mobile ? '0' : '0 0 4px'};
  font-weight: 500;
  font-size: 0.9rem;
  line-height: ${props => props.mobile ? '29px' : '1rem'};
  display: block;
  color: ${props => props.color ? props.theme.colors[props.color].regular : props.theme.colors.default.regular};
  ${ props => props.mobile && `
    flex: 0 0 auto;
    padding-right: 5px;
    border-bottom: 1px solid ${props.color ? props.theme.colors[props.color].regular : props.theme.colors.default.regular};
  `}
`;

const RangeSelectPlace = styled.div`
  flex: 0 1 auto;
  width: 100%;
`;

const RangeSelectInput = styled.input`
  width: ${props => props.width};
  -webkit-appearance: none;
  margin: 10px 0;
  background: none;

  &:focus,
  &:active {
    outline: none;
  }
  &::-webkit-slider-runnable-track {
    width: 100%;
    height: 1px;
    cursor: pointer;
    background: rgba(128,128,128,0.3);
    box-shadow: none;
    border: none;
    margin: 10px 0;
  }
  &::-webkit-slider-thumb {
    box-shadow: none;
    border: none;
    height: 24px;
    width: 24px;
    border-radius: 12px;
    background: ${props => props.theme.colors[props.color].regular};
    cursor: pointer;
    -webkit-appearance: none;
    margin-top: -12px;
  }
  &:hover::-webkit-slider-thumb {
    background: ${props => props.theme.colors[props.color].hover};
  }
  &:active::-webkit-slider-thumb {
    background: ${props => props.theme.colors[props.color].active};
  }
  &:focus::-webkit-slider-runnable-track {
    background: rgba(128,128,128,0.3);
  }
  &::-moz-range-track {
    width: 100%;
    height: 1px;
    cursor: pointer;
    background: rgba(128,128,128,0.3);
    box-shadow: none;
    border: none;
  }
  &::-moz-range-thumb {
    box-shadow: none;
    border: none;
    height: 24px;
    width: 24px;
    border-radius: 12px;
    background: ${props => props.theme.colors[props.color].regular};
    cursor: pointer;
  }
  &:hover::-moz-range-thumb {
    background: ${props => props.theme.colors[props.color].hover};
  }
  &:active::-moz-range-thumb {
    background: ${props => props.theme.colors[props.color].active};
  }
  &::-ms-track {
    width: 100%;
    height: 1px;
    cursor: pointer;
    background: transparent;
    border-color: transparent;
    color: transparent;
  }
  &::-ms-fill-lower {
    background: rgba(128,128,128,0.3);
    border: none;
    border-radius: 0;
    box-shadow: none;
  }
  &::-ms-fill-upper {
    background: rgba(128,128,128,0.3);
    border: none;
    border-radius: 0;
    box-shadow: none;
  }
  &::-ms-thumb {
    box-shadow: none;
    border: none;
    height: 24px;
    width: 24px;
    border-radius: 12px;
    background: ${props => props.theme.colors[props.color].regular};
    cursor: pointer;
    height: 1px;
  }
  &:hover::-ms-thumb {
    background: ${props => props.theme.colors[props.color].hover};
  }
  &:active::-ms-thumb {
    background: ${props => props.theme.colors[props.color].active};
  }
  &:focus::-ms-fill-lower {
    background: rgba(128,128,128,0.3);
  }
  &:focus::-ms-fill-upper {
    background: rgba(128,128,128,0.3);
  }
`;

const RangeSelectHelp = styled.small`
  color: ${props => props.color === 'hollow' ? props.theme.colors.hollow.regular : props.theme.colors.default.regular};
  font-size: 0.8rem;
  line-height: 1rem;
  display: block;
  margin-top: 4px;
  opacity: 0.6;
`;

const RangeSelect = ({
  id,
  color,
  width,
  noMargin,
  containerClassname,
  label,
  mobile,
  theme,
  help,
  ...otherProps
}) => (
  <RangeSelectContainer noMargin={noMargin} className={containerClassname}>
    <RangeSelectBox>
      { label && <RangeSelectLabel mobile={mobile} color={color} htmlFor={id}>{label}</RangeSelectLabel> }
      <RangeSelectPlace>
        <RangeSelectInput type="range" width={width} color={color} {...otherProps}/>
      </RangeSelectPlace>
    </RangeSelectBox>
    { help && <RangeSelectHelp color={color}>{help}</RangeSelectHelp>}
  </RangeSelectContainer>
)

export default RangeSelect;

RangeSelect.defaultProps = {
  color: 'default',
  width: '100%',
  theme: {
    colors: Theme.colors
  }
}
