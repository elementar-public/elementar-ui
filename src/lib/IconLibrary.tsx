import React from 'react';
import PhotoIcon from './Library/PhotoIcon';
import MoneyIcon from './Library/MoneyIcon';
import VideoIcon from './Library/VideoIcon';
import MapIcon from './Library/MapIcon';
import LocationIcon from './Library/LocationIcon';
import AreaIcon from './Library/AreaIcon';
import BedroomIcon from './Library/BedroomIcon';
import BathroomIcon from './Library/BathroomIcon';
import GarageIcon from './Library/GarageIcon';
import ConfirmIcon from './Library/ConfirmIcon';
import WaterBoilerIcon from './Library/WaterBoilerIcon';
import CoworkingSpaceIcon from './Library/CoworkingSpaceIcon';
import BarbecueIcon from './Library/BarbecueIcon';
import PoolIcon from './Library/PoolIcon';
import SportsCourtIcon from './Library/SportsCourtIcon';
import CinemaRoomIcon from './Library/CinemaRoomIcon';
import PlaygroundIcon from './Library/PlaygroundIcon';
import GymIcon from './Library/GymIcon';
import USBIcon from './Library/USBIcon';
import GourmetIcon from './Library/GourmetIcon';
import PartyIcon from './Library/PartyIcon';
import GrillIcon from './Library/GrillIcon';
import ClosedGarageIcon from './Library/ClosedGarageIcon';
import AirConditionerIcon from './Library/AirConditionerIcon';
import PizzaIcon from './Library/PizzaIcon';
import GamesRoomIcon from './Library/GamesRoomIcon';
import GamerSpaceIcon from './Library/GamerSpaceIcon';
import ToysRoomIcon from './Library/ToysRoomIcon';
import BicycleIcon from './Library/BicycleIcon';
import PipedGasIcon from './Library/PipedGasIcon';
import SpaIcon from './Library/SpaIcon';
import SolariumIcon from './Library/SolariumIcon';
import SteamRoomIcon from './Library/SteamRoomIcon';
import GeneratorIcon from './Library/GeneratorIcon';
import LaundryIcon from './Library/LaundryIcon';
import BalconyIcon from './Library/BalconyIcon';
import UnitIcon from './Library/UnitIcon';
import TransportationIcon from './Library/TransportationIcon';
import EducationIcon from './Library/EducationIcon';
import HealthIcon from './Library/HealthIcon';
import ServicesIcon from './Library/ServicesIcon';
// import ChevronIcon from './Library/ChevronIcon';
import ChevronUpIcon from './Library/ChevronUpIcon';
import ChevronDownIcon from './Library/ChevronDownIcon';
import ChevronLeftIcon from './Library/ChevronLeftIcon';
import ChevronRightIcon from './Library/ChevronRightIcon';
import ArrowUpIcon from './Library/ArrowUpIcon';
import ArrowDownIcon from './Library/ArrowDownIcon';
import ArrowLeftIcon from './Library/ArrowLeftIcon';
import ArrowRightIcon from './Library/ArrowRightLeft';
import ArrowUpLeftIcon from './Library/ArrowUpLeftIcon';
import ArrowUpRightIcon from './Library/ArrowUpRightIcon';
import ArrowDownRightIcon from './Library/ArrowDownRightIcon';
import ArrowDownLeftIcon from './Library/ArrowDownLeftIcon';
import ThickArrowUpIcon from './Library/ThickArrowUpIcon';
import ThickArrowDownIcon from './Library/ThickArrowDownIcon';
import ThickArrowLeftIcon from './Library/ThickArrowLeftIcon';
import ThickArrowRightIcon from './Library/ThickArrowRightIcon';
import ThickArrowUpLeftIcon from './Library/ThickArrowUpLeftIcon';
import ThickArrowUpRightIcon from './Library/ThickArrowUpRightIcon';
import ThickArrowDownLeftIcon from './Library/ThickArrowDownLeftIcon';
import ThickArrowDownRightIcon from './Library/ThickArrowDownRightIcon';
import EditIcon from './Library/EditIcon';
import MoreIcon from './Library/MoreIcon';
import LessIcon from './Library/LessIcon';
import ExpandIcon from './Library/ExpandIcon';
import CloseIcon from './Library/CloseIcon';
import PhoneIcon from './Library/PhoneIcon';
import MutePhoneIcon from './Library/MutePhoneIcon';
import ClipboardIcon from './Library/ClipboardIcon';
import WhatsappIcon from './Library/WhatsappIcon';
import RemoveIcon from './Library/RemoveIcon';
import DeleteIcon from './Library/DeleteIcon';
import CleanIcon from './Library/CleanIcon';
import AddIcon from './Library/AddIcon';
import SearchIcon from './Library/SearchIcon';
import HomeIcon from './Library/HomeIcon';
import UserIcon from './Library/UserIcon';
import UsersIcon from './Library/UsersIcon';
import MoreItemsIcon from './Library/MoreItemsIcon';
import BuildingsIcon from './Library/BuildingsIcon';
import PropertiesIcon from './Library/PropertiesIcon';
import PropertyIcon from './Library/PropertyIcon';
import DevelopersIcon from './Library/DevelopersIcon';
import DeveloperIcon from './Library/DeveloperIcon';
import DocumentIcon from './Library/DocumentIcon';
import DocumentsIcon from './Library/DocumentsIcon';
import LogoutIcon from './Library/LogoutIcon';
import PersonIcon from './Library/PersonIcon';
import AddPersonIcon from './Library/AddPersonIcon';
import MenuIcon from './Library/MenuIcon';
import CheckIcon from './Library/CheckIcon';
import WarningIcon from './Library/WarningIcon';
import DangerIcon from './Library/DangerIcon';
import EditPanelIcon from './Library/EditPanelIcon';
import DownloadIcon from './Library/DownloadIcon';
import MenuTasksIcon from './Library/MenuTasksIcon';
import MenuDashboardIcon from './Library/MenuDashboardIcon';
import MenuContactsIcon from './Library/MenuContactsIcon';
import MenuCampaignsIcon from './Library/MenuCampaignsIcon';
import MenuSalesIcon from './Library/MenuSalesIcon';
import MenuSalesAltIcon from './Library/MenuSalesAltIcon';
import MenuVisitsIcon from './Library/MenuVisitsIcon';
import MenuUsersIcon from './Library/MenuUsersIcon';
import MenuPersonsIcon from './Library/MenuPersonsIcon';
import MenuMarketingIcon from './Library/MenuMarketingIcon';
import MenuChevronUpIcon from './Library/MenuChevronUpIcon';
import MenuChevronDownIcon from './Library/MenuChevronDownIcon';
import MenuChevronLeftIcon from './Library/MenuChevronLeftIcon';
import MenuChevronRightIcon from './Library/MenuChevronRightIcon';
import LinkIcon from './Library/LinkIcon';
import MessageIcon from './Library/MessageIcon';
import GlobeIcon from './Library/GlobeIcon';
import SunIcon from './Library/SunIcon';
import MoonIcon from './Library/MoonIcon';
import PlayIcon from './Library/PlayIcon';
import ReverseIcon from './Library/ReverseIcon';
import PauseIcon from './Library/PauseIcon';
import PlayPauseIcon from './Library/PlayPauseIcon';
import FastForwardIcon from './Library/FastForwardIcon';
import FastReverseIcon from './Library/FastReverseIcon';
import NextTrackIcon from './Library/NextTrackIcon';
import PrevTrackIcon from './Library/PrevTrackIcon';
import StopIcon from './Library/StopIcon';
import EjectIcon from './Library/EjectIcon';
import HelpIcon from './Library/HelpIcon';
import MenuHelpIcon from './Library/MenuHelpIcon';
import MenuOpsIcon from './Library/MenuOpsIcon';
import SeenMessageIcon from './Library/SeenMessageIcon';
import ContactIcon from './Library/ContactIcon';
import OptOutIcon from './Library/OptOutIcon';
import SpinnerIcon from './Library/SpinnerIcon';
import LoadingIcon from './Library/LoadingIcon';
import BookmarkIcon from './Library/BookmarkIcon';
import HeartIcon from './Library/HeartIcon';
import StarIcon from './Library/StarIcon';
import ShareIcon from './Library/ShareIcon';
import PetIcon from './Library/PetIcon';
import ElevatorIcon from './Library/ElevatorIcon';
import WarmPoolIcon from './Library/WarmPoolIcon';
import WetDeckIcon from './Library/WetDeckIcon';
import RoofIcon from './Library/RoofIcon';
import MeetingRoomIcon from './Library/MeetingRoom';
import ExclusiveGarageIcon from './Library/ExclusiveGarageIcon';
import SoundIcon from './Library/SoundIcon';
import SoundUpIcon from './Library/SoundUpIcon';
import SoundDownIcon from './Library/SoundDownIcon';
import SoundMutedIcon from './Library/SoundMutedIcon';
import CalendarIcon from './Library/CalendarIcon';
import BeautyCareIcon from './Library/BeautyCareIcon';
import OutdoorFitnessIcon from './Library/OutdoorFitnessIcon';
import BeachCourtIcon from './Library/BeachCourtIcon';

const BenefitIcon = ({
  benefit,
  grid
}: {
  benefit?: string,
  grid?: boolean,
}) => {
  switch (benefit) {
    case 'barbecue':
      return <BarbecueIcon grid={grid} />;
    case 'party_space':
      return <PartyIcon grid={grid} />;
    case 'sports':
      return <SportsCourtIcon grid={grid} />;
    case 'pizza_oven':
      return <PizzaIcon grid={grid} />;
    case 'play_room':
      return <GamesRoomIcon grid={grid} />;
    case 'toy_room':
      return <ToysRoomIcon grid={grid} />;
    case 'game_room':
      return <GamerSpaceIcon grid={grid} />;
    case 'playground':
      return <PlaygroundIcon grid={grid} />;
    case 'pool':
      return <PoolIcon grid={grid} />;
    case 'cinema':
      return <CinemaRoomIcon grid={grid} />;
    case 'coworking':
      return <CoworkingSpaceIcon grid={grid} />;
    case 'gourmet_space':
      return <GourmetIcon grid={grid} />;
    case 'gym_space':
      return <GymIcon grid={grid} />;
    case 'gym':
      return <GymIcon grid={grid} />;
    case 'gyn':
      return <GymIcon grid={grid} />;
    case 'academia':
      return <GymIcon grid={grid} />;
    case 'covered_parking_spot':
      return <ClosedGarageIcon grid={grid} />;
    case 'usb_spots':
      return <USBIcon grid={grid} />;
    case 'grill_space':
      return <GrillIcon grid={grid} />;
    case 'air_conditioning':
      return <AirConditionerIcon grid={grid} />;
    case 'hot_water':
      return <WaterBoilerIcon grid={grid} />;
    case 'bike_spot':
      return <BicycleIcon grid={grid} />;
    case 'gas':
      return <PipedGasIcon grid={grid} />;
    case 'spa':
      return <SpaIcon grid={grid} />;
    case 'solarium':
      return <SolariumIcon grid={grid} />;
    case 'steam_room':
      return <SteamRoomIcon grid={grid} />;
    case 'sauna':
      return <SteamRoomIcon grid={grid} />;
    case 'energy_bank':
      return <GeneratorIcon grid={grid} />;
    case 'gerador':
      return <GeneratorIcon grid={grid} />;
    case 'laundry_room':
      return <LaundryIcon grid={grid} />;
    case 'lavanderia-coletiva':
      return <LaundryIcon grid={grid} />;
    case 'gourmet_balcony':
      return <BalconyIcon grid={grid} />;
    case 'varanda_gourmet':
      return <BalconyIcon grid={grid} />;
    case 'espaco_pet':
      return <PetIcon grid={grid} />;
    case 'elevador_regenerativo':
      return <ElevatorIcon grid={grid} />;
    case 'piscina_aquecida':
      return <WarmPoolIcon grid={grid} />;
    case 'deck_molhado':
      return <WetDeckIcon grid={grid} />;
    case 'lazer_cobertura':
      return <RoofIcon grid={grid} />;
    case 'sala_reuniao':
      return <MeetingRoomIcon grid={grid} />;
    case 'vagas_determinadas':
      return <ExclusiveGarageIcon grid={grid} />;
    case 'beauty_care':
      return <BeautyCareIcon grid={grid} />;
    case 'fitness_externo':
      return <OutdoorFitnessIcon grid={grid} />;
    case 'quadra_de_areia':
      return <BeachCourtIcon grid={grid} />;
    default:
      return <ConfirmIcon grid={grid} />;
  }
}

export default BenefitIcon;

export {
  PhotoIcon,
  VideoIcon,
  AreaIcon,
  MapIcon,
  LocationIcon,
  BedroomIcon,
  BathroomIcon,
  GarageIcon,
  ConfirmIcon,
  WaterBoilerIcon,
  CoworkingSpaceIcon,
  BarbecueIcon,
  PoolIcon,
  SportsCourtIcon,
  CinemaRoomIcon,
  PlaygroundIcon,
  GymIcon,
  USBIcon,
  GourmetIcon,
  PartyIcon,
  GrillIcon,
  ClosedGarageIcon,
  AirConditionerIcon,
  PizzaIcon,
  GamesRoomIcon,
  GamerSpaceIcon,
  ToysRoomIcon,
  BicycleIcon,
  PipedGasIcon,
  SpaIcon,
  SolariumIcon,
  SteamRoomIcon,
  GeneratorIcon,
  LaundryIcon,
  BalconyIcon,
  UnitIcon,
  TransportationIcon,
  EducationIcon,
  HealthIcon,
  ServicesIcon,
  // ChevronIcon,
  ChevronUpIcon,
  ChevronDownIcon,
  ChevronLeftIcon,
  ChevronRightIcon,
  ArrowUpIcon,
  ArrowDownIcon,
  ArrowLeftIcon,
  ArrowRightIcon,
  ArrowUpLeftIcon,
  ArrowUpRightIcon,
  ArrowDownLeftIcon,
  ArrowDownRightIcon,
  ThickArrowUpIcon,
  ThickArrowDownIcon,
  ThickArrowLeftIcon,
  ThickArrowRightIcon,
  ThickArrowUpLeftIcon,
  ThickArrowUpRightIcon,
  ThickArrowDownLeftIcon,
  ThickArrowDownRightIcon,
  EditIcon,
  MoreIcon,
  LessIcon,
  ExpandIcon,
  CloseIcon,
  PhoneIcon,
  MutePhoneIcon,
  WhatsappIcon,
  RemoveIcon,
  CleanIcon,
  DeleteIcon,
  AddIcon,
  SearchIcon,
  HomeIcon,
  UserIcon,
  UsersIcon,
  MoreItemsIcon,
  BuildingsIcon,
  PropertiesIcon,
  PropertyIcon,
  DevelopersIcon,
  DeveloperIcon,
  DocumentIcon,
  DocumentsIcon,
  LogoutIcon,
  MenuIcon,
  PersonIcon,
  AddPersonIcon,
  EditPanelIcon,
  CheckIcon,
  WarningIcon,
  DangerIcon,
  DownloadIcon,
  MenuTasksIcon,
  MenuDashboardIcon,
  MenuContactsIcon,
  MenuCampaignsIcon,
  MenuSalesIcon,
  MenuSalesAltIcon,
  MenuVisitsIcon,
  MenuUsersIcon,
  MenuPersonsIcon,
  MenuMarketingIcon,
  MenuChevronUpIcon,
  MenuChevronDownIcon,
  MenuChevronLeftIcon,
  MenuChevronRightIcon,
  MenuHelpIcon,
  MenuOpsIcon,
  HelpIcon,
  ClipboardIcon,
  LinkIcon,
  MessageIcon,
  GlobeIcon,
  PlayIcon,
  ReverseIcon,
  PauseIcon,
  PlayPauseIcon,
  FastForwardIcon,
  FastReverseIcon,
  NextTrackIcon,
  PrevTrackIcon,
  StopIcon,
  EjectIcon,
  SunIcon,
  MoonIcon,
  SeenMessageIcon,
  ContactIcon,
  OptOutIcon,
  SpinnerIcon,
  LoadingIcon,
  MoneyIcon,
  BookmarkIcon,
  HeartIcon,
  StarIcon,
  ShareIcon,
  PetIcon,
  ElevatorIcon,
  WarmPoolIcon,
  WetDeckIcon,
  RoofIcon,
  MeetingRoomIcon,
  ExclusiveGarageIcon,
  SoundIcon,
  SoundUpIcon,
  SoundDownIcon,
  SoundMutedIcon,
  CalendarIcon,
  BeautyCareIcon,
  OutdoorFitnessIcon,
  BeachCourtIcon,
}
