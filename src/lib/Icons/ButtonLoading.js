import React from 'react';

const ButtonLoading = ({className}) => (
  <svg className={className} width="16" height="16" viewBox="0 0 16 16" xmlns="http://www.w3.org/2000/svg" fill="currentColor">
    <rect x="0" y="4" width="4" height="8" rx="1">
      <animate
        attributeName="height"
        begin="0.5s"
        dur="1s"
        values="12;11;10;9;8;7;6;5;4;16;12"
        calcMode="linear"
        repeatCount="indefinite"
      />
      
      <animate
        attributeName="y"
        begin="0.5s"
        dur="1s"
        values="2;2.5;3;3.5;4;4.5;5;5.5;6;0;2"
        calcMode="linear"
        repeatCount="indefinite"
      />
    </rect>

    <rect x="6" y="2" width="4" height="12" rx="1">
      <animate
        attributeName="height"
        begin="0.25s"
        dur="1s"
        values="12;11;10;9;8;7;6;5;4;16;12"
        calcMode="linear"
        repeatCount="indefinite"
      />
      
      <animate
        attributeName="y"
        begin="0.25s"
        dur="1s"
        values="2;2.5;3;3.5;4;4.5;5;5.5;6;0;2"
        calcMode="linear"
        repeatCount="indefinite"
      />
    </rect>

    <rect x="12" y="0" width="4" height="16" rx="1">
      <animate
        attributeName="height"
        begin="0s"
        dur="1s"
        values="12;11;10;9;8;7;6;5;4;16;12"
        calcMode="linear"
        repeatCount="indefinite"
      />
      
      <animate
        attributeName="y"
        begin="0s"
        dur="1s"
        values="2;2.5;3;3.5;4;4.5;5;5.5;6;0;2"
        calcMode="linear"
        repeatCount="indefinite"
      />
    </rect>
    <rect x="0" y="0" width="100%" height="100%" fill="none" />
</svg>
)

export default ButtonLoading;