import React from 'react';

const MoreIcon = () => (
  <svg width="12" height="12" viewBox="0 0 12 12">
    <rect width="100%" height="100%" fill="none" />
    <path d="M 0 0 h 12 l -6 6 l -6 -6 z" fill="currentColor" stroke="none" />
  </svg>
);

export default MoreIcon;