import React from 'react';

const UploadIcon = props => (
  <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" className={props.className}>
    <path d="M 3,17 h 5 v 3 h 8 v -3 h 7 v 6 h -18 v -6 z M 18,20 v 1 h 1 v -1 h -1 z M 20,20 v 1 h 1 v -1 h -1 z" />
    <path d="M 9,19 h 6 v -5 h 5 l -8,-8 l -8,8 h 5 v 5 z" />
  </svg>
)

export default UploadIcon;
