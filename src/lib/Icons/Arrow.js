import React from 'react';

const ArrowIcon = () => (
  <svg width="16" height="16" viewBox="0 0 16 16">
    <path d="M 3 6 h 10 l -5 5 l -5 -5 z" fill="currentColor" stroke="none" />
  </svg>
);

export default ArrowIcon;
