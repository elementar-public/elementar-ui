import React from 'react';
import styled from 'styled-components';
import Theme from './Theme';

const ProgressBarContainer = styled.div`
  display: flex;
  overflow: hidden;
  height: ${props => props.height || '20px'};
  background: rgba(128,128,128,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.2);
  border-radius: ${props => props.rounded ? '99px' : '4px'};
  margin: 10px 0;
`;

const ProgressBarUnit = styled.div`
  background: ${props => props.invisible ? 'none' : props.theme.colors[props.color].regular};
  width: ${props => props.width + '%'};
  text-align: ${props => props.align || 'center'};
  line-height: ${props => props.height || '20px'};
  height: inherit;
  white-space: nowrap;
  font-size: 0.9rem;
  font-weight: bold;
  color: ${props => props.invisible ? 'rgba(0,0,0,0.5)' : 'white'};
  overflow: hidden;
  box-shadow: ${props => props.invisible ? 'none' : 'inset 0 0 0 1px rgba(0,0,0,0.1)'};
  flex: ${props => props.invisible ? '0 1 100%' : `0 0 ${props.width}%`};
  text-shadow: ${props => props.invisible ? 'none' : '0 -1px 0 rgba(0,0,0,0.2)'};
`;

const ProgressBarLabel = styled.span`
  display: ${props => props.labelHidden ? 'none' : 'inline'};
  padding: 0 4px;
`;

const getRemainingSpace = progress => {
  let sum = 0;

  progress.forEach(p => sum = sum + parseInt(p.progress));

  return 100 - sum;
}

const formatMessage = (message, progress) => message.replace(/\$\$/, progress);

const ProgressBar = ({
  progress,
  color,
  rounded,
  align,
  height,
  labelHidden,
  message,
  remaining,
  ...otherProps
}) => {
  if (Array.isArray(progress)) {
    return (
      <ProgressBarContainer
        rounded={rounded}
        height={height}
        {...otherProps}
      >
        { progress.map((p, i) => 
          <ProgressBarUnit
            key={i}
            width={p.progress}
            color={p.color}
            align={p.align}
            height={height}
          >
            <ProgressBarLabel labelHidden={p.labelHidden}>{p.message ? formatMessage(p.message, p.progress) : p.progress + '%'}</ProgressBarLabel>
          </ProgressBarUnit>
        )}
        { remaining && <ProgressBarUnit invisible height={height}><ProgressBarLabel>{typeof remaining === 'string' ? formatMessage(remaining, getRemainingSpace(progress)) : getRemainingSpace(progress) + '%'}</ProgressBarLabel></ProgressBarUnit>}
      </ProgressBarContainer>
    )
  } else if (typeof progress === 'object') {
    return (
      <ProgressBarContainer
        rounded={rounded}
        height={height}
        {...otherProps}
      >
        <ProgressBarUnit
          width={progress.progress}
          color={progress.color}
          align={progress.align}
          height={height}
        >
          <ProgressBarLabel labelHidden={progress.labelHidden}>{progress.message ? formatMessage(progress.message, progress.progress) : progress.progress + '%'}</ProgressBarLabel>
        </ProgressBarUnit>
        { remaining && <ProgressBarUnit invisible height={height}><ProgressBarLabel>{typeof remaining === 'string' ? formatMessage(remaining, 100 - progress.progress) : 100 - progress.progress + '%'}</ProgressBarLabel></ProgressBarUnit>}
      </ProgressBarContainer>
    )
  } else {
    return (
      <ProgressBarContainer
        rounded={rounded}
        height={height}
        {...otherProps}
      >
        <ProgressBarUnit
          width={progress}
          color={color}
          align={align}
          height={height}
        >
          <ProgressBarLabel labelHidden={labelHidden}>{message ? formatMessage(message, progress) : progress + '%'}</ProgressBarLabel>
        </ProgressBarUnit>
        { remaining && <ProgressBarUnit invisible height={height}><ProgressBarLabel>{typeof remaining === 'string' ? formatMessage(remaining, 100 - progress) : 100 - progress + '%'}</ProgressBarLabel></ProgressBarUnit>}
      </ProgressBarContainer>
    )
  }
}

ProgressBar.defaultProps = {
  height: '20px',
  color: 'default',
  align: 'center',
  labelHidden: false,
  rounded: false,
  message: null,
  remaining: false,
  progress: 0,
  theme: {
    colors: Theme.colors
  }
}

export default ProgressBar;