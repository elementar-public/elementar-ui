import React from 'react';
import styled from 'styled-components';

const Icon = ({ className }) => (
  <svg viewBox="0 0 120 120" width="120" height="120" className={className}>
    <circle cx="60" cy="60" r="58" fill="rgba(0,0,0,0.5)" stroke="white" strokeWidth="1" />
    <path d="M 38,30 l 52,30 l -52,30 z" fill="none" stroke="white" strokeWidth="1" />
  </svg>
);

const VideoBox = styled.div`
  width: 100%;
  max-height: ${({ maxHeight }) => maxHeight || 'none'};
  overflow: hidden;
  position: relative;
`;

const VideoContainer = styled.div`
  width: 100%;
  height: 0;
  padding-bottom: ${({ height }) => height};
  background: black;
  background-size: contain;

  &.visible {
    background: black url('https://img.youtube.com/vi/${({ videoId }) => videoId}/0.jpg') no-repeat center center;
    background-size: ${({ cover }) => cover ? 'cover' : 'auto'};
  }

  iframe {
    width: 100%;
    height: 100%;
    max-height: ${({ maxHeight }) => maxHeight || 'none'};
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
  }
`;

const VideoButton = styled.button`
  padding: 0;
  margin: -60px 0 0 -60px;
  border: none;
  background: none;
  position: absolute;
  top: 50%;
  left: 50%;
  width: 120px;
  height: 120px;
  cursor: pointer;
  opacity: 0.8;

  &:hover {
    opacity: 1;
  }
`;

const VideoButtonIcon = styled(Icon)`
  pointer-events: none;
`;

export default class YouTubeVideo extends React.Component {
  constructor(props) {
    super(props);
    this.videoContainer = React.createRef();
  }

  componentDidMount() {
    if (this.props.autoPlay)
      this.loadYouTubeScript();
    else
      this.videoContainer.current.classList.add('visible');
  }

  handlePlayButton = e => {
    e.target.remove();
    this.loadYouTubeScript();
  }

  loadYouTubeScript = () => {
    if (!window.YT) {
      const tag = document.createElement('script');
      tag.src = 'https://www.youtube.com/iframe_api';

      window.onYouTubeIframeAPIReady = this.loadVideo;

      const firstScriptTag = document.getElementsByTagName('script')[0];
      firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
    } else {
      this.loadVideo();
    }
  }

  loadVideo = () => {
    const { videoId, noControls } = this.props;

    this.player = new window.YT.Player(this.youtubePlayerAnchor, {
      videoId,
      playerVars: {
        modestbranding: true,
        controls: noControls ? 0 : 1,
        showinfo: 0,
        autoplay: 1
      },
      events: {
        onStateChange: this.onPlayerStateChange
      }
    });
  }

  onPlayerStateChange = event => {
    if(event.data === 0) {
      event.target.stopVideo();
    }
  }

  render() {
    return (
      <VideoBox maxHeight={this.props.maxHeight} {...this.props.otherProps}>
        <VideoContainer
          ref={this.videoContainer}
          videoId={this.props.videoId}
          height={this.props.height}
          maxHeight={this.props.maxHeight}
          cover={this.props.cover}
        >
          { !this.props.autoPlay &&
            <VideoButton type="button" aria-label="Reproduzir" onClick={e => this.handlePlayButton(e)}>
              <VideoButtonIcon />
            </VideoButton>
          }
          <div ref={e => this.youtubePlayerAnchor = e} />
        </VideoContainer>
      </VideoBox>
    )
  }
}

YouTubeVideo.defaultProps = {
  videoId: 'oavMtUWDBTM',
  noControls: false,
  autoPlay: false,
  height: '56.25%',
  cover: false
}
