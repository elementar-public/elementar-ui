import React from 'react';
import styled from 'styled-components';

const SkeletonContainer = styled.svg`
  margin: 0;
  padding: 0;
  display: block;
`;

const SkeletonLine = ({ lines, id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    {lines.map((line, i) =>
      <rect key={i} width={`calc(${line}% - 8px)`} height="8" x="4" y={(i * 16) + 8} rx="4" />
    )}
  </clipPath>
);

const SkeletonAvatar = ({ withName, id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    <circle cx="24" cy="24" r="20" />
    {withName && <rect width="calc(100% - 56px)" height="12" x="52" y="11" rx="6" /> }
    {withName && <rect width="calc(100% - 96px)" height="8" x="52" y="31" rx="4" /> }
  </clipPath>
);

const SkeletonCard = ({ height, withLabel, id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    <rect width="100" height={height} x="calc(50% - 50px)" y="4" rx="6" />
    {withLabel && <rect width="120" height="8" x="calc(50% - 60px)" y={`calc(${isNaN(height) ? height : height + 'px'} + 12px)`} rx="4" /> }
  </clipPath>
);

const SkeletonTable = ({ withAction, id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    <rect width={`calc(100% - ${withAction ? '48px' : '8px'})`} height="12" x="4" y="14" rx="6" />
    {withAction && <circle cx="calc(100% - 20px)" cy="20" r="16" />}
  </clipPath>
);

const SkeletonMenu = ({ id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    <rect width="calc(100% - 48px)" height="16" x="44" y="12" rx="8" />
    <circle cx="20" cy="20" r="16" />
  </clipPath>
);

const SkeletonIcon = ({ height, id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    <rect width="30" height="20" x="calc(50% - 60px)" y={height > 200 ? (height / 2) + 20 : '120'} rx="5" />
    <rect width="30" height="50" x="calc(50% - 15px)" y={height > 200 ? (height / 2) - 10 : '90'} rx="5" />
    <rect width="30" height="80" x="calc(50% + 30px)" y={height > 200 ? (height / 2) - 40 : '60'} rx="5" />
  </clipPath>
);

const SkeletonBlock = ({ id }) => (
  <clipPath id={`SkeletonClip${id}`}>
    <rect width="100%" height="100%" x="0" y="0" rx="4" />
  </clipPath>
);

const Skeleton = ({
  type,
  width,
  withName,
  withLabel,
  withAction,
  lines,
  height,
  style,
  ...otherProps
}) => {
  const getHeight = () => {
    switch (type) {
      case 'line':
        return ((lines.length * 16) + 8);
      case 'avatar':
        return 48;
      case 'card':
        return (height + 12 + (withLabel ? 12 : 0));
      case 'block':
        return height;
      case 'icon':
        return (height > 200 ? height : 200);
      default:
        return 40;
    }
  }

  const randomNumber = parseInt(Math.random() * 10**10);

  return (
    <SkeletonContainer
      width={width}
      height={getHeight()}
      style={style}
      {...otherProps}
    >
      <defs>
        <linearGradient id={`SkeletonGradient${randomNumber}`} x1="-100%" x2="0%">
          <stop offset="0%" stopColor="#99999944" />
          <stop offset="50%" stopColor="#99999922" />
          <stop offset="100%" stopColor="#99999944" />

          <animate id="gradient" attributeName="x1" values="-100%;100%" dur="1s" repeatCount="indefinite" />
          <animate id="gradient" attributeName="x2" values="0%;200%" dur="1s" repeatCount="indefinite" />
        </linearGradient>

        {type === 'line' && <SkeletonLine lines={lines} id={randomNumber} />}
        {type === 'avatar' && <SkeletonAvatar withName={withName} id={randomNumber} />}
        {type === 'card' && <SkeletonCard withLabel={withLabel} height={height} id={randomNumber} />}
        {type === 'table' && <SkeletonTable withAction={withAction} id={randomNumber} />}
        {type === 'menu' && <SkeletonMenu id={randomNumber} />}
        {type === 'icon' && <SkeletonIcon height={height} id={randomNumber} />}
        {type === 'block' && <SkeletonBlock id={randomNumber} />}
      </defs>

      <rect width="100%" height="100%" x="0" y="0" fill={`url(#SkeletonGradient${randomNumber})`} clipPath={`url(#SkeletonClip${randomNumber})`} />
    </SkeletonContainer>
  );
}

Skeleton.defaultProps = {
  type: 'block',
  width: '100%',
  lines: [90, 100, 60],
  height: 50,
  withName: false,
  withLabel: false,
  withAction: false
}

export default Skeleton;
