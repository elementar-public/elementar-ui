import React from 'react';
import styled from 'styled-components';
import { Link } from 'react-router-dom';
import ButtonLoading from './Icons/ButtonLoading';

type ButtonProps = {
  children?: React.ReactNode,
  className?: string,
  label?: string,
  to?: string,
  type?: 'reset' | 'button' | 'submit',
  href?: string,
  uppercase?: boolean,
  theme?: any,
  active?: boolean,
  rounded?: boolean,
  radius?: string,
  outline?: boolean,
  light?: boolean,
  leftIcon?: React.FunctionComponent | React.ComponentClass,
  rightIcon?: React.FunctionComponent | React.ComponentClass,
  disabled?: boolean,
  loading?: boolean,
  fullWidth?: boolean,
  noWrap?: boolean,
  [otherProps: string]: any
}

const BasicButton: any = React.forwardRef(({
  type = 'button',
  label = 'Ação',
  light = undefined,
  className,
  children,
  to,
  href,
  uppercase,
  theme,
  active,
  rounded,
  radius,
  outline,
  leftIcon,
  rightIcon,
  disabled,
  loading,
  fullWidth,
  noWrap,
  ...otherProps
}: ButtonProps, ref: any) => {
  if (to) {
    return React.createElement(
      Link,
      { className: (active ? `${className} active` : className), to, ref, ...otherProps },
      [
        ((leftIcon) && React.createElement(leftIcon, { key: 'left-icon' })),
        (React.createElement('span', {key: 'span', className: 'label'}, (children || label))),
        ((rightIcon) && React.createElement(rightIcon, { key: 'right-icon' }))
      ]
    );
  } else {
    return React.createElement(
      (href ? 'a' : 'button'),
      {
        className: (active ? className + ' active' : className),
        type: (href ? null : type),
        href: (href || null),
        ref,
        disabled: (disabled || loading),
        ...otherProps
      },
      [
        ((leftIcon) && React.createElement(leftIcon, { key: 'left-icon' })),
        (React.createElement('span', {key: 'span', className: 'label'}, (children || label))),
        ((rightIcon) && React.createElement(rightIcon, { key: 'right-icon' })),
        ((loading) && React.createElement(ButtonLoading, { key: 'loading', className: 'loading' }))
      ]
    );
  }
});

const Button = styled(BasicButton).attrs(({ rounded, radius, theme, dock }) => {
  switch (dock) {
    case 'left':
      return { style: { borderRadius: rounded ? '99px 0 0 99px' : `${ radius || theme.buttons.borderRadius} 0 0 ${radius || theme.buttons.borderRadius }` }}
    case 'right':
      return { style: { borderRadius: rounded ? '0 99px 99px 0' : `0 ${ radius || theme.buttons.borderRadius} ${radius || theme.buttons.borderRadius} 0`, marginLeft: -1 }}
    default:
      return { style: { borderRadius: rounded ? '99px' : (radius || theme.buttons.borderRadius)}}
  }
})`
  font-size: ${({ theme, size }) => size ? theme.buttons.sizes[size].fontSize : theme.buttons.sizes.default.fontSize};
  line-height: ${({ theme, size }) => size ? theme.buttons.sizes[size].lineHeight : theme.buttons.sizes.default.lineHeight};
  text-transform: ${({ uppercase }) => uppercase ? 'uppercase' : 'none'};
  white-space: ${({ noWrap }) => noWrap ? 'nowrap' : 'normal'};
  width: ${({ fullWidth }) => fullWidth ? '100%' : 'auto'};
  font-weight: ${({ light }) => light ? 'normal' : 'bold'};
  font-family: ${({ theme }) => theme.typography.fontFamily};
  display: flex;
  align-items: center;
  justify-content: center;
  vertical-align: top;
  text-decoration: none;
  letter-spacing: -0.5px;
  transition: background 0.2s, box-shadow 0.2s;
  overflow: hidden;
  box-sizing: border-box;
  cursor: pointer;
  text-align: center;
  position: relative;

  ${({ outline, color, size, theme }) => ((outline) ? `
    background: none;
    color: ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
    padding: ${(size) ? theme.buttons.sizes[size].outline : theme.buttons.sizes.default.outline};
    border: 1px solid ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
    box-shadow: 0 1px 2px ${(color) ? theme.colors[color].regular + (theme.colors[color].regular.length > 4 ? '55' : '5') : theme.colors.default.regular + (theme.colors.default.regular.length > 4 ? '55' : '5')};

    &:hover,
    &.hover {
      border-color: ${(color) ? theme.colors[color].hover : theme.colors.default.hover};
      color: ${(color) ? theme.colors[color].hover : theme.colors.default.hover};
      padding: ${(size) ? theme.buttons.sizes[size].outlineHover : theme.buttons.sizes.default.outlineHover};
      border-width: 2px;
      box-shadow: 0 2px 4px ${(color) ? theme.colors[color].regular + (theme.colors[color].regular.length > 4 ? '66' : '6') : theme.colors.default.regular + (theme.colors.default.regular.length > 4 ? '66' : '6')};
    }
    
    &:active,
    &.active {
      border-color: ${(color) ? theme.colors[color].active : theme.colors.default.active};
      color: ${(color) ? theme.colors[color].active : theme.colors.default.active};
      padding: ${(size) ? theme.buttons.sizes[size].outlineHover : theme.buttons.sizes.default.outlineHover};
      border-width: 2px;
      box-shadow: 0 1px 1px ${(color) ? theme.colors[color].regular + (theme.colors[color].regular.length > 4 ? '44' : '4') : theme.colors.default.regular + (theme.colors.default.regular.length > 4 ? '44' : '4')};
    }
  `
  :
  `
    background: ${(color) ? theme.colors[color].regular : theme.colors.default.regular};
    color: ${(color) ? theme.colors[color].fontColor : theme.colors.default.fontColor};
    padding: ${(size) ? theme.buttons.sizes[size].regular : theme.buttons.sizes.default.regular};
    border: none;
    box-shadow: 0 1px 2px ${(color) ? theme.colors[color].regular + (theme.colors[color].regular.length > 4 ? '55' : '5') : theme.colors.default.regular + (theme.colors.default.regular.length > 4 ? '55' : '5')}, inset 0 0 0 1px rgba(0, 0, 0, 0.1);

    &:hover,
    &.hover {
      background: ${(color) ? theme.colors[color].hover : theme.colors.default.hover};
      color: ${(color) ? theme.colors[color].fontColorHover : theme.colors.default.fontColorHover};
      box-shadow: 0 2px 4px ${(color) ? theme.colors[color].regular + (theme.colors[color].regular.length > 4 ? '66' : '6') : theme.colors.default.regular + (theme.colors.default.regular.length > 4 ? '66' : '6')}, inset 0 0 0 1px rgba(0, 0, 0, 0.2);
    }

    &:active,
    &.active {
      background: ${(color) ? theme.colors[color].active : theme.colors.default.active};
      color: ${(color) ? theme.colors[color].fontColorActive : theme.colors.default.fontColorActive};
      box-shadow: 0 1px 1px ${(color) ? theme.colors[color].regular + (theme.colors[color].regular.length > 4 ? '44' : '4') : theme.colors.default.regular + (theme.colors.default.regular.length > 4 ? '44' : '4')}, inset 0 0 0 1px rgba(0, 0, 0, 0.2);
    }
  `
  )}

  .label {
    padding: 0 4px;
  }

  &:disabled,
  &.disabled {
    cursor: default;
    pointer-events: none;
    
    .label,
    svg:not(.loading) {
      opacity: 0.4;
    }
  }

  * {
    pointer-events: none;
  }

  svg:not(.loading) {
    vertical-align: -2px;
  }

  .icon-left {
    margin: 0 4px 0 0;
  }
  
  .icon-right {
    margin: 0 0 0 4px;
  }

  .loading {
    position: absolute;
    left: 50%;
    top: 50%;
    transform: translate(-50%, -50%);
    opacity: 1;
  }

  & + [class*=${props => props.className}] {
    margin-left: 4px;
  }
`;

export default Button;
