import React from 'react';
import { renderToString } from 'react-dom/server'
import styled from 'styled-components';

const GoogleMapBox = styled.div`
  width: 100%;
  max-height: ${({ maxHeight }) => maxHeight || 'none'};
  overflow: hidden;
`;

const GoogleMapContainer = styled.div`
  width: 100%;
  position: relative;
  background-color: black;
  padding-bottom: ${({ height }) => height};

  > div {
    width: 100%;
    height: 100%;
    display: block;
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;

    ${({ maxHeight }) => maxHeight && `max-height: ${maxHeight}`}
  }
`;

const createHTMLMapMarker = ({
  OverlayView = window.google.maps.OverlayView,
  ...args
}) => {
  class HTMLMapMarker extends OverlayView {
    constructor() {
      super();
      this.latlng = args.latlng;
      this.html = args.html;
      this.setMap(args.map);
    }

    createDiv() {
      this.div = document.createElement("div");
      this.div.style.position = "absolute";
      if (this.html) {
        this.div.innerHTML = this.html;
      }
      window.google.maps.event.addDomListener(this.div, "click", event => {
        window.google.maps.event.trigger(this, "click");
      });
    }

    appendDivToOverlay() {
      const panes = this.getPanes();
      panes.overlayImage.appendChild(this.div);
    }

    positionDiv() {
      const point = this.getProjection().fromLatLngToDivPixel(this.latlng);
      let offset = 25;
      if (point) {
        this.div.style.left = `${point.x - offset}px`;
        this.div.style.top = `${point.y - offset}px`;
      }
    }

    draw() {
      if (!this.div) {
        this.createDiv();
        this.appendDivToOverlay();
      }
      this.positionDiv();
    }

    remove() {
      if (this.div) {
        this.div.parentNode.removeChild(this.div);
        this.div = null;
      }
    }

    getPosition() {
      return this.latlng;
    }

    getDraggable() {
      return false;
    }
  }

  return new HTMLMapMarker();
};

export default class GoogleMap extends React.Component {
  componentDidMount() {
    if (!window.google && !this.props.blockScriptLoad) {
      var s = document.createElement('script');
      s.type = 'text/javascript';
      s.src = `https://maps.google.com/maps/api/js?key=${this.props.apiKey}&callback=Function.prototype&v=3`;
      var x = document.getElementsByTagName('script')[0];
      x.parentNode.insertBefore(s, x);

      s.addEventListener('load', this.onScriptLoad);
    } else {
      this.onScriptLoad()
    }
  }

  onScriptLoad = () => {
    const { zoom, hideZoomControl, hideFullScreenControl, marker, lat, lng, styles, children } = this.props;

    const map = new window.google.maps.Map(this.mapContainer, {
      center: { lat: parseFloat(lat), lng: parseFloat(lng) },
      styles,
      zoom,
      zoomControl: !hideZoomControl,
      mapTypeControl: false,
      scaleControl: true,
      streetViewControl: false,
      rotateControl: true,
      fullscreenControl: !hideFullScreenControl
    });

    if (marker) {
      if (Array.isArray(marker)) {
        marker.forEach((m, i) => {
          new window.google.maps.Marker({
            position: { lat: parseFloat(m.lat || lat), lng: parseFloat(m.lng || lng) },
            map,
            title: (m.title || `Marcador ${i+1}`)
          });
        });
      } else {
        new window.google.maps.Marker({
          position: { lat: parseFloat(marker.lat || lat), lng: parseFloat(marker.lng || lng) },
          map,
          title: (marker.title || 'Marcador')
        });
      }
    };

    if (children) {
      React.Children.map(children, c => {
        let latlng = new window.google.maps.LatLng(parseFloat(c.props.lat || lat), parseFloat(c.props.lng || lng));
        let html   = renderToString(c);

        return createHTMLMapMarker({ latlng, map, html });
      });
    }
  }

  render() {
    return React.createElement(
      GoogleMapBox,
      {
        maxHeight: this.props.maxHeight
      },
      React.createElement(
        GoogleMapContainer,
        {
          height: this.props.height,
          maxHeight: this.props.maxHeight,
          ref: (e => this.mapContainer = e)
        },
        this.props.children
      )
    )
  }
};

GoogleMap.defaultProps = {
  height: '56.25%',
  zoom: 11,
  hideZoomControl: false,
  hideFullScreenControl: false,
  lat: -15.793889,
  lng: -47.882778,
  blockScriptLoad: false
};
