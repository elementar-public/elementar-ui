import React from 'react';
import styled from 'styled-components';
import Theme from './Theme';

const BasicGrid = ({ className,
    children,
    element,
    container,
    breakpoint,
    reverse,
    row,
    xs,
    sm,
    md,
    lg,
    xl,
    xsOffsetLeft,
    xsOffsetRight,
    smOffsetLeft,
    smOffsetRight,
    mdOffsetLeft,
    mdOffsetRight,
    lgOffsetLeft,
    lgOffsetRight,
    xlOffsetLeft,
    xlOffsetRight,
    theme,
    ...otherProps
  }) => React.createElement(
  (element || 'div'),
  {
    className: className,
    ...otherProps
  },
  children
);

const Grid = styled(BasicGrid)`
  ${({container, breakpoint, theme}) => container && `
    display: block;
    max-width: ${breakpoint ? theme.breakpoints[breakpoint].width : '100%'};
    width: 100%;
    padding: 0;
    margin: 0 auto;
    list-style: none;
  `}

  ${({row, reverse, gutter}) => row && `
    display: flex;
    flex-wrap: wrap;
    box-sizing: border-box;
    flex-direction: ${reverse ? 'row-reverse' : 'row'};
    margin: ${gutter ? ('0 -' + (gutter / 2) + 'px') : '0'};
    padding: 0;
    list-style: none;

    & > * {
      padding: ${gutter ? ('0 ' + (gutter / 2) + 'px') : '0'};
    }
  `}

  ${({container, row, xs, xsOffsetLeft, xsOffsetRight, sm, smOffsetLeft, smOffsetRight, md, mdOffsetLeft, mdOffsetRight, lg, lgOffsetLeft, lgOffsetRight, xl, xlOffsetLeft, xlOffsetRight, theme}) => (!container && !row) &&`
    width: ${(xs) && xs !== 'hidden' ? ((xs) / 12 * 100) + '%' : '100%'};
    max-width: ${(xs) && xs !== 'hidden' ? ((xs) / 12 * 100) + '%' : '100%'};
    flex-basis: ${(xs) && xs !== 'hidden' ? ((xs) / 12 * 100) + '%' : '100%'};
    flex-grow: 0;
    flex-shrink: 0;
    box-sizing: border-box;
    list-style: none;
    margin-left: ${(xsOffsetLeft) ? ((xsOffsetLeft) / 12 * 100) + '%' : '0'};
    margin-right: ${(xsOffsetRight) ? ((xsOffsetRight) / 12 * 100) + '%' : '0'};
    min-width: 0;

    @media ${theme.breakpoints.xs.queryStrict} {
      display: ${xs === 'hidden' ? 'none' : 'initial'};
    }

    @media ${theme.breakpoints.sm.queryStrict} {
      display: ${sm === 'hidden' ? 'none' : 'initial'};
    }

    @media ${theme.breakpoints.md.queryStrict} {
      display: ${md === 'hidden' ? 'none' : 'initial'};
    }

    @media ${theme.breakpoints.lg.queryStrict} {
      display: ${lg === 'hidden' ? 'none' : 'initial'};
    }

    @media ${theme.breakpoints.xl.queryStrict} {
      display: ${xl === 'hidden' ? 'none' : 'initial'};
    }

    @media ${theme.breakpoints.sm.queryRecursive} {
      width: ${sm && (sm === 'hidden' ? '0' : ((sm) / 12 * 100) + '%')};
      max-width: ${sm && (sm === 'hidden' ? '0' : ((sm) / 12 * 100) + '%')};
      flex-basis: ${sm && (sm === 'hidden' ? '0' : ((sm) / 12 * 100) + '%')};
      margin-left: ${(smOffsetLeft) ? ((smOffsetLeft) / 12 * 100) + '%' : '0'};
      margin-right: ${(smOffsetRight) ? ((smOffsetRight) / 12 * 100) + '%' : '0'};
    }

    @media ${theme.breakpoints.md.queryRecursive} {
      width: ${md && (md === 'hidden' ? '0' : ((md) / 12 * 100) + '%')};
      max-width: ${md && (md === 'hidden' ? '0' : ((md) / 12 * 100) + '%')};
      flex-basis: ${md && (md === 'hidden' ? '0' : ((md) / 12 * 100) + '%')};
      margin-left: ${(mdOffsetLeft) ? ((mdOffsetLeft) / 12 * 100) + '%' : '0'};
      margin-right: ${(mdOffsetRight) ? ((mdOffsetRight) / 12 * 100) + '%' : '0'};
    }

    @media ${theme.breakpoints.lg.queryRecursive} {
      width: ${lg && (lg === 'hidden' ? '0' : ((lg) / 12 * 100) + '%')};
      max-width: ${lg && (lg === 'hidden' ? '0' : ((lg) / 12 * 100) + '%')};
      flex-basis: ${lg && (lg === 'hidden' ? '0' : ((lg) / 12 * 100) + '%')};
      margin-left: ${(lgOffsetLeft) ? ((lgOffsetLeft) / 12 * 100) + '%' : '0'};
      margin-right: ${(lgOffsetRight) ? ((lgOffsetRight) / 12 * 100) + '%' : '0'};
    }

    @media ${theme.breakpoints.xl.queryRecursive} {
      width: ${xl && (xl === 'hidden' ? '0' : ((xl) / 12 * 100) + '%')};
      max-width: ${xl && (xl === 'hidden' ? '0' : ((xl) / 12 * 100) + '%')};
      flex-basis: ${xl && (xl === 'hidden' ? '0' : ((xl) / 12 * 100) + '%')};
      margin-left: ${(xlOffsetLeft) ? ((xlOffsetLeft) / 12 * 100) + '%' : '0'};
      margin-right: ${(xlOffsetRight) ? ((xlOffsetRight) / 12 * 100) + '%' : '0'};
    }
  `}
`;

Grid.defaultProps = {
  gutter: 20,
  theme: {
    breakpoints: Theme.breakpoints
  },
}

export default Grid;
