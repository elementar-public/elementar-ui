import React from 'react';
import styled from 'styled-components';
import Theme from './Theme';
import ErrorIcon from './Icons/Error';
import WarningIcon from './Icons/Warning';

const ErrorContainer = styled.div`
  margin: 0;
  padding: 10px;
  border: 1px solid ${({ type, theme }) => theme.colors[type].regular};
  border-radius: 4px;
  display: flex;
  align-items: center;

  svg {
    flex: 0 0 auto;
    margin-right: 10px;
    color: ${({ type, theme }) => theme.colors[type].regular};
  }

  p {
    font-size: 0.85rem;
    margin: 0;
  }

  details {
    margin-top: 10px;
    cursor: pointer;

    p {
      margin-top: 10px;
    }
  }

  summary {
    font-size: 0.8rem;
    font-weight: bold;
  }

  hr {
    height: 0;
    border: none;
    margin: 10px 0;
    border-top: 1px solid ${({ theme }) => theme.colors.dimmed.active};
  }
`;

const ErrorBox = ({
  type,
  error,
  errorInfo,
  message,
  style,
  ...otherProps
}) => (
  <ErrorContainer type={type} style={style} {...otherProps}>
    { type === 'danger' ? <ErrorIcon /> : <WarningIcon /> }
    <div>
      <p>{message || 'Aconteceu um erro inesperado.'}</p>

      { errorInfo && process.env.NODE_ENV !== 'production' &&
        <details>
          <summary>Detalhes do erro</summary>
          <p>{error ? error.toString() : 'Um erro desconhecido foi encontrado'}</p>
          {errorInfo && <hr/>}
          {errorInfo && <p><small>{errorInfo.componentStack}</small></p>}
        </details>
      }
    </div>
  </ErrorContainer>
);

class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      error: null,
      errorInfo: null,
    };
  }

  componentDidCatch(error, errorInfo) {
    this.setState({ error, errorInfo });

    console.error(error.toString(), errorInfo.componentStack);
  }

  render() {
    if (this.state.error) return <ErrorBox error={this.state.error} errorInfo={this.state.errorInfo} />;
    if (typeof this.props.children === 'undefined') return <ErrorBox error="Sem conteúdo" />;

    return this.props.children;
  }
}

ErrorBox.defaultProps = {
  type: 'danger',
  message: null,
  error: null,
  style: null,
  theme: {
    colors: Theme.colors,
  }
}

export default ErrorBox;
export { ErrorBoundary };
