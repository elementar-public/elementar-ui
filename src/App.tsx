import React, { useEffect } from 'react';
import { HashRouter as Router, Route, Switch, useLocation } from 'react-router-dom';
import styled, { ThemeProvider } from 'styled-components';
import Theme from './lib/Theme';
import Header from './components/header';

import HomeView from './views/home';
import FoundationView from './views/foundation';
import LayoutView from './views/layout';
import ComponentsView from './views/components';
import FormsView from './views/forms';
import ModalsView from './views/modals';
import HelpersView from './views/helpers';
import UtilitiesView from './views/utilities';
import NotFoundView from './views/404';

const theme = Theme;

theme.typography.fontFamily = '"IBM Plex Sans", -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif';

const AppContainer = styled.div`
  padding: 0 20px;
`;

const ScrollToTop = () => {
  const { pathname } = useLocation();

  useEffect(() => window.scrollTo(0,0), [pathname]);

  return null;
}

const App = () => (
  <ThemeProvider theme={theme}>
    <Router>
      <ScrollToTop />
      <Header
        items={[
          {value: '/fundamentos', label: 'Fundamentos'},
          {value: '/layout', label: 'Layout'},
          {value: '/componentes', label: 'Componentes'},
          {value: '/forms', label: 'Forms'},
          {value: '/modais', label: 'Modais'},
          {value: '/helpers', label: 'Helpers'},
          {value: '/utilidades', label: 'Utilidades'}
        ]}
      />

      <AppContainer>
        <Switch>
          <Route path="/" exact component={HomeView} />
          <Route path="/fundamentos" exact component={FoundationView} />
          <Route path="/layout" exact component={LayoutView} />
          <Route path="/componentes" exact component={ComponentsView} />
          <Route path="/forms" exact component={FormsView} />
          <Route path="/modais" exact component={ModalsView} />
          <Route path="/helpers" exact component={HelpersView} />
          <Route path="/utilidades" exact component={UtilitiesView} />
          <Route component={NotFoundView} status={404} />
        </Switch>
      </AppContainer>
    </Router>
  </ThemeProvider>
);

export default App;
