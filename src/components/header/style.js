import styled from 'styled-components';

const HeaderBar = styled.header`
  position: sticky;
  top: 0;
  z-index: 1;
  height: 60px;
  background: white;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0 2px 4px rgba(0,0,0,0.1);
`;

const HeaderLogo = styled.h1`
  margin: 0;
  padding: 6px 0;
  flex: 0 0 auto;
  height: 48px;

  img {
    display: block;
    height: 48px;
  }
`;

const HeaderMenu = styled.ul`
  margin: 0 8px;
  padding: 0;
  flex: 0 0 auto;
  display: flex;
  list-style: none;
  height: 60px;

  a {
    display: block;
    line-height: 60px;
    padding: 0 8px;
    text-decoration: none;
    color: #677;

    &:hover {
      color: #455;
    }

    &.active {
      border-bottom: 4px solid #097;
    }
  }

  @media screen and (max-width: 799px) {
    visibility: ${({active}) => active ? 'visible' : 'hidden'};
    flex-direction: column;
    background: white;
    position: absolute;
    top: 70px;
    right: 0;
    height: auto;
    width: 160px;
    box-shadow: 0 2px 4px rgba(0,0,0,0.1);
    opacity: ${({active}) => active ? 1 : 0 };
    transition: all 0.25s ease-in-out;

    a {
      line-height: 40px;

      &.active {
        border: none;
        border-left: 4px solid #097;
      }
    }
  }
`;

const HeaderToggler = styled.button`
  background: none;
  border: none;
  display: none;
  width: 32px;
  height: 32px;
  padding: 4px;
  margin: 0 10px 0 0;
  cursor: pointer;
  color: #677;

  &:hover,
  &:active {
    color: #455;
  }

  svg {
    display: block;
    pointer-events: none;
  }

  @media screen and (max-width: 799px) {
    display: block;
  }
`;

export {
  HeaderBar,
  HeaderLogo,
  HeaderMenu,
  HeaderToggler
};