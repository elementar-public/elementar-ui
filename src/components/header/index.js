import React, {useState, useEffect, useRef, useCallback} from 'react';
import {Link, NavLink} from 'react-router-dom'
import Logo from '../../assets/logo.svg';
import {MenuIcon, CloseIcon} from '../../lib/IconLibrary';
import {
  HeaderBar,
  HeaderLogo,
  HeaderMenu,
  HeaderToggler
} from './style';

const Header = ({items}) => {
  const [menu, toggleMenu] = useState(false);
  const menuRef = useRef();
  const buttonRef = useRef();

  const controlMenu = () => toggleMenu(!menu);

  const getClick = useCallback(e => {
    if (menuRef.current && !menuRef.current.contains(e.target) && !buttonRef.current.contains(e.target) && menu && true) toggleMenu(false);
  }, [menu]);

  useEffect(() => {
    document.addEventListener('mousedown', getClick, false);

    return () => document.removeEventListener('mousedown', getClick, false);
  }, [getClick]);

  return (
    <HeaderBar>
      <HeaderLogo>
        <Link to="/">
          <img src={Logo} alt="Elementar-UI" />
        </Link>
      </HeaderLogo>
  
      <nav>
        <HeaderToggler type="button" onClick={controlMenu} ref={buttonRef}>
          {menu
            ?
            <CloseIcon />
            :
            <MenuIcon />
          }
        </HeaderToggler>
        <HeaderMenu active={menu} ref={menuRef}>
          {items.length > 0 ? items.map(i => <li key={i.value}><NavLink to={i.value} onClick={() => toggleMenu(false)}>{i.label}</NavLink></li>) : <li>Sem itens</li>}
        </HeaderMenu>
      </nav>
    </HeaderBar>
  );
}

export default Header;