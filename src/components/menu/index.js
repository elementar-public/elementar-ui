import React, {useEffect, useRef} from 'react';
import Grid from '../../lib/Grid';
import {
  MenuBar,
  MenuTitle,
  MenuList
} from './style';

const Header = ({title, children}) => {
  const menuRef = useRef();

  const testFunction = e => {
    if (e.target.tagName === 'A') {
      e.preventDefault();
      const sectionName = e.target.hash.replace(/#/, '');
      const sectionEl = document.getElementById(sectionName);

      window.scrollTo({top: sectionEl.offsetTop - 80, behavior: 'smooth'});
    }
  }

  useEffect(() => {
    const menuEl = menuRef.current;

    menuEl.addEventListener('click', testFunction);

    return () => menuEl.removeEventListener('click', testFunction);
  })

  return (
    <Grid container breakpoint="md">
      <MenuBar>
        <MenuTitle>{title || 'Título'}</MenuTitle>
        <MenuList ref={menuRef}>{children || <li>Sem itens</li>}</MenuList>
      </MenuBar>
    </Grid>
  );
}

export default Header;