import styled from 'styled-components';

const MenuBar = styled.div`
  margin-top: 20px;
  border-radius: 8px;
  background: rgba(0,0,0,0.05);
  padding: 20px;
  display: flex;
  justify-content: space-between;
`;

const MenuTitle = styled.h2`
  flex: 0 1 auto;
  width: 100%;
  font-size: clamp(1.2rem, 4vw, 10rem);
  line-height: clamp(1.2rem, 4vw, 10rem);
  letter-spacing: -0.15vw;
  min-width: 0;
  overflow: hidden;
  white-space: nowrap;
  text-overflow: ellipsis;
  font-weight: bolder;
  margin: 0 10px 0 0;
  padding: 0 0 10px;
`;

const MenuList = styled.ul`
  flex: 0 0 auto;
  width: 150px;
  list-style: none;
  font-size: 0.85rem;
  margin: 0;
  padding: 0;

  a {
    display: block;
    margin-bottom: 5px;
    color: black;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
`;

export {
  MenuBar,
  MenuTitle,
  MenuList
}