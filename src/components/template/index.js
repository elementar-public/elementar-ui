import React from 'react';
import Box from '../../lib/Box';
import Grid from '../../lib/Grid';
import {
  TemplateTitle as Title,
  TemplateCodeBox as CodeBox,
  TemplateTable as TableBox,
  TemplateBack
} from './style';

const BackIcon = () => (
  <svg width="20" height="20" viewBox="0 0 20 20">
    <path d="M 1 14 l 9 -9 l 9 9" fill="none" stroke="black" strokeWidth="1" />
  </svg>
);

const DemoBox = ({children}) => <Box elevation="2" spacing="20">{children}</Box>

const Template = ({ title, id, children }) => {
  const returnToTop = e => {
    e.preventDefault();

    window.scrollTo({top: 0, behavior: 'smooth'});
  }

  return React.createElement(
    Grid,
    {
      container: true,
      element: 'article',
      breakpoint: 'md',
      id: id
    },
    [
      React.createElement(Title, { key: title }, title),
      children,
      React.createElement(TemplateBack, {key: 'back-' + title, href: '#', alt: 'Voltar ao topo', onClick: returnToTop}, React.createElement(BackIcon))
    ]
  );
}

export default Template;
export {
  CodeBox,
  DemoBox,
  TableBox
}
