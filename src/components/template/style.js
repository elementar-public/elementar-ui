import styled from 'styled-components';

const TemplateTitle = styled.h2`
  margin: 80px 0 40px;
  font-size: 3rem;
  line-height: 3rem;
  letter-spacing: -1px;
`;

const TemplateCodeBox = styled.code`
  background: rgba(0,0,0,0.05);
  white-space: pre-wrap;
  display: block;
  padding: 24px;
  margin: 12px auto 48px;
  font-size: 0.9rem;
  line-height: 1.2rem;
  width: 100%;
  border-radius: 2px;
  box-sizing: border-box;
  box-shadow: 0 2px 4px rgba(0,0,0,0.02);
`;

const TemplateBack = styled.a`
  display: block;
  margin: 40px 0 40px auto;
  padding: 20px;
  width: 20px;
  height: 20px;
  border-radius: 30px;
  background: rgba(0,0,0,0.1);
  transition: background 0.2s ease-in-out;
  
  &:hover {
    background: rgba(0,0,0,0.2);
  }
`;

const TemplateTable = styled.div`
  width: 100%;
  overflow: hidden;
  overflow-x: auto;
`;

export {
  TemplateTitle,
  TemplateCodeBox,
  TemplateBack,
  TemplateTable
}