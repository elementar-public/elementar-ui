import React from 'react';
import GridDocs from '../docs/Grid';
import TableDocs from '../docs/Table';
import Menu from '../components/menu';

const Layout = () => (
  <section>
    <Menu title="Layout">
      <li><a href="#grid">Grid</a></li>
      <li><a href="#tabela">Tabelas</a></li>
    </Menu>

    <GridDocs/>
    <TableDocs/>
  </section>
);

export default Layout;

/*
- grid
- tables
- colors
*/