import React from 'react';
import BoxDocs from '../docs/Box';
import ButtonDocs from '../docs/Button';
import LinkButtonDocs from '../docs/LinkButton';
import AccordionDocs from '../docs/Accordion';
import DropdownDocs from '../docs/Dropdown';
import TabsDocs from '../docs/Tabs';
import YouTubeVideoDocs from '../docs/YouTubeVideo';
import GoogleMapDocs from '../docs/GoogleMap';
import Menu from '../components/menu';

const Components = () => (
  <section>
    <Menu title="Componentes">
      <li><a href="#box">Boxes</a></li>
      <li><a href="#buttons">Botões</a></li>
      <li><a href="#linkbuttons">Botões de link</a></li>
      <li><a href="#accordion">Accordions</a></li>
      <li><a href="#dropdown">Menus dropdown</a></li>
      <li><a href="#tab">Tabs</a></li>
      <li><a href="#youtube-video">YouTube Player</a></li>
      <li><a href="#google-map">Google Maps</a></li>
    </Menu>

    <BoxDocs />
    <ButtonDocs />
    <LinkButtonDocs />
    <AccordionDocs />
    <DropdownDocs />
    <TabsDocs />
    <YouTubeVideoDocs />
    <GoogleMapDocs />
  </section>
);

export default Components;