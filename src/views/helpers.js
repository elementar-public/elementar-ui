import React from 'react';
import TooltipDocs from '../docs/Tooltip';
import PopoverDocs from '../docs/Popover';
import StepperDocs from '../docs/Stepper';
import ProgressBarDocs from '../docs/ProgressBar';
import Menu from '../components/menu';

const Helpers = () => (
  <div>
    <Menu title="Helpers">
      <li><a href="#tooltip">Tooltip</a></li>
      <li><a href="#popover">Popover</a></li>
      <li><a href="#stepper">Stepper</a></li>
      <li><a href="#progress-bar">Barras de progresso</a></li>
    </Menu>
    
    <TooltipDocs/>
    <PopoverDocs/>
    <StepperDocs/>
    <ProgressBarDocs/>
  </div>
);

export default Helpers;

/*
- tooltip
- popover
- stepper
*/