import React from 'react';
import TextInputDocs from '../docs/TextInput';
import TextAreaDocs from '../docs/TextArea';
import FileInputDocs from '../docs/FileInput';
import SelectDocs from '../docs/Select';
import SelectionDocs from '../docs/Selection';
import RangeSelectDocs from '../docs/RangeSelect';
import Menu from '../components/menu';

const Forms = () => (
  <section>
    <Menu title="Forms">
      <li><a href="#text-input">TextInput</a></li>
      <li><a href="#text-area">TextArea</a></li>
      <li><a href="#file-input">FileInput</a></li>
      <li><a href="#select">Select</a></li>
      <li><a href="#selection-controls">Controles de seleção</a></li>
      <li><a href="#range-selector">Range select</a></li>
    </Menu>

    <TextInputDocs/>
    <TextAreaDocs />
    <FileInputDocs />
    <SelectDocs />
    <SelectionDocs />
    <RangeSelectDocs />
  </section>
);

export default Forms;