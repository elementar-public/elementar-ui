import React from 'react';
import ModalDocs from '../docs/Modal';
import ContextDocs from '../docs/Context';
import DialogDocs from '../docs/Dialog';
import FloaterDocs from '../docs/Floater';
import CurtainDocs from '../docs/Curtain';
import Menu from '../components/menu';

const Modals = () => (
  <section>
    <Menu title="Modais">
      <li><a href="#modal">Modal</a></li>
      <li><a href="#context">Context</a></li>
      <li><a href="#dialog">Caixa de diálogo</a></li>
      <li><a href="#floater">Floater</a></li>
      <li><a href="#curtain">Curtain</a></li>
    </Menu>
    
    <ModalDocs/>
    <ContextDocs/>
    <DialogDocs/>
    <FloaterDocs/>
    <CurtainDocs/>
  </section>
);

export default Modals;
