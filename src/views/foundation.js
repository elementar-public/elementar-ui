import React from 'react';
import Menu from '../components/menu';
import LogoDocs from '../docs/Logo';
import ColorsDocs from '../docs/Colors';

const FoundationView = () => (
  <section>
    <Menu title="Fundamentos">
      <li><a href="#logo">Marca</a></li>
      <li><a href="#cores">Cores</a></li>
    </Menu>

    <LogoDocs/>
    <ColorsDocs/>
  </section>
);

export default FoundationView;