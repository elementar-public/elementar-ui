import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Grid from '../lib/Grid';
import LogoShort from '../assets/logoShort.svg';

const HomeLogo = styled.img`
  display: block;
  margin: 40px auto;
  width: 100%;
  max-width: 300px;
`;

const HomeTitle = styled.h2`
  text-align: center;
`;

const HomeVersion = styled.p`
  margin: 0 auto 20px;
  width: max-content;
  display: flex;
  font-size: 0.85rem;

  .label {
    display: block;
    padding: 4px;
    border-radius: 4px 0 0 4px;
    background: #344;
    color: white;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
  }
  
  .value {
    display: block;
    padding: 4px;
    border-radius: 0 4px 4px 0;
    background: #097;
    color: white;
    text-shadow: 0 -1px 0 rgba(0,0,0,0.2);
    font-weight: bold;
  }
`;

const HomeMenu = styled.ul`
  display: grid;
  grid-template: auto / repeat(auto-fit, minmax(200px, 1fr));
  grid-gap: 20px;
  margin: 40px 0;
  padding: 0;
  list-style: none;

  > li {
    background: rgba(0,0,0,0.1);
    padding: 10px;
    border-radius: 8px;
  }

  h3 {
    font-size: 0.9rem;
    margin: 0;
    padding: 0;
  }

  ul {
    margin: 0;
    padding: 0;
    list-style: none;
  }

  a {
    display: block;
    margin-top: 5px;
    font-size: 0.9rem;
    color: black;
    text-decoration: none;

    &:hover {
      text-decoration: underline;
    }
  }
`;

const Home = () => (
  <Grid container breakpoint="md">
    <HomeLogo src={LogoShort} alt="Elementar UI" />
    <HomeTitle>Elementar UI</HomeTitle>
    <HomeVersion>
      <span className="label">Versão</span>
      <span className="value">{process.env.REACT_APP_VERSION}</span>
    </HomeVersion>

    <p>O Elementar UI é um conjunto de elementos de UI para projetos da <a href="https://elementar.digital" target="_blank" rel="noopener noreferrer">Elementar.Digital</a>, ainda em progresso. Conta com elementos de interface voltados para diagramação e disposição de conteúdo, elementos de interação e utilitários para a criação de interfaces ricas.</p>

    <HomeMenu>
      <li>
        <h3><Link to="/fundamentos">Fundamentos</Link></h3>

        <ul>
          <li><Link to="/fundamentos#logo">Marca</Link></li>
          <li><Link to="/fundamentos#cores">Cores</Link></li>
        </ul>
      </li>

      <li>
        <h3><Link to="/layout">Layout</Link></h3>

        <ul>
          <li><Link to="/layout#grid">Grid</Link></li>
          <li><Link to="/layout#tabela">Tabelas</Link></li>
        </ul>
      </li>

      <li>
        <h3><Link to="/componentes">Componentes</Link></h3>

        <ul>
          <li><Link to="/componentes#box">Boxes</Link></li>
          <li><Link to="/componentes#buttons">Botões</Link></li>
          <li><Link to="/componentes#accordion">Accordions</Link></li>
          <li><Link to="/componentes#dropdown">Menus dropdown</Link></li>
          <li><Link to="/componentes#tab">Tabs</Link></li>
          <li><Link to="/componentes#youtube-video">YouTube Player</Link></li>
          <li><Link to="/componentes#google-map">Google Maps</Link></li>
        </ul>
      </li>

      <li>
        <h3><Link to="/forms">Forms</Link></h3>

        <ul>
          <li><Link to="/forms#text-input">TextInput</Link></li>
          <li><Link to="/forms#text-area">TextArea</Link></li>
          <li><Link to="/forms#file-input">FileInput</Link></li>
          <li><Link to="/forms#select">Select</Link></li>
          <li><Link to="/forms#selection-controls">Controles de seleção</Link></li>
          <li><Link to="/forms#range-selector">Range select</Link></li>
        </ul>
      </li>

      <li>
        <h3><Link to="/modais">Modais</Link></h3>

        <ul>
          <li><Link to="/modais#modal">Modal</Link></li>
          <li><Link to="/modais#context">Context</Link></li>
          <li><Link to="/modais#dialog">Caixa de diálogo</Link></li>
          <li><Link to="/modais#floater">Floater</Link></li>
        </ul>
      </li>

      <li>
        <h3><Link to="/helpers">Helpers</Link></h3>

        <ul>
          <li><Link to="/helpers#tooltip">Tooltip</Link></li>
          <li><Link to="/helpers#popover">Popover</Link></li>
          <li><Link to="/helpers#stepper">Stepper</Link></li>
          <li><Link to="/helpers#progress-bar">Barras de progresso</Link></li>
        </ul>
      </li>

      <li>
        <h3><Link to="Utilidades">Utilidades</Link></h3>

        <ul>
          <li><Link to="/utilidades#label">Label</Link></li>
          <li><Link to="/utilidades#errorbox">ErrorBox</Link></li>
          <li><Link to="/utilidades#zerostate">ZeroState</Link></li>
          <li><Link to="/utilidades#skeleton">Skeleton</Link></li>
          <li><Link to="/utilidades#loading">Loading</Link></li>
          <li><Link to="/utilidades#icons">Ícones</Link></li>
        </ul>
      </li>
    </HomeMenu>
  </Grid>
);

export default Home;
