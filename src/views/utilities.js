import React from 'react';
import LabelDocs from '../docs/Label';
import ErrorBoxDocs from '../docs/ErrorBox';
import ZeroStateDocs from '../docs/ZeroState';
import LoadingDocs from '../docs/Loading';
import SkeletonDocs from '../docs/Skeleton';
import IconLibraryDocs from '../docs/IconLibrary';
import Menu from '../components/menu';

const Utilities = () => (
  <div>
    <Menu title="Utilidades">
      <li><a href="#label">Label</a></li>
      <li><a href="#error">Error</a></li>
      <li><a href="#zerostate">ZeroState</a></li>
      <li><a href="#skeleton">Skeleton</a></li>
      <li><a href="#loading">Loading</a></li>
      <li><a href="#icon-library">Ícones</a></li>
    </Menu>

    <LabelDocs/>
    <ErrorBoxDocs/>
    <ZeroStateDocs/>
    <SkeletonDocs/>
    <LoadingDocs/>
    <IconLibraryDocs/>
  </div>
);

export default Utilities;